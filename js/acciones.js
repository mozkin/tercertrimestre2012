$(document).ready(function(){
    $('textarea').autoResize({
        // On resize:
        onResize : function() {
            $(this).css({
                opacity:0.8
            });
        },
        // After resize:
        animateCallback : function() {
            $(this).css({
                opacity:1
            });
        },
        // Quite slow animation:
        animateDuration : 300,
        // More extra space:
        extraSpace : 40,
        limit: 300
    });
//    $.extend($.fn.Tooltip.defaults, {
//        track: true,
//        delay: 0,
//        showURL: false,
//        showBody: " - "
//    });
//    $('a, input, img, p').Tooltip();

    id_meta=$("#id_meta").val();
    base=$("#url_base").val();
    $("input:submit, input:button").button();

    $('.seleccionar').hover(function(){
        $(this).children().addClass('datahighlight');
    },function(){
        $(this).children().removeClass('datahighlight');
    });

    $("#divAccion").hide();

    $("#AgregarAcc").click(function () {
        $("#divAccion").show('slow');
    });

    $("#insertarAccion").validate( {
        submitHandler: function() {
            var descripcion1=$("#descripcion1").val();
            var descripcion2=$("#descripcion2").val();
            var accion=$("#accion").val();
            var submit=1;

            $.ajax({
                type: "POST",
                url: base+"/acciones",
                data: "submit="+submit+"&descripcion1="+descripcion1+"&descripcion2="+descripcion2+"&accion="+accion+"&id_meta="+id_meta,
                success: function(msg){
                    alert("La acción se ha agregado exitosamente a la meta.");
                    $('#divMeta').html(msg).show('slow');
                }
            });
        }
    });

    $(".verAccion").click(function () {
        var  id_accion = $(this).children().val();
        $("#AgregarAcc").hide();
        $.ajax({
            type: "POST",
            url: base+"/verAccion",
            data: "&accion="+id_accion,
            success: function(msg){
                $('#divAccion').html(msg).show('slow');
            }
        });

    });


    $(".verRubros").click(function () {
        $("#AgregarAcc").hide();
        var  id_accion= $(this).children().val();
        var submit=0;
        $.ajax({
            type: "POST",
            url: base+"/rubros",
            data: "submit="+submit+"&id_accion="+id_accion,
            success: function(msg){
                $('#divAccion').html(msg).show('slow');
            }
        });

    });

    $('.eliminarAccion').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar la acción?');
        if(answer == true){
            $("#AgregarAcc").hide();
            var id_meta = $("#id_meta").val();
            var id_accion = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_accion="+id_accion+"&id_meta="+id_meta,
                url: base+"/eliminarAccion",
                success: function(msg){
                    $('#divMeta').html(msg).show('slow');
                }
            });
        }else{

    }
    });
});

