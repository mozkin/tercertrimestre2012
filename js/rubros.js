var periodos = [
{
    'When':'1',
    'Value':'4',
    'Text':'Primer periodo: septiembre, octubre, noviembre de 2011'
},
{
    'When':'1',
    'Value':'5',
    'Text':'Segundo periodo: enero, febrero, marzo de 2012'
},
{
    'When':'1',
    'Value':'6',
    'Text':'Tercer periodo: abril, mayo, junio de 2012'
},
{
    'When':'2',
    'Value':'9',
    'Text':'Primer periodo: septiembre, octubre, noviembre de 2012'
},
{
    'When':'2',
    'Value':'10',
    'Text':'Segundo periodo: enero, febrero, marzo de 2013'
},
{
    'When':'2',
    'Value':'11',
    'Text':'Tercer periodo: abril, mayo, junio de 2013'
},

];

function commonTemplate(item) {
    return "<option value='" + item.Value + "'>" + item.Text + "</option>";
}
function commonTemplate2(item) {
    return "<option value='" + item.Value + "'>***" + item.Text + "***</option>";
}

function commonMatch(selectedValue) {
    return this.When == selectedValue;
}

$(document).ready(function(){
    $("input:submit, input:button").button();
    id_accion=$("#id_accion").val();
    base=$("#url_base").val();

    $('.seleccionar').hover(function(){
        $(this).children().addClass('datahighlight');
    },function(){
        $(this).children().removeClass('datahighlight');
    });
    
    //para actualizar el presupuesto total, cada que se agregan rubros
    presupuestoTotal=$("#presupuestoTotal").val();
    $("#presupuesto").html("Presupuesto: $"+presupuestoTotal);

    $("#divRubro").hide();

    $("#AgregarRub").click(function () {
        $("#AgregarRub").hide();
        $("#divRubro").show('slow');
    });

    $("#metros").css("display", "none");
    $("#metroslabel").css("display", "none");
    $("#id_clasificador").change(function(){
        var id_clasificador = $("#id_clasificador").val();
        if(id_clasificador>=29){
            if(id_clasificador <=40){
                $("#metros").css("display", "block");
                $("#metroslabel").css("display", "block");
            }
        }
        if(id_clasificador>=58){
            if(id_clasificador <=68){
                $("#metros").css("display", "block");
                $("#metroslabel").css("display", "block");
            }
        }
        if(id_clasificador==70){

            $("#metros").css("display", "block");
            $("#metroslabel").css("display", "block");
        }
        if(id_clasificador<=29){
            $("#metros").css("display", "none");
            $("#metroslabel").css("display", "none");
        }
        if(id_clasificador>=41){
            if(id_clasificador<=56){
                $("#metros").css("display", "none");
                $("#metroslabel").css("display", "none");
            }
        }
    });

    $("#insertarRubro").validate( {
        submitHandler: function() {
            var id_clasificador=$("#id_clasificador").val();
            var anyo=$("#anyo").val();
            var cantidad=$("#cantidad").val();
            var precio_unitario=$("#precio_unitario").val();
            var id_periodo_aplicacion=$("#id_periodo_aplicacion").val();

            var submit=1;

            $.ajax({
                type: "POST",
                url: base+"/rubros",
                data: "submit="+submit+"&id_clasificador="+id_clasificador+"&id_accion="+id_accion+"&cantidad="+cantidad+"&precio_unitario="+precio_unitario+"&anyo="+anyo+"&id_periodo_aplicacion="+id_periodo_aplicacion,
                success: function(msg){
                    $('#divAccion').html(msg).show('slow');
                }
            });
        }

    });

    $(".verRubro").click(function () {
        var  id_rubro = $(this).children().val();
        $("#AgregarRub").hide();
        $.ajax({
            type: "POST",
            url: base+"/verRubro",
            data: "&rubro=" + id_rubro,
            success: function(msg){
                $('#divRubro').html(msg).show('slow');
            }
        });
    });


    $('.eliminarRubro').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar el rubro?');
        if(answer == true){
            $("#AgregarRub").hide();
            var id_accion = $("#id_accion").val();
            var id_rubro = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_rubro="+id_rubro+"&id_accion="+id_accion,
                url: base+"/eliminarRubro",
                success: function(msg){
                    $('#divAccion').html(msg).show('slow');
                }
            });
        }else{

    }
    });

    //    $('input.idRubroEliminar').confirm({
    //        msg:'Estás seguro?',
    //        timeout:3000
    //    });

    //para anyo-periodo de aplicacion
    $("#id_periodo_aplicacion").cascade("#anyo",{
        list: periodos,
        template: commonTemplate,
        match: commonMatch
    });
});


