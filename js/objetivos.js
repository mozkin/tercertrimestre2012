$(document).ready(function(){
    $('textarea').autoResize({
        // On resize:
        onResize : function() {
            $(this).css({
                opacity:0.8
            });
        },
        // After resize:
        animateCallback : function() {
            $(this).css({
                opacity:1
            });
        },
        // Quite slow animation:
        animateDuration : 300,
        // More extra space:
        extraSpace : 40,
        limit: 300
    });
    
    base=$("#url_base").val();
    id_proyecto=$("#id_proyecto").val();
    $("input:submit, input:button").button();

    $('.seleccionar').hover(function(){
        $(this).children().addClass('datahighlight');
    },function(){
        $(this).children().removeClass('datahighlight');
    });

    $("#divObjetivo").hide();

    $(".verObjetivo").click(function(){
        objetivo=$(this).children().val();
        $.ajax({
            type: "POST",
            url: base+"/ver_objetivo",
            data: "&objetivo="+objetivo,
            success: function(msg){
                $('#divObjetivo').html(msg).show('slow');
            }
        });
    });

    
    $("#AgregarObj").click(function () {
        $("#divObjetivo").show('slow');
    });

    $("#insertarObjetivo").validate({
        submitHandler: function() {
            $("#AgregarObj").hide();
            var descripcion=$("#descripcion").val();
            var justificacion=$("#justificacion").val();
            var id_subtipo_de_objetivo=$("#id_subtipo_de_objetivo").val();
            var	submit=1;
            $.ajax({
                type: "POST",
                url: base+"/objetivos",
                data: "submit="+submit+"&id_proyecto="+id_proyecto+"&descripcion="+descripcion+"&justificacion="+justificacion+"&id_subtipo_de_objetivo="+id_subtipo_de_objetivo,
                success: function(msg){
                    alert("El objetivo se ha agregado exitosamente al proyecto.");
                    $('#subContent').html(msg).show('slow');
                }
            });
        }
    });

    $('.eliminarObjetivo').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar el objetivo?');
        if(answer == true){
            $("#AgregarObj").hide();
            var id_proyecto = $("#id_proyecto").val();
            var id_objetivo = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_objetivo="+id_objetivo+"&id_proyecto="+id_proyecto,
                url: base+"/eliminarObjetivo",
                success: function(msg){
                    $('#subContent').html(msg).show('slow');
                }
            });
        }else{

    }
    });

    $(".verMetas").click(function(){
        $("#AgregarObj").hide();
        var  id_objetivo = $(this).children().val();
        var	submit=0;
        $.ajax({
            type: "POST",
            url: base+"/metas",
            data:  "submit="+submit+"&id_objetivo="+id_objetivo,
            success: function(msg){
                $('#divObjetivo').html(msg).show('slow');
            }
        });

    });
});