$(document).ready(function() {
    $('textarea').autoResize({
        // On resize:
        onResize: function() {
            $(this).css({
                opacity: 0.8
            });
        },
        // After resize:
        animateCallback: function() {
            $(this).css({
                opacity: 1
            });
        },
        // Quite slow animation:
        animateDuration: 300,
        // More extra space:
        extraSpace: 40,
        limit: 300
    });

    id_objetivo = $("#id_objetivo").val();
    base = $("#url_base").val();
    $("input:submit, input:button").button();

    $('.seleccionar').hover(function() {
        $(this).children().addClass('datahighlight');
    }, function() {
        $(this).children().removeClass('datahighlight');
    });

    $("#divMeta").hide();

    $("#AgregarMet").click(function() {
        $("#divMeta").show('slow');
    });

    $("#insertarMeta").validate({
        submitHandler: function() {
            var descripcion = $("#descripcion").val();
            var unidad_de_medida = $("#unidad_de_medida").val();
            var beneficiarios_hombres = $("#beneficiarios_hombres").val();
            var beneficiarios_mujeres = $("#beneficiarios_mujeres").val();
            var id_tipo_de_beneficiario = $("#id_tipo_de_beneficiario").val();
            var submit = 1;
            $.ajax({
                type: "POST",
                url: base + "/metas",
                data: "submit=" + submit + "&id_objetivo=" + id_objetivo + "&descripcion=" + descripcion + "&unidad_de_medida=" + unidad_de_medida + "&beneficiarios_hombres=" + beneficiarios_hombres + "&beneficiarios_mujeres=" + beneficiarios_mujeres + "&id_tipo_de_beneficiario=" + id_tipo_de_beneficiario,
                success: function(msg) {
                    alert("La meta se ha agregado exitosamente al objetivo.");
                    $('#divObjetivo').html(msg).show('slow');
                }
            });
        }

    });

    $(".verMeta").click(function() {
        var id_meta = $(this).children().val();
        $("#AgregarMet").hide();
        $.ajax({
            type: "POST",
            url: base + "/verMeta",
            data: "&meta=" + id_meta,
            success: function(msg) {
                $('#divMeta').html(msg).show('slow');
            }
        });
    });

    $(".verInformeMeta").click(function() {
        var id_meta = $(this).children().val();
        $("#AgregarMet").hide();
        $.ajax({
            type: "POST",
            url: base + "/verInformeMeta",
            data: "&meta=" + id_meta,
            success: function(msg) {
                $('#divMeta').html(msg).show('slow');
            }
        });
    });

    $(".verAcciones").click(function() {
        $("#AgregarMet").hide();
        var id_meta = $(this).children().val();
        var submit = 0;
        $.ajax({
            type: "POST",
            url: base + "/acciones",
            data: "submit=" + submit + "&id_meta=" + id_meta,
            success: function(msg) {
                $('#divMeta').html(msg).show('slow');
            }
        });
    });

    $('.eliminarMeta').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar la meta?');
        if (answer == true) {
            $("#AgregarMet").hide();
            var id_objetivo = $("#id_objetivo").val();
            var id_meta = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_meta=" + id_meta + "&id_objetivo=" + id_objetivo,
                url: base + "/eliminarMeta",
                success: function(msg) {
                    $('#divObjetivo').html(msg).show('slow');
                }
            });
        } else {

        }
    });

    $("button.capturaAvance").click(function() {
        $("#capturaAvance").hide();//ocultando el boton de edtar
        alert("yuju");
        $("#avance").show();
        //$("em").show();
        $.ajax({
            type: "POST",
            url: base + "/showFormAvanceProgramatico/",
            data: "id_periodo=" + id_periodo + "&id_meta=" + id_meta,
            success: function(msg) {
                $('#avance').html(msg).show('slow');
            }
        });
    });


});

