$(document).ready(function(){
    $('textarea').autoResize({
        // On resize:
        onResize : function() {
            $(this).css({
                opacity:0.8
            });
        },
        // After resize:
        animateCallback : function() {
            $(this).css({
                opacity:1
            });
        },
        // Quite slow animation:
        animateDuration : 300,
        // More extra space:
        extraSpace : 40,
        limit: 300
    });

    //Aplicando la función textlimit a cada textarea
    $("textarea").click(function(){
        $(this).each(function(){
            var idd = $(this).attr('id');
            var maximo = $(this).attr('maxlength');
            $(this).textlimit('span[id='+idd+']', maximo);
        });
    });

    var base=$("#url_base").val();
    $("#updateProyecto").hide();
    $("input:submit, input:button").button();

    $('.seleccionar').hover(function(){
        $(this).children().addClass('datahighlight');
    },function(){
        $(this).children().removeClass('datahighlight');
    });

    $(".verProyecto").click(function(){
        proyecto=$(this).children().val();
        $("tr[id="+proyecto+"] td[class=borde]").css('background-color','#e33241');

        $.ajax({
            type: "POST",
            url: base+"/verProyecto",
            data: "&proyecto="+proyecto,
            success: function(msg){
                $('#subContent').html(msg).show();
            }
        });
    });

    $("#editarProyecto").click(function () {
        $('*').removeAttr('disabled');
        $("#editarProyecto").hide();
        $("#updateProyecto").show();
        $("em").show();
    });

    $("em").hide();
    $("#formProyecto").validate({
        rules: {
            justificacion: {
                required: true,
                rangelength: [5, 300]
            },
            objetivo: {
                required: true,
                rangelength: [5, 300]
            }
        },
        submitHandler: function() {
            datos=$("#formProyecto").serialize();
            $.ajax({
                type: "POST",
                url: base+"/updateProyecto",
                data: datos,
                success: function(msg){
                    alert("El Proyecto se actualizo exitosamente.");
                    $('#subContent').html(msg).show('slow');

                }
            });
        }
    });

    $(".mostrarObjetivos").click(function(){
        proyecto=$(this).children().val();
        submit = 0;
        $.ajax({
            type: "POST",
            url: base+"/objetivos",
            data: "submit=" + submit + "&id_proyecto="+proyecto,
            success: function(msg){
                $('#subContent').html(msg).show();
            }
        });
    });

    $('.eliminarProyecto').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar el proyecto?');
        if(answer == true){
            var  id_proyecto = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_proyecto="+ id_proyecto,
                url: base+"/eliminarProyecto",
                success: function(msg){
                    $('#content').html(msg).show('slow');
                }
            });
        }else{
    }
    });

});