/**
 *responsables.js
 *funcion js para responsables
 */

$(document).ready(function(){
    var base=$("#url_base").val();
    $("#updateResponsable").hide();
    $("em").hide();
    $("input:submit, input:button").button();


    $("#editarResponsable").click(function(){
	$('*').removeAttr('disabled');
	$("#editarResponsable").hide();
	$("#updateResponsable").show();
	$("em").show();

    });
    
    $("#formResponsable").validate({
	messages:{
	    nombre: "Este campo no puede estar vacío",
	    cargo: "Este campo no puede estar vacío",
	    titulo: "Este campo no puede estar vacío",
	    id_grado_academico: "Este campo no puede estar vacío",
	    telefono: "Este campo no puede estar vacío",
	    email:{
		email: "Se requiere un email válido",
		required: "Este campo no puede estar vacío"
	    }
	},
    	submitHandler: function() {
    	    datos=$("#formResponsable").serialize();
	    
	    urlBase=base+"/updateResponsable";

    	    $.ajax({ 
    	    	type: "POST", 
    	    	url: urlBase,
    	    	data: datos,		
    	    	success: function(msg){ 
	    	 //   alert(msg)
	      	    $('#content').html(msg).show('slow');
	    	} 
    	    });
    	}	
    });

     $('.eliminar_responsable').click(function() {
        var answer = confirm('¿Está seguro que desea eliminar el responsable?');
        if(answer == true){
            var  id_proyecto = $(this).children().val();
            $.ajax({
                type: "POST",
                data: "id_proyecto="+ id_proyecto,
                url: "responsable/eliminarProyecto",
                success: function(msg){
                    $('#content').html(msg).show('slow');
                }
            });
        }else{
    }
    });
});
