#Inserciones para los permisos de los usuarios de prueba para el sistema
insert into sistema values(5, 'Reprogramación 2011-2012', 1);
#Usuario geraldine
insert into permiso_sistema values(5, 1, 1, 31);
insert into permiso_sistema values(5, 1, 2, 31);
insert into permiso_sistema values(5, 1, 3, 31);
insert into permiso_sistema values(5, 1, 4, 31);
insert into permiso_sistema values(5, 1, 5, 31);
insert into permiso_sistema values(5, 1, 6, 31);
insert into permiso_sistema values(5, 1, 7, 31);
#Usuario geral
insert into permiso_sistema values(5, 2, 1, 31);
insert into permiso_sistema values(5, 2, 2, 31);
insert into permiso_sistema values(5, 2, 3, 31);
insert into permiso_sistema values(5, 2, 4, 31);
insert into permiso_sistema values(5, 2, 5, 31);
insert into permiso_sistema values(5, 2, 6, 31);
insert into permiso_sistema values(5, 2, 7, 31);


/*tablas para Genaro*/
CREATE TABLE accion_alcance
(
  id_accion_alcance serial NOT NULL,
  id_accion integer,
  id_periodo integer,
  especificaciones text,
  breve_descripcion text,
  CONSTRAINT accion_alcance_pkey PRIMARY KEY (id_accion_alcance),
  CONSTRAINT accion_alcance_id_accion_fkey FOREIGN KEY (id_accion)
      REFERENCES accion (id_accion) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT accion_alcance_id_periodo_fkey FOREIGN KEY (id_periodo)
      REFERENCES periodo (id_periodo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE meta_avance
(
  id_meta_avance serial NOT NULL,
  id_metas integer,
  id_periodo integer,
  mujeres integer,
  hombres integer,
  CONSTRAINT meta_avance_pkey PRIMARY KEY (id_meta_avance),
  CONSTRAINT meta_avance_id_metas_fkey FOREIGN KEY (id_metas)
      REFERENCES metas (id_metas) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT meta_avance_id_periodo_fkey FOREIGN KEY (id_periodo)
      REFERENCES periodo (id_periodo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


/*Para borrado lógico de los registros*/
ALTER TABLE proyecto ADD COLUMN eliminado boolean;
ALTER TABLE proyecto ALTER COLUMN eliminado SET DEFAULT false;
ALTER TABLE objetivos ADD COLUMN eliminado boolean;
ALTER TABLE objetivos ALTER COLUMN eliminado SET DEFAULT false;
ALTER TABLE metas ADD COLUMN eliminado boolean;
ALTER TABLE metas ALTER COLUMN eliminado SET DEFAULT false;
ALTER TABLE accion ADD COLUMN eliminado boolean;
ALTER TABLE accion ALTER COLUMN eliminado SET DEFAULT false;
ALTER TABLE rubro ADD COLUMN eliminado boolean;
ALTER TABLE rubro ALTER COLUMN eliminado SET DEFAULT false;
ALTER TABLE monto ADD COLUMN eliminado boolean;
ALTER TABLE monto ALTER COLUMN eliminado SET DEFAULT false;

/*VISTAS*/
CREATE VIEW presupuesto AS
SELECT p.id_proyecto, p.id_periodo, mo.cantidad, mo.precio_unitario as precio
FROM proyecto p, objetivos o, metas m, accion a, rubro r, monto mo
WHERE p.id_proyecto = o.id_proyecto AND o.id_objetivos = m.id_objetivos AND m.id_metas = a.id_metas AND a.id_accion = r.id_accion AND r.id_rubro = mo.id_rubro;

/*Vistas Gnaro */

CREATE VIEW v_rubro_periodo AS 
SELECT r.id_rubro AS id_rubro, r.id_accion AS id_accion,
	r.anyo AS anyo, c.id_clasificador AS id_clasificador,
	c.clasificador AS clasificador, p.id_periodo AS id_periodo,
	p.periodo AS periodo, m.cantidad AS cantidad,
	m.precio_unitario AS precio_unitario, m.id_monto AS id_monto 
FROM rubro r, clasificador c, monto m, periodo p 
WHERE r.id_clasificador = c.id_clasificador AND m.id_rubro = r.id_rubro 
	AND m.id_periodo = p.id_periodo;
