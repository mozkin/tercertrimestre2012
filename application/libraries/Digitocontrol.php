<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of digitoControl
 *
 * @author gnaro
 */
class Digitocontrol {

    function Digitocontrol() {
        
    }

    function d($j, $k) {
        $table = array(
            array(0,1,2,3,4,5,6,7,8,9),
            array(1,2,3,4,0,6,7,8,9,5),
            array(2,3,4,0,1,7,8,9,5,6),
            array(3,4,0,1,2,8,9,5,6,7),
            array(4,0,1,2,3,9,5,6,7,8),
            array(5,9,8,7,6,0,4,3,2,1),
            array(6,5,9,8,7,1,0,4,3,2),
            array(7,6,5,9,8,2,1,0,4,3),
            array(8,7,6,5,9,3,2,1,0,4),
            array(9,8,7,6,5,4,3,2,1,0),
        );
        return $table[$j][$k];
    }

    function p($pos, $num) {
        $table = array(
            array(0,1,2,3,4,5,6,7,8,9),
            array(1,5,7,6,2,8,3,0,9,4),
            array(5,8,0,3,7,9,6,1,4,2),
            array(8,9,1,6,0,4,3,5,2,7),
            array(9,4,5,3,1,2,6,8,7,0),
            array(4,2,8,6,5,7,3,9,0,1),
            array(2,7,9,3,8,0,6,4,1,5),
            array(7,0,4,6,9,1,3,2,5,8),
        );
        return $table[$pos % 8][$num];
    }

    function inv($j) {
        $table = array(0,4,3,2,1,5,6,7,8,9);
        return $table[$j];
    }

    function calculaSuma($number) {
        $c = 0;
        $n = strrev($number);
        $len = strlen($n);
        for ($i = 0; $i < $len; $i++)
            $c = $this->d($c, $this->p($i+1, $n[$i]));
        return $this->inv($c);
    }

    function verificaSuma($number) {
        $c = 0;
        $n = strrev($number);
        $len = strlen($n);
        for ($i = 0; $i < $len; $i++)
            $c = $this->d($c, $this->p($i, $n[$i]));
        return $c;
    }

}
?>
