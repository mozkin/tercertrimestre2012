<?php 
 
$config['navigation'] = array( 
            'Responsable' => array( 
                'id'     => 'responsable', 
                'title'  => 'Responsable', 
                'link'   => 'responsable'
            ), 
            'Proyecto' => array( 
                'id'     => 'proyectos', 
                'title'  => 'Proyectos', 
                'link'   => 'proyectos' 
            ), 
            'Reporte' => array( 
                'id'     => 'reportes',
                'title'  => 'Reportes', 
                'link'   => 'reportes' 
            ), 
            'Salir' => array( 
                'id'     => 'salir',			     
                'title'  => 'Salir', 
                'link'   => 'salir' 
            ) 
        ); 
 
/* End of file navigation.php */ 
/* Location: ./application/config/navigation.php */