<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "proyecto";
$route['404_override'] = '';


//$route['login'] = 'login/verificar';
//
//$route['responsable/responsables'] = 'responsable/index';
//$route['responsable/(\d+)'] = 'responsable/$1';
//$route['responsable/(\d+)/edit'] = 'responsable/actualizar/$1';
//
//$route['proyectos'] = 'proyecto/index';
//$route['proyecto/(\d+)'] = 'proyecto/$1';
//$route['proyecto/(\d+)/edit'] = 'proyecto/actualizar/$1';
//
//$route['proyecto/(\d+)/objetivos'] = 'objetivo/index';
//$route['proyecto/(\d+)/objetivo/(\d+)'] = 'objetivo/$2';
//$route['proyecto/(\d+)/objetivo/(\d+)/edit'] = 'objetivo/actualizar/$2';
//
//$route['proyecto/(\d+)/objetivo/(\d+)/metas'] = 'meta/index';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)'] = 'meta/$3';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/edit'] = 'meta/actualizar/$3';
//
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/acciones'] = 'accion/index';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/accion/(\d+)'] = 'accion/$4';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/accion/(\d+)/edit'] = 'accion/actualizar/$4';
//
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/accion/(\d+)/rubros'] = 'rubro/index';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/accion/(\d+)/rubro/(\d+)'] = 'rubro/$5';
//$route['proyecto/(\d+)/objetivo/(\d+)/meta/(\d+)/accion/(\d+)/rubro/(\d+)/edit'] = 'rubro/actualizar/$5';
//
//$route['proyecto/(\d+)/reporte'] = 'reporte/pdf/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */