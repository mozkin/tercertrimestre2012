<script>
        var periodos = [
            {'When':'1','Value':'4','Text':'Primer periodo: septiembre, octubre, noviembre de 2011'},
            {'When':'1','Value':'5','Text':'Segundo periodo: enero, febrero, marzo de 2012'},
            {'When':'1','Value':'6','Text':'Tercer periodo: abril, mayo, junio de 2012'},
            {'When':'2','Value':'9','Text':'Primer periodo: septiembre, octubre, noviembre de 2012'},
            {'When':'2','Value':'10','Text':'Segundo periodo: enero, febrero, marzo de 2013'},
            {'When':'2','Value':'11','Text':'Tercer periodo: abril, mayo, junio de 2013'},
        ];
    
        function commonTemplate(item) {
            return "<option value='" + item.Value + "'>" + item.Text + "</option>";
        };
        function commonTemplate2(item) {
            return "<option value='" + item.Value + "'>***" + item.Text + "***</option>";
        };
        function commonMatch(selectedValue) {
            return this.When == selectedValue;
        };

    $(document).ready(function(){
        $("input:submit, input:button").button();
        var base=$("#url_base").val();
        var id_rubro=$("#id_rubro").val();
        $("#actualizarRubro").hide();
        $("em").hide();

        $("#editarRubro").click(function () {
            $('*').removeAttr('disabled');//le quita el disable a todos los inputs
            $("#editarRubro").hide();//oculando el boton de edtar
            $("#actualizarRubro").show();//aparece el boton de guardar
            $("em").show();
        });

        $("#updateGuardarRubro").validate({
            submitHandler: function() {
                var id_clasificador=$("#id_clasificador").val();
                var cantidad=$("#cantidad").val();
                var precio_unitario=$("#precio_unitario").val();
                var id_periodo_aplicacion=$("#id_periodo_aplicacion").val();
                var anyo=$("#anyo").val();
                var submit=2;
		
                $.ajax({
                    type: "POST",
                    url: base+"/rubros",
                    data: "submit="+submit+"&cantidad="+cantidad+"&precio_unitario="+precio_unitario+"&id_rubro="+id_rubro+"&id_accion="+id_accion+"&id_clasificador="+id_clasificador+"&id_periodo_aplicacion="+id_periodo_aplicacion+"&anyo="+anyo,
                    success: function(msg){
                        $('#divAccion').html(msg).show('slow');
                    }
                });
            }
        });

        //para anyo-periodo de aplicacion
        $("#id_periodo_aplicacion").cascade("#anyo",{
            list: periodos,
            template: commonTemplate,
            match: commonMatch
        });
        
        $("button.capturaTrimestre").click(function (){
			$("#capturaTrimestre").hide();//ocultando el boton de edtar
			$("#trimestre").show();
			$.ajax({
                    type: "POST",
                    url: base+"/agregarPeriodo/",
                    data: "id_rubro="+id_rubro+"&id_accion="+id_accion,
                    success: function(msg){
                        $('#trimestre').html(msg).show();
                    }
		});
	});

    });
</script>

<!-- form id="updateGuardarRubro" -->

      <input type="hidden" name="id_rubro" id="id_rubro"  value="<?php echo $id_rubro; ?>">

    <table  class="info" align="center">
        <tfoot>
            <tr>
                <td colspan="2" style="text-align:center; border: 0px">
                    <?php if ($permisosRubros['editar']) {
                    ?>
                        <!-- input type="button"  id="editarRubro" value="Editar" -->
                        <input type="submit"  id="actualizarRubro" name="guardarUpdateRubro" class="guardarUpdateRubro" value="Guardar">
                    <?php } ?>
                </td>
            </tr>
        </tfoot>
        <thead>
        <th colspan="2">
            <p class="title">Ver Rubro</p>
        </th>
        </thead>
        <tbody>
            <tr>
                <td class="titulo">
                    <?php echo form_label($anyo, $anyo); ?>
                    <em>*</em>
                </td>
                <td>
                    <select id="anyo" name="anyo" title="Por favor elige una opci&oacute;n" class=required disabled>
                        <?php
                        $anyo = $rubro['anyo'];
                        foreach ($anyos as $ind1 => $val1) {
                            if ($anyo == $ind1) {
                                echo "<option value=$ind1 selected>$val1</option>'";
                            } else {
                                echo "<option value=$ind1>$val1</option>'";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="titulo">
                    <label for="Rubro de gasto"><?php echo "Rubro de gasto"; ?></label>
                    <?php $idclasificador = $rubro['id_clasificador']; ?>
                        <em>*</em>
                    </td>
                    <td>
                        <select id="id_clasificador" name="id_clasificador" title="Por favor elige una opci&oacute;n" disabled class=required>
                        <?php
                        foreach ($clasificador as $indice1 => $valor1) {
                            if ($idclasificador == $indice1) {
                                echo "<option value=$indice1 selected>$valor1</option>'";
                            } else {
                                echo "<option value=$indice1>$valor1</option>'";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="titulo">
                    <?php echo form_label($cantidad, $cantidad); ?>
                    <?php
                        $can = array('value' => $rubro['cantidad'], 'disabled' => '');
                        $fcantidad1 = array_merge($can, $fcantidad);
                    ?>
                        <em>*</em>
                    </td>
                    <td>
                    <?php echo form_input($fcantidad1); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                    <?php echo form_label($precio_unitario, $precio_unitario); ?>
                    <?php
                        $pre = array('value' => $rubro['precio_unitario'], 'disabled' => '');
                        $fprecio_unitario1 = array_merge($pre, $fprecio_unitario);
                    ?>
                        <em>*</em>
                    </td>
                    <td>
                    <?php echo form_input($fprecio_unitario1); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                        <label for="Periodo de Aplicacion"><?php echo "Periodo de Aplicacion"; ?></label>
                        <em>*</em>
                    </td>
                    <td>
                        <select id="id_periodo_aplicacion" name="id_periodo_aplicacion" title="Por favor elige una opci&oacute;n" class=required disabled>
                        <?php
                        $id_periodo_aplicacion = $rubro['id_periodo_aplicacion'];
                        if ($id_periodo_aplicacion == 4 and $anyo == 1) {
                        ?>
                            <option value="4" selected>Primer periodo: septiembre, octubre, noviembre de 2011</option>
                            <option value="5">Segundo periodo: enero, febrero, marzo de 2012</option>
                            <option value="6">Tercer periodo: abril, mayo, junio de 2012</option>
                        <?php } else if ($id_periodo_aplicacion == 5 and $anyo == 1) {
                        ?>
                            <option value="4">Primer periodo: septiembre, octubre, noviembre de 2011</option>
                            <option value="5" selected>Segundo periodo: enero, febrero, marzo de 2012</option>
                            <option value="6">Tercer periodo: abril, mayo, junio de 2012</option>
                        <?php } else if ($id_periodo_aplicacion == 6 and $anyo == 1) {
                        ?>
                            <option value="4">Primer periodo: septiembre, octubre, noviembre de 2011</option>
                            <option value="5">Segundo periodo: enero, febrero, marzo de 2012</option>
                            <option value="6"  selected>Tercer periodo: abril, mayo, junio de 2012</option>
                        <?php } else if ($id_periodo_aplicacion == 9 and $anyo == 2) {
                        ?>
                            <option value="9" selected>Primer periodo: septiembre, octubre, noviembre de 2012</option>
                            <option value="10">Segundo periodo: enero, febrero, marzo de 2013</option>
                            <option value="11">Tercer periodo: abril, mayo, junio de 2013</option>
                        <?php } else if ($id_periodo_aplicacion == 10 and $anyo == 2) {
                        ?>
                            <option value="9">Primer periodo: septiembre, octubre, noviembre de 2012</option>
                            <option value="10" selected>Segundo periodo: enero, febrero, marzo de 2013</option>
                            <option value="11">Tercer periodo: abril, mayo, junio de 2013</option>
                        <?php } else if ($id_periodo_aplicacion == 11 and $anyo == 2) {
 ?>
                            <option value="9">Primer periodo: septiembre, octubre, noviembre de 2012</option>
                            <option value="10">Segundo periodo: enero, febrero, marzo de 2013</option>
                            <option value="11"  selected>Tercer periodo: abril, mayo, junio de 2013</option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        </tbody>
    </table>
<!-- /form -->

<p> <button name="capturaTrimestre" id="capturaTrimestre"  class="capturaTrimestre" value="Captura">Captura Trimestre</button></p>

	<div id=trimestre>
	</div>




