<script>
    $(document).ready(function(){
        $("input:submit, input:button").button();
        $('textarea').autoResize({
            // On resize:
            onResize : function() {
                $(this).css({opacity:0.8});
            },
            // After resize:
            animateCallback : function() {
                $(this).css({opacity:1});
            },
            // Quite slow animation:
            animateDuration : 300,
            // More extra space:
            extraSpace : 40,
            limit: 300
        });
        
        //Aplicando la función textlimit a cada textarea
        $("textarea").click(function(){
            $(this).each(function(){
                var idd = $(this).attr('id');
                var maximo = $(this).attr('maxlength');
                $(this).textlimit('span[id='+idd+']', maximo);
            });
        });

        base=$("#url_base").val();
        $("#actualizarMeta").hide();
        $("em").hide();
        id_meta = $('#id_meta').val();
        
        $("#editarMeta").click(function () {
            $('*').removeAttr('disabled');//le quita el disable a todos los inputs
            $("#editarMeta").hide();//oculando el boton de edtar
            $("#actualizarMeta").show();//aparece el boton de guardar
            $("em").show();
        });

        $("#updateGuardarMeta").validate({
            submitHandler: function() {
                var descripcion=$("#descripcion").val();
                var unidad_medida=$("#unidad_de_medida").val();
                var beneficiarios_hombres=$("#beneficiarios_hombres").val();
                var beneficiarios_mujeres=$("#beneficiarios_mujeres").val();
                var id_tipo_de_beneficiario=$("#id_tipo_de_beneficiario").val();
			
                var submit=2;

                $.ajax({
                    type: "POST",
                    url: base+"/metas",
                    data: "submit="+submit+"&descripcion="+descripcion+"&unidad_medida="+unidad_medida+"&beneficiarios_hombres="+beneficiarios_hombres+"&beneficiarios_mujeres="+beneficiarios_mujeres+"&id_tipo_de_beneficiario="+id_tipo_de_beneficiario+"&id_meta="+id_meta+"&id_objetivo="+id_objetivo,
                    success: function(msg){
                        alert("La meta se ha actualizado exitosamente.");
                        $('#divObjetivo').html(msg).show('slow');
                    }
                });
            }
        });

		$("button.capturaAvance").click(function (){
			id_meta = $('#id_meta').val();
			id_periodo = $('#id_periodo').val();
			$("#capturaAvance").hide();//ocultando el boton de edtar
			//alert("yuju");
			$("#avance").show();
			//$("em").show();
			$.ajax({
				type: "POST",
				url: base+"/showFormAvanceProgramatico/",
				data: "id_periodo="+id_periodo+"&id_meta="+id_meta,
				success: function(msg){
					$('#avance').html(msg).show('slow');
				}	
			});
		});

    });
</script>

<!-- form -->
    <input type="hidden" value="<?php echo $id_meta; ?>" name="id_meta" id="id_meta">
	<input type=hidden value="<?php echo $id_periodo;?>" name="id_periodo" id="id_periodo">
    <table class="info" align="center">
        <tfoot>
            <tr>
                <td colspan="2" style="text-align:center; border: 0px">
                    <?php if ($permisosMetas['editar']) {
                    ?>
                        <!-- input type="button" id="editarMeta"  value="Editar" -->
                        <input id="actualizarMeta"  type="submit" name="guardarUpdateMeta" class="guardarUpdateMeta" value="Guardar">
                    <?php } ?>
                </td>
            </tr>
        </tfoot>
        <thead>
            <tr>
                <th colspan="2">
                    <p class="title">Ver Meta</p>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="titulo">
                    <?php echo form_label($descripcion, $descripcion); ?>
                    <?php
                    $desc = array('value' => $meta['descripcion'], 'disabled' => '');
                    $fdescripcion1 = array_merge($desc, $fdescripcion);
                    ?>
                    <em>*</em>
                </td>
                <td><?php echo form_textarea($fdescripcion1); ?><span id="descripcion1"></span></td>
            </tr>
            <tr>
                <td class="titulo">
                    <label for="Unidad de Medida"><?php echo "Unidad de Medida"; ?></label>
                    <?php
                    $unidadmedida = array('value' => $meta['unidad_de_medida'], 'disabled' => '');
                    $funidad_de_medida1 = array_merge($unidadmedida, $funidad_de_medida);
                    ?>
                    <em>*</em>
                </td>
                <td><?php echo form_input($funidad_de_medida1); ?> (*Consulta el Manual para referencia)</td>
            </tr>
            <tr>
                <td class="titulo"><label for="Tipo de Beneficiario"><?php echo "Tipo de Beneficiario"; ?></label>
                    <?php $idtipobeneficiario = $meta['id_tipo_de_beneficiario']; ?>
                    <em>*</em>
                </td>
                <td>
                    <select id="id_tipo_de_beneficiario" name="id_tipo_de_beneficiario" title="Por favor elige una opci&oacute;n" disabled>
                        <?php
                        foreach ($tipoBeneficiario as $indice1 => $valor1) {
                            if ($idtipobeneficiario == $indice1) {
                                echo "<option value=$indice1 selected>$valor1</option>'";
                            } else {
                                echo "<option value=$indice1>$valor1</option>'";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="titulo">
                    <?php echo form_label($beneficiarios_hombres, $beneficiarios_hombres); ?>
                    <?php
                        $bh = array('value' => $meta['beneficiarios_hombres'], 'disabled' => '');
                        $fbeneficiarios_hombres1 = array_merge($bh, $fbeneficiarios_hombres);
                    ?>
                        <em>*</em>
                    </td>
                    <td>
                    <?php echo form_input($fbeneficiarios_hombres1); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                    <?php echo form_label($beneficiarios_mujeres, $beneficiarios_mujeres); ?>
                    <?php
                        $bm = array('value' => $meta['beneficiarios_mujeres'], 'disabled' => '');
                        $fbeneficiarios_mujeres1 = array_merge($bm, $fbeneficiarios_mujeres);
                    ?>
                        <em>*</em>
                    </td>
                    <td>
                    <?php echo form_input($fbeneficiarios_mujeres1); ?>
                </td>
            </tr>
        </tbody>
    </table> 
    <!-- captura avance para informes trimestrales -->
    
    <p> <button name="capturaAvance" id="capturaAvance"  class="capturaAvance" value="Captura">Captura Avance</button></p>

	<div id=avance>
	</div>

