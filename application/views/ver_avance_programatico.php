<script>
$(document).ready(function(){
	var id_meta = $('#id_meta').val();
	var id_periodo = $('#id_periodo').val(); 
	$.ajax({
		type: "POST",
		url: base+"/getAvanceProgramatico/",
		data: "id_periodo="+id_periodo+"&id_meta="+id_meta,
		success: function(msg){
			$('#mostrarCapturaAvance').html(msg).show();
		}
		
	});
    
    $('#guardarAvanceMeta').validate({
        
        submitHandler: function(){
            var id_tipo_de_beneficiario=$("#id_tipo_de_beneficiario").val();
            var ben_hombres=$("#ben_hombres_avance").val();
            var ben_mujeres=$("#ben_mujeres_avance").val();
            $.ajax({
                type: "POST",
                url: base+"/setAvanceProgramatico/",
                data: "ben_hombres="+ben_hombres+"&ben_mujeres="+ben_mujeres+"&id_tipo_de_beneficiario="+id_tipo_de_beneficiario+"&id_periodo="+id_periodo+"&id_meta="+id_meta,
                success: function(msg){
                    $('#mostrarCapturaAvance').html(msg).show('slow');
                }
            });
        }
	});
	
});
</script>
<div align="center">
<fieldset style="border: 1px solid gray; background-color: #CDCCB2; color: #382C1E; width: 300px; font-size:12px; ">
<form id=guardarAvanceMeta method="post">
		<h4>Avance Programatico</h4>
		<p>	
            <label>Beneficiarios Hombres</label>
            <em>*</em><input name="ben_hombres_avance" value="<?= $beneficiarios_hombres ?>" maxlength="15" size="10" class="required" id="ben_hombres_avance" type="text">
		</p>
		<p>
          <label>Beneficiarios Mujeres</label>
            <em>*</em><input name="ben_mujeres_avance" value="<?= $beneficiarios_mujeres ?>" maxlength="15" size="10" class="required" id="ben_mujeres_avance" type="text">
		</p>
        <input type="submit" value="Guardar">
</form>
</fieldset>
</div>
<div id = "mostrarCapturaAvance" align = "center">

</div>
		