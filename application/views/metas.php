<?php
if ($this->session->userdata('logged_in')) {//verificando 'logueo'
    ?>
    <br>
    <br>
    <?php echo script_tag('js/metas.js') ?>

    <input type="hidden" name="id_objetivo" id="id_objetivo" value="<?= $id_objetivo ?>">
    <input type="hidden" name="numMetas" id="numMetas" value="<?= $numMetas ?>">

    <?php if ($numMetas < 4 and $permisosMetas['agregar']) {
        ?>
        <input type="button" id="AgregarMet" value="Agregar Meta">
    <?php } ?>

    <br />

    <table class="info" id="metas" align="center">
        <caption>
            <span class="title">Metas :: Total <?= $numMetas ?></span>  <br />
            <span class="subtitle"> *Solo es posible capturar 4 metas por objetivo* </span>
        </caption>
        <thead>
            <tr>
                <th class="titulo">Meta</th>
                <th class="titulo">Unidad de Medida</th>
                <?php if ($permisosMetas['ver']) {
                    ?>
                    <th class="titulo">Detalle</th>
                    <th class="titulo">Informe Programático</th>
                <?php } ?>
                <?php
                if ($permisosMetas['borrar']) {
                    ?>
                    <th class="titulo">Eliminar</th>
                <?php } ?>
                <?php if ($permisosAcciones['login']) {
                    ?>
                    <th class="titulo">Acciones</th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach ($mostrarMetas as $indice) {
                ?>
                <tr class="seleccionar" id="<?php echo $indice->id_metas; ?>">
                    <td class="borde"><?php echo $indice->descripcion; ?></td>
                    <td class="borde"><?php echo $indice->unidad_de_medida; ?></td>
                    <?php if ($permisosMetas['ver']) {
                        ?>
                        <td class="ver" align="center"><span class="verMeta ui-icon ui-icon-circle-triangle-s" id="verMeta"><input type="hidden" value="<?= $indice->id_metas; ?>" id="meta"/></span>
                        <td class="ver" align="center"><span class="verInformeMeta ui-icon ui-icon-circle-triangle-s" id="verInformeMeta"><input type="hidden" value="<?= $indice->id_metas; ?>" id="meta"/></span>
                        <?php } ?>
                        <?php
                        if ($permisosMetas['borrar']) {
                            ?>
                        <td class="eliminar" align="center"><span class="eliminarMeta ui-icon ui-icon-circle-triangle-s" id="eliminarMeta"><input type="hidden" value="<?= $indice->id_metas; ?>" id="meta"/></span></td>
                    <?php } ?>
                    <?php if ($permisosAcciones['login']) {
                        ?>
                        <td class="ver" align="center"><span class="verAcciones ui-icon ui-icon-circle-triangle-s" id="verAcciones"><input type="hidden" value="<?= $indice->id_metas; ?>" id="meta"/></span>
                        <?php } ?>
                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>

    <div id="divMeta">
        <?php if ($permisosMetas['agregar']) {
            ?>
            <form id="insertarMeta">
                <table class="info" align="center">
                    <tfoot>
                        <tr align="center">
                            <td colspan="2">
                                <input type="submit" name="guardarMeta" class="guardarMeta" value="Guardar" />
                            </td>
                        </tr>
                    </tfoot>
                    <thead>
                    <th colspan="2">
                    <p class="title">Alta de Metas</p>
                    </th>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="titulo">
                                <?php echo form_label($descripcion, $descripcion); ?>
                                <em>*</em>
                            </td>
                            <td>
                                <?php echo form_textarea($fdescripcion); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">
                                <label for="Unidad de Medida"><?php echo "Unidad de Medida"; ?></label>
                                <em>*</em>
                            </td>
                            <td>
                                <?php echo form_input($funidad_de_medida); ?> (*Consulta el Manual para referencia)
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">
                                <label for="Tipo de Beneficiario"><?php echo "Tipo de Beneficiario"; ?></label>
                                <em>*</em>
                            </td>
                            <td>
                                <select id="id_tipo_de_beneficiario" name="id_tipo_de_beneficiario" title="Por favor elige una opci&oacute;n"  class="required">
                                    <option value="">Selecciona..</option>
                                    <?php
                                    foreach ($tipoBeneficiario as $indice1 => $valor1) {
                                        echo "<option value=$indice1>$valor1</option>'";
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">
                                <?php echo form_label($beneficiarios_hombres, $beneficiarios_hombres); ?>
                                <em>*</em>
                            </td>
                            <td>
                                <?php echo form_input($fbeneficiarios_hombres); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">
                                <?php echo form_label($beneficiarios_mujeres, $beneficiarios_mujeres); ?>
                                <em>*</em>
                            </td>
                            <td>
                                <?php echo form_input($fbeneficiarios_mujeres); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        <?php } ?>
    </div>

    <?php
} else {//sino está logueado
    $this->load->view('sinAcceso');
}
?>
