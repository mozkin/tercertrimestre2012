<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Informes Trimestrales 2011-2012</title>
        <?php echo link_tag('css/estilos.css') ?>
        <?php echo link_tag('css/css_menu.css') ?>
        <?php echo link_tag('css/forms.css') ?>
        <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
        <?php echo script_tag('js/jquery-1.5.2.min.js') ?>
        <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
        <?php echo script_tag('js/jquery.validate.js') ?>
        <?php echo script_tag('js/jquery_autoresize.js') ?>
        <?php echo script_tag('js/jquery_cascade.js') ?>
        <?php echo script_tag('js/jquery_cascade_ext.js') ?>
        <?php echo script_tag('js/jquery_confirm.js') ?>
        <?php echo script_tag('js/jquery_dimensions.js') ?>
        <?php echo script_tag('js/jquery_tooltip.js') ?>
        <?php echo script_tag('js/jquery.textlimit.js') ?>
        <?php echo script_tag('js/proyectos.js') ?>
    </head>

    <body>
        <input type="hidden" name="url_base" id="url_base" value="<?php echo site_url('proyecto/') ?>"></input>
        <div id="header" class="ui-priority-primary">
            <?php echo $sistema; ?><br/>
            <div id="subheader">
                <?php
                if ($es_PROFEN == 1) {
                    echo $escuela;
                } else if ($es_PROFEN == 2) {
                    echo $entidad;
                }
                ?>
            </div>
        </div>

        <div id="menu_nav"><?php echo $menu; ?></div>

        <div id="content" class="ui-widget-content ui-corner-all">
            <?php
                if ($permisosProyecto['ver']) {
            ?>
                    <table class="info" align="center">
                        <caption>Proyectos</caption>
                        <thead>
                            <tr>
                                <th class="titulo">Proyecto</th>
                                <th class="titulo">Tipo de Proyecto</th>
                        <?php if ($permisosProyecto['ver']) {
                        ?>
                            <th class="titulo">Detalle</th>
                        <?php } ?>
                        <?php
                        if ($permisosProyecto['borrar']) {
                        ?>
                            <th class="titulo">Eliminar</th>
                        <?php } ?>
                        <?php
                        if ($permisosObjetivo['ver']) {
                        ?>
                            <th class="titulo">Objetivos</th>
                        <?php } ?>
                    </tr>
                </thead>
                <?php
                        foreach ($proyectos as $indice) {
                ?>
                            <tr class="seleccionar" id="<?php echo $indice->id_proyecto; ?>">
                                <td class="borde"><?php echo $indice->proyecto; ?></td>
                                <td class="borde"><?php echo $indice->tipo_proyecto; ?></td>
                    <?php if ($permisosProyecto['ver']) {
                    ?>
                                <td class="ver" align="center"><span class="verProyecto ui-icon ui-icon-circle-triangle-s" id="verProyecto"><input type="hidden" value="<?= $indice->id_proyecto; ?>" id="proyecto"/></span></td>
                    <?php } ?>
                    <?php
                            if ($permisosProyecto['borrar']) {
                    ?>
                                <td class="eliminar" align="center"><span class="eliminarProyecto ui-icon ui-icon-circle-triangle-s" id="eliminarProyecto"><input type="hidden" value="<?= $indice->id_proyecto; ?>" id="proyecto"/></span></td>
                    <?php } ?>
                    <?php
                            if ($permisosObjetivo['ver']) {
                    ?>
                                <td class="ver" align="center"><span class="mostrarObjetivos ui-icon ui-icon-circle-triangle-s" id="mostrarObjetivos"><input type="hidden" value="<?= $indice->id_proyecto; ?>" id="proyecto"/></span></td>
                    <?php } ?>
                        </tr>
                <?php
                        }
                ?>
                    </table>
                    <div id="subContent">

                    </div>
            <?php
                    } else {
                        echo "No tienes permiso para ver esta sección";
                    }
            ?>
        </div>
    </body>
</html>
