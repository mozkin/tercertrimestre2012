<?php
$logged_in=$this->session->userdata('logged_in');
$id_usuario=$this->session->userdata('id_usuario');
$user=$this->session->userdata('user');
$es_PROFEN=$this->session->userdata('es_PROFEN');
$id_entidad=$this->session->userdata('id_entidad');
$id_escuela=$this->session->userdata('id_escuela');
//$periodo_id = $this->session->userdata('periodo_id');
if($logged_in){//verificando 'logueo'

?>
<script>
$(document).ready(function(){
	id_accion=$("#id_accion").val();
	base=$("#url_base").val();
	var rubro_id = $("#id_rubro").val(); 
	$.ajax({
		type: "POST",
		url: base+"/muestraPeriodos/",
		data: "id_rubro="+rubro_id,
		success: function(msg){
			$('#mostrarPeriodos').html(msg).show();
		}		
	});
	

	$("#guardarReportePeriodo").validate({

		 submitHandler: function() {
			var id_rubro=$("#id_rubro").val();
			var cantidad=$("#cantidad_").val();
			var precio_unitario=$("#precio_unitario_").val();
			var periodo_id=$("#periodo_id").val();
			$.ajax({ 
				type: "POST", 
				url: base+"/guardarReportePeriodo/",
				data: "id_rubro="+id_rubro+"&cantidad="+cantidad+"&precio_unitario="+precio_unitario+"&id_accion="+id_accion+"&periodo_id="+periodo_id,
				success: function(msg){ 
					$('#mostrarPeriodos').html(msg).show();
				} 
			});
		}
	});


});
</script>


	<?php 
		$url_base=site_url('/proyecto/');
	?>
	<input type=hidden name=url_base id=url_base value="<?=  $url_base ?>">
	<input type=hidden name=id_rubro id=id_rubro value="<?=  $id_rubro ?>">
	<input type=hidden name=id_accion id=id_accion value="<?=  $id_accion ?>">
	<input type=hidden name=periodo_id id=periodo_id value="<?= $periodo_id ?>">
	
<br />

	<fieldset style="border: 1px solid gray; background-color: #CDCCB2; color: #382C1E; width: 300px; font-size:12px; ">
	<form id=guardarReportePeriodo method="post">
		<h4>Reporte del trimestre: <br><? $id_periodo?></h4>
		<p>
            <label>Cantidad</label>
            <em>*</em><input name="cantidad_" value="" maxlength="15" size="10" class="required" id="cantidad_" type="text">
    	</p>
 		<p>
            <label>Precio unitario</label>
            <em>*</em><input name="precio_unitario_" value="" maxlength="15" size="10" class="required" id="precio_unitario_" type="text">
  		</p>
		<p>
			<input type=submit value=Guardar>
		</p>
    </form>
	</fieldset>
<div id="mostrarPeriodos"> 
</div>
<?php
	
}
else{//sino está logueado	
	$this->load->view('sinAcceso');

}
?>

