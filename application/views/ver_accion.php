<script>
    $(document).ready(function(){
        $("input:submit, input:button").button();
        $('textarea').autoResize({
            // On resize:
            onResize : function() {
                $(this).css({opacity:0.8});
            },
            // After resize:
            animateCallback : function() {
                $(this).css({opacity:1});
            },
            // Quite slow animation:
            animateDuration : 300,
            // More extra space:
            extraSpace : 40,
            limit: 300
        });

        //Aplicando la función textlimit a cada textarea
        $("textarea").click(function(){
            $(this).each(function(){
                var idd = $(this).attr('id');
                var maximo = $(this).attr('maxlength');
                $(this).textlimit('span[id='+idd+']', maximo);
            });
        });

        base=$("#url_base").val();
        $("#actualizarAccion").hide();

        $("#editarAccion").click(function () {
            $('*').removeAttr('disabled');//le quita el disable a todos los inputs
            $("#editarAccion").hide();//oculando el boton de edtar
            $("#actualizarAccion").show();//aparece el boton de guardar
            $("em").show();
        });
        
        $("#updateGuardarAccion").validate({
            submitHandler: function() {
                var descripcion1=$("#descripcion1").val();
                var descripcion2=$("#descripcion2").val();
                var accion=$("#accion").val();
                var id_accion=$("#id_accion").val();		
                var submit=2;

                $.ajax({
                    type: "POST",
                    url: base+"/acciones",
                    data: "submit="+submit+"&descripcion1="+descripcion1+"&descripcion2="+descripcion2+"&accion="+accion+"&id_accion="+id_accion+"&id_meta="+id_meta,
                    success: function(msg){
                        alert("La acción se ha actualizado exitosamente.");
                        $('#divMeta').html(msg).show('slow');
                    }
                });
            }
        });
        
        id_accion = $('#id_accion').val();
		id_periodo = $('#id_periodo').val();

		$("button.capturaAlcance").click(function () {
			$("#capturaAlcance").hide();//oculando el boton de edtar
			$("#alcance").show();//aparece el boton de guardar
			$("em").show();
			$.ajax({
				type: "POST",
				url: base+"/showFormAlcanceAccion/",
				data: "id_periodo="+id_periodo+"&id_accion="+id_accion,
				success: function(msg){
					$('#alcance').html(msg).show('slow');
				}
			});		 
		});

    });
</script>

<!-- form id="updateGuardarAccion" -->
    <input type="hidden" name="id_accion" id="id_accion"  value="<?php echo $id_accion; ?>">
    <input type=hidden value="<?php echo $id_periodo;?>" name=id_periodo id=id_periodo>

    <table  class="info" align="center">
        <tfoot>
            <tr>
                <td colspan="2" style="text-align:center; border: 0px">
                    <?php if ($permisosRubros['editar']) {
                    ?>
                        <!-- input type="button"  id="editarAccion" value="Editar" -->
                        <input type="submit" id="actualizarAccion" name="guardarUpdateAccion" class="guardarUpdateAccion" value="Guardar">
                    <?php } ?>
                </td>
            </tr>
        </tfoot>
        <thead>
            <tr>
                <th colspan="2">
                    <p class="title">Ver Acci&oacute;n</p>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="titulo"> <?php echo form_label($accion, $accion); ?>
                    <?php
                    $acc = array('value' => $accion1['accion'], 'disabled' => '');
                    $faccion1 = array_merge($acc, $faccion);
                    ?>
                    <em>*</em></td>
                <td>
                    <?php echo form_textarea($faccion1); ?><span id="accion"></span>
                </td>
            </tr>
            <tr>
                <td class="titulo">
                    <?php echo form_label($descripcion1, $descripcion1); ?>
                    <?php
                    $desc = array('value' => $accion1['descripcion1'], 'disabled' => '');
                    $fdescripcion1 = array_merge($desc, $fdescripcion1);
                    ?>
                    <em>*</em>
                </td>
                <td>
                    <?php echo form_textarea($fdescripcion1); ?><span id="descripcion1"></span>
                </td>
            </tr>
            <tr>
                <td class="titulo">
                    <?php echo form_label($descripcion2, $descripcion2); ?>
                    <?php
                    $desc = array('value' => $accion1['descripcion2'], 'disabled' => '');
                    $fdescripcion2 = array_merge($desc, $fdescripcion2);
                    ?>
                    <em>*</em>
                </td>
                <td>
                    <?php echo form_textarea($fdescripcion2); ?><span id="descripcion2"></span>
                </td>
            </tr>

        </tbody>
    </table>
<!-- /form -->
<p><button  name="capturaAlcance" id="capturaAlcance" class="capturaAlcance" > Captura Alcances de Trimestre </button></p>
	<div id=alcance>
	</div>



