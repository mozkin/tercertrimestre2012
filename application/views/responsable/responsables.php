<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Informes Trimestrales 2011-2012</title>
        <?php echo link_tag('css/estilos.css') ?>
        <?php echo link_tag('css/css_menu.css') ?>
        <?php echo link_tag('css/forms.css') ?>
        <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
        <?php echo script_tag('js/jquery-1.6.1.min.js') ?>
        <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
        <?php echo script_tag('js/jquery.validate.js') ?>
        <?php echo script_tag('js/responsable.js') ?>
    </head>

    <body>
        <div id="header" class="ui-priority-primary">
            <?php echo $sistema; ?><br/>
            <div id="subheader">
                <?php
                if ($es_PROFEN == 1) {
                    echo $escuela;
                } else if ($es_PROFEN == 2) {
                    echo $entidad;
                }
                ?>
            </div>
        </div>

        <div id="menu_nav"><?php echo $menu; ?></div>

        <div id="content" class="ui-widget-content ui-corner-all">
            <?php
                if ($permisosResponsable['ver']) {
            ?>
                    <table class="info" id="tabla_responsable" align="center">
                        <caption>
                            <span class="title">Responsables</span>
                        </caption>
                        <thead>
                            <tr>
                                <th class="titulo">Nombre</th>
                                <th class="titulo">Cargo</th>
                        <?php if ($permisosResponsable['ver']) {
                        ?>
                            <th class="titulo">Detalle</th>
                        <?php } ?>
                        <?php
                        if ($permisosResponsable['borrar']) {
                        ?>
                            <th class="titulo">Eliminar</th>
                        <?php } ?>

                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($responsables as $res) {
                    ?>
                            <tr class="seleccionar">
                                <td class="borde"><?php echo $res->nombre; ?></td>
                                <td class="borde"><?php echo $res->cargo; ?></td>
                        <?php if ($permisosResponsable['ver']) {
                        ?>
                                <td class="ver" align="center"><span class="ver_responsable ui-icon ui-icon-circle-triangle-s" id="ver_responsable"><input type="hidden" value="<?= $res->id_responsable; ?>" id="responsable"/></span></td>
                        <?php } ?>
                        <?php
                            if ($permisosResponsable['borrar']) {
                        ?>
                                <td class="eliminar" align="center"><span class="eliminar_responsable ui-icon ui-icon-circle-triangle-s" id="eliminar_responsable"><input type="hidden" value="<?= $res->id_responsable; ?>" id="responsable"/></span></td>
                        <?php } ?>
                        </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            <?php
                    } else {
                        $this->load->view('sin_acceso');
                    }
            ?>
        </div>

    </body>
</html>
