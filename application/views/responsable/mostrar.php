<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Informes Trimestrales 2011-2012</title>
        <?php echo link_tag('css/estilos.css') ?>
        <?php echo link_tag('css/css_menu.css') ?>
        <?php echo link_tag('css/forms.css') ?>
        <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
        <?php echo script_tag('js/jquery-1.6.1.min.js') ?>
        <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
        <?php echo script_tag('js/jquery.validate.js') ?>
        <?php echo script_tag('js/responsable.js') ?>
    </head>

    <body>
        <div id="header" class="ui-priority-primary">
            <?php echo $sistema; ?><br/>
            <div id="subheader">
                <?php
                if ($es_PROFEN == 1) {
                    echo $escuela;
                } else if ($es_PROFEN == 2) {
                    echo $entidad;
                }
                ?>
            </div>
        </div>

        <div id="menu_nav"><?php echo $menu; ?></div>

        <div id="content" class="ui-widget-content ui-corner-all">
            <?php
                if ($permisosResponsable['ver']) {
            ?>
                    <form id="formResponsable" name="formResponsable">
                        <table class="info" align="center">

                    <?php
                    foreach ($infoResponsable as $indice) {
                        $id_responsable = $indice->id_responsable;
                        $nombre = $indice->nombre;
                        $cargo = $indice->cargo;
                        $telefono = $indice->telefono;
                        $email = $indice->email;
                        $id_grado_academico = $indice->id_grado_academico;
                    ?>
                        <input type="hidden" name="responsableUpd" id="responsableUpd" value="<?php echo $id_responsable ?>" />

                            <tr>
                                <td class="titulo">Responsable</td>
                                <td><em>*</em><input type="text" class="required" name="nombre" value="<?= $nombre ?>" size="30" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td class="titulo">Cargo</td>
                                <td><em>*</em><input type="text" class="required" name="cargo" value="<?= $cargo ?>" size="30" disabled="disabled" /></td>
                            </tr>
                            <tr>
                                <td class="titulo">Grado académico</td>
                                <td><em>*</em>
                                    <select id="id_grado_academico" name="id_grado_academico"  disabled="disabled" class="required">
                                    <?php
                                    foreach ($gradosAcademicos as $indice => $valor) {
                                        if ($id_grado_academico == $indice) {
                                            echo '<option value="' . $indice . '" selected="selected">' . $valor . '</option>';
                                        } else {
                                            echo '<option value="' . $indice . '">' . $valor . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">Teléfono</td>
                            <td><em>*</em><input type="text" class="required" name="telefono" value="<?= $telefono ?>" size="30" disabled="disabled" /></td>
                        </tr>
                        <tr>
                            <td class="titulo">Correo electrónico</td>
                            <td><em>*</em><input type="text" class="email required" name="email" value="<?= $email ?>" size="30" disabled="disabled" /></td>
                        </tr>

                        <?php
                                }
                        ?>
                                <tr>
                                    <td colspan="2" style="text-align:center; border: 0px">
<?php if ($permisosResponsable['editar']) { ?>
                                    <input type="button" id="editarResponsable" value="Editar" />
                                    <input type="submit" id="updateResponsable" value="Guardar"  />
<?php } ?>
                            </td>
                        </tr>
                </table>
            </form>
            <?php
                            } else {
                                $this->load->view('sin_acceso');
                            }
            ?>
        </div>

    </body>
</html>

