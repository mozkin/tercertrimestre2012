<?php
if ($this->session->userdata('logged_in')) {//verificando 'logueo'
?>
    <br />
    <br />
<?php echo script_tag('js/acciones.js') ?>

    <input type="hidden" name="id_meta" id="id_meta" value="<?= $id_meta ?>">
    <input type="hidden" name="numAcciones" id="numAcciones" value="<?= $numAcciones ?>">

<?php if ($permisosAcciones['agregar']) {
?>
        <input type="button" id="AgregarAcc" value="Agregar Acci&oacute;n">
<?php } ?>

    <table class="info" id="acciones" align="center">
        <caption>
            <span class="title">Acciones :: Total <?= $numAcciones ?></span>  <br />
        </caption>
        <thead>
            <tr>
                <th class="titulo">Acci&oacute;n</th>
            <?php if ($permisosAcciones['ver']) {
            ?>
                <th class="titulo">Detalle</th>
            <?php } ?>
            <?php
            if ($permisosAcciones['borrar']) {
            ?>
                <th class="titulo">Eliminar</th>
            <?php } ?>
            <?php if ($permisosRubros['login']) {
            ?>
                <th class="titulo">Rubros</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach ($mostrarAcciones as $indice) {
        ?>
                <tr class="seleccionar" id="<?php echo $indice->id_accion; ?>">
                    <td class="borde"><?php echo $indice->accion; ?></td>

            <?php if ($permisosAcciones['ver']) {
            ?>
                    <td class="ver" align="center"><span class="verAccion ui-icon ui-icon-circle-triangle-s" id="verAccion"><input type="hidden" value="<?= $indice->id_accion; ?>" id="id_acc"/></span>
                <?php } ?>
                <?php
                if ($permisosAcciones['borrar']) {
                ?>
                <td class="eliminar" align="center"><span class="eliminarAccion ui-icon ui-icon-circle-triangle-s" id="eliminarAccion"><input type="hidden" value="<?= $indice->id_accion; ?>" id="id_acc"/></span></td>
            <?php } ?>
            <?php if ($permisosRubros['login']) {
            ?>
                    <td class="ver" align="center"><span class="verRubros ui-icon ui-icon-circle-triangle-s" id="verRubros"><input type="hidden" value="<?= $indice->id_accion; ?>" id="id_acc"/></span>
                <?php } ?>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>

    <div id="divAccion">

    <?php if ($permisosAcciones['agregar']) {
    ?>
                <form id="insertarAccion">
                    <table class="info" align="center">
                        <tfoot>
                            <tr align="center">
                                <td colspan="2">
                                    <input type="submit" name="guardarAccion" class="guardarAccion" value="Guardar" />
                                </td>
                            </tr>
                        </tfoot>
                        <thead>
                        <th colspan="2">
                            <p class="title">Alta de Acciones</p>
                        </th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="titulo">
                        <?php echo form_label($accion, $accion); ?>
                        <em>*</em>
                    </td>
                    <td>
                        <?php echo form_textarea($faccion); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                        <?php echo form_label($descripcion1, $descripcion1); ?>
                        <em>*</em>
                    </td>
                    <td>

                        <?php echo form_textarea($fdescripcion1); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                        <?php echo form_label($descripcion2, $descripcion2); ?>
                        <em>*</em>
                    </td>
                    <td>

                        <?php echo form_textarea($fdescripcion2); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <?php } ?>
                </div>
<?php
                } else {//sino está logueado
                    $this->load->view('sinAcceso');
                }
?>
