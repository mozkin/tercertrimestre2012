<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Informes Trimestrales 2011-2012</title>
    <?php echo link_tag('css/estilos.css') ?>
    <?php echo link_tag('css/css_menu.css') ?>
    <?php echo link_tag('css/forms.css') ?>
    <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
    <?php echo script_tag('js/jquery-1.5.2.min.js') ?>
    <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
    <?php echo script_tag('js/jquery.validate.js') ?>
    <?php echo script_tag('js/login.js') ?>
    
    <!-- <link href="http://view.jqueryui.com/menu/themes/base/jquery.ui.menu.css" rel="stylesheet" type="text/css"/> -->
    <!-- <script src="http://view.jqueryui.com/menu/ui/jquery.ui.menu.js"></script> -->



  </head>

  <body>
    <?php echo $menu;  ?>
    <div id="content">

    </div>

  </body>
</html>
