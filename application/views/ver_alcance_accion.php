<?php
$logged_in=$this->session->userdata('logged_in');
$id_usuario=$this->session->userdata('id_usuario');
$user=$this->session->userdata('user');
$es_PROFEN=$this->session->userdata('es_PROFEN');
$id_entidad=$this->session->userdata('id_entidad');
$id_escuela=$this->session->userdata('id_escuela');
$periodo_id = $this->session->userdata('periodo_id');
if($logged_in){//verificando 'logueo'

?>

<script>
$(document).ready(function(){
    id_accion = $('#id_accion').val();
    base=$("#url_base").val();
    $("#guardar").hide();
    $("em").hide();
    id_periodo = $('#id_periodo').val();
    $.ajax({
        type: "POST",
        url: base+"/getAlcanceAccion/",
        data: "id_periodo="+id_periodo+"&id_accion="+id_accion,
        success: function(msg){
            $('#mostrarCapturaAlcance').html(msg).show();
        }

    });

    $("#guardarAlcanceAccion").validate({
        
        submitHandler: function() {
            especificaciones=$("#especificacion_reporte").val();
            breve_descripcion=$("#breve_descripcion").val();
            $.ajax({
                type: "POST",
                url: base+"/setAlcanceAccion/",
                data: "especificaciones="+especificaciones+"&breve_descripcion="+breve_descripcion+"&id_periodo="+id_periodo+"&id_accion="+id_accion,
                success: function(msg){
                    $('#mostrarCapturaAlcance').html(msg).show();
                }
            });
        }
    });
});

</script>
<input type=hidden name=id_accion id=id_accion value="<?=  $id_accion ?>">

<div align="center">
<fieldset style="border: 1px solid gray; background-color: #CDCCB2; color: #382C1E; width: 500px; font-size:12px; ">
    <form id=guardarAlcanceAccion method="post">
        <h4>Avance de la Acción</h4>
        <p>
            <label>Especificacion: </label>
            <em>*</em><textarea name="especificacion_reporte" class="required" id="especificacion_reporte" rows="5" cols="50" minlength= "5" maxlength="250"> </textarea>
        </p>

        <p>
            <label>Breve descripcion: </label>
            <em>*</em><textarea name="breve_descripcion" class="required" id="breve_descripcion" rows="5" cols="50" minlength= "5" maxlength="250"></textarea>
        </p>
        <input type="submit" value="Guardar">
    </form>
</fieldset>
</div>
<div id = "mostrarCapturaAlcance" align = "center">
</div>
<?php

}
else{//sino está logueado
	$this->load->view('sinAcceso');

}
?>
