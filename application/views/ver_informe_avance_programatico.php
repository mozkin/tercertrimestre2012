<script>
    $(document).ready(function() {
        var id_meta = $('#id_meta').val();
        var id_periodo = $('#id_periodo').val();
        $.ajax({
            type: "POST",
            url: base + "/getInformeAvanceProgramatico/",
            data: "id_periodo=" + id_periodo + "&id_meta=" + id_meta,
            success: function(msg) {
                $('#mostrarCapturaAvance').html(msg).show();
            }

        });

        $('#guardarInformeAvanceMeta').validate({
            submitHandler: function() {

                var medida_alcanzada = $("#medida_alcanzada").val();
                var porcentaje_alcanzado = $("#porcentaje_alcanzado").val();
                var justificacion = $("#justificacion").val();

                $.ajax({
                    type: "POST",
                    url: base + "/setInformeAvanceProgramatico/",
                    data: "medida_alcanzada=" + medida_alcanzada + "&porcentaje_alcanzado=" + porcentaje_alcanzado + "&justificacion=" + justificacion + "&id_periodo=" + id_periodo + "&id_meta=" + id_meta,
                    success: function(msg) {
                        $('#mostrarCapturaAvance').html(msg).show('slow');
                    }
                });
            }
        });

    });
</script>
<div align="center">
    <fieldset style="border: 1px solid gray; background-color: #CDCCB2; color: #382C1E; width: 300px; font-size:12px; ">
        <form id=guardarInformeAvanceMeta method="post">
            <h4>Informe Avance Programatico</h4>
            <p>
                <label>Unidad de Medida Alcanzada</label>
                <em>*</em><input name="medida_alcanzada" value="<?= $medida_alcanzada ?>" maxlength="15" size="10" class="required" id="medida_alcanzada" type="text">
            </p>
            <p>
                <label>Porcentaje Alcanzado</label>
                <em>*</em><input name="porcentaje_alcanzado" value="<?= $porcentaje_alcanzado ?>" maxlength="15"  class="required" id="porcentaje_alcanzado" type="text">
            </p>
            <p>
                <label>Justificación</label>
                <em>*</em><input name="justificacion" value="<?= $justificacion ?>" size="50" class="required" id="justificacion" type="text">
            </p>
            <input type="submit" value="Guardar">
        </form>
    </fieldset>
</div>
<div id = "mostrarCapturaAvance" align = "center">

</div>
