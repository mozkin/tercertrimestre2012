<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Informes Trimestreles 2011-2012</title>
        <?php echo link_tag('css/login.css') ?>
        <?php echo link_tag('css/forms.css') ?>
        <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
        <?php echo script_tag('js/jquery-1.5.2.min.js') ?>
        <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
        <?php echo script_tag('js/jquery.validate.js') ?>
        <?php echo script_tag('js/login.js') ?>

    </head>

    <body>
        <div id="content">
            <?php
            if ($log != 5) {
            ?>
                <p class=" ui-state-error ui-corner-all">
                    <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <?php echo $log_texto; ?>
            </p>
            <?php } ?>
            <!--class="ui-widget-header ui-corner-all"-->
            <?php $attributes = array('id' => 'login');
            echo form_open('proyecto/verificarLogin', $attributes); ?>
            <table id="login" class="ui-corner-all ui-widget-header" >
                <thead>
                    <tr>
                        <td colspan="2">
                            <p class="title">Informes Trimestrales 2011-2012</p>
                        </td>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="loginform">
                        <td colspan="2">
                            <input type="submit" name="login" value="Entrar" />
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td class="loginform">
                            Usuario
                        </td>
                        <td>
                            <input type="text" name="usuario" />
                        </td>
                    </tr>
                    <tr>
                        <td class="loginform">
                            Contrase&ntilde;a
                        </td>
                        <td>
                            <input type="password" name="contrasena" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <?php echo form_close(); ?>
            <div id="aviso" class="ui-corner-all">
                <span>
                    Es indispensable usar el navegador Firefox 3.0 o superior (desc&aacute;rguelo <a href="http://www.mozilla.com">aqu&iacute; </a>)
        ,  o el navegador Google Chrome (desc&aacute;rguelo <a href="http://http://www.google.com/chrome?hl=es">aqu&iacute; </a>).
                </span>
            </div>
        </div>
    </body>
</html>
