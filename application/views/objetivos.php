<?php
if ($this->session->userdata('logged_in')) {//verificando 'logueo'
?>
    <br />
    <br />
<?php echo script_tag('js/objetivos.js') ?>

    <input type="hidden" name="id_proyecto" id="id_proyecto" value="<?= $id_proyecto ?>">
    <input type="hidden" name="numObjetivos" id="numObjetivos" value="<?= $numObjetivos ?>">

<?php if ($numObjetivos < 6 and $permisosObjetivo['agregar'] == 1) {
?>
        <input id="AgregarObj" type="button" value="Agregar Objetivo" />
<?php } ?>

    <br />

    <table class="info" id="objetivos" align="center">
        <caption>
            <span class="title">Objetivos :: Total <?= $numObjetivos ?></span>  <br />
            <span class="subtitle"> *Solo es posible capturar 6 objetivos por proyecto* </span>
        </caption>
        <thead>
            <tr>
                <th class="titulo">Objetivo</th>
                <th class="titulo">Subtipo</th>
            <?php if ($permisosObjetivo['ver']) {
            ?>
                <th class="titulo">Detalle</th>
            <?php } ?>
            <?php
            if ($permisosObjetivo['borrar']) {
            ?>
                <th class="titulo">Eliminar</th>
            <?php } ?>
            <?php if ($permisosMetas['ver']) {
            ?>
                <th class="titulo">Metas</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>

        <?php
            foreach ($objetivos as $indice) {
        ?>
                <tr class="seleccionar" id="<?php echo $indice->id_objetivos; ?>">
                    <td class="borde"><?php echo $indice->descripcion; ?></td>
                    <td class="borde"><?php echo $indice->subtipo_de_objetivo; ?></td>
            <?php if ($permisosObjetivo['ver']) {
            ?>
                    <td class="ver" align="center"><span class="verObjetivo ui-icon ui-icon-circle-triangle-s" id="verObjetivo"><input type="hidden" value="<?= $indice->id_objetivos; ?>" id="objetivo"/></span>
                <?php } ?>
                <?php
                if ($permisosObjetivo['borrar']) {
                ?>
                <td class="eliminar" align="center"><span class="eliminarObjetivo ui-icon ui-icon-circle-triangle-s" id="eliminarObjetivo"><input type="hidden" value="<?= $indice->id_objetivos; ?>" id="objetivo"/></span></td>
            <?php } ?>
            <?php if ($permisosMetas['ver']) {
            ?>
                    <td class="ver" align="center"><span class="verMetas ui-icon ui-icon-circle-triangle-s" id="verMetas"><input type="hidden" value="<?= $indice->id_objetivos; ?>" id="objetivo"/></span>
                <?php } ?>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>

    <div id="divObjetivo">

    <?php if ($permisosObjetivo['agregar']) {
    ?>
                <form id="insertarObjetivo">
                    <table class="info" align="center">
                        <tfoot>
                            <tr align="center">
                                <td colspan="2">
                                    <input type="submit" name="guardarObjetivo" class="guardarObjetivo" value="Guardar" />
                                </td>
                            </tr>
                        </tfoot>
                        <thead>
                        <th colspan="2">
                            <p class="title">Alta de Objetivos </p>
                        </th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="titulo">
                        <?php echo form_label($objetivo_particular, $objetivo_particular); ?>
                        <em>*</em>
                    </td>
                    <td>
                        <?php echo form_textarea($fobjetivo_particular); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                        <?php echo form_label($justificacion, $justificacion); ?>
                        <em>*</em>
                    </td>
                    <td>
                        <?php echo form_textarea($fjustificacion); ?>
                    </td>
                </tr>
                <tr>
                    <td class="titulo">
                        <label for="SubTipo de objetivo"><?php echo "Sub Tipo de objetivo"; ?></label>
                        <em>*</em>
                    </td>
                    <td>
                        <select id="id_subtipo_de_objetivo" name="id_subtipo_de_objetivo" title="Por favor elige una opci&oacute;n" class="required">
                            <option value="">Selecciona...</option>
                            <?php
                            foreach ($tipo_subtipoObjetivo_arr as $indice1 => $valor1) {
                                echo "<option value=$indice1>$valor1</option>'";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    <?php } ?>
                    </div>

<?php
                    } else {//sino está logueado
                        $this->load->view('sin_acceso');
                    }
?>


