<?php
echo script_tag('js/proyectos.js');
?>

<br />
<br />
<form id="formProyecto" name="formProyecto">
    <input type="hidden" name="id_proyecto" id="id_proyecto" value="<?php echo $id_proyecto; ?>">
    <table class="info" align="center">
        <caption>Ver Proyecto</caption>
        <tfoot>
            <tr>
                <td colspan="2" style="text-align:center; border: 0px">
                    <?php if ($permisosProyecto['editar']) {
 ?>
                        <input type="button" id="editarProyecto" value="Editar" />
                        <input type="submit" id="updateProyecto" value="Guardar"  />
<?php } ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <tr>
                <td class="titulo">Proyecto Integral:</td>
                <td><?php
                    $nom = array('value' => $infoProyecto['proyecto'], 'disabled' => '');
                    $fnombre1 = array_merge($nom, $fnombre);
?>
                    <em>*</em><?php echo form_textarea($fnombre1); ?><span id="nombre"></span> </td>
            </tr>
            <tr>
                <td class="titulo">Objetivo General:</td>
                <td><?php
                    $obj = array('value' => $infoProyecto['objetivo'], 'disabled' => '');
                    $fobjetivo1 = array_merge($obj, $fobjetivo);
?>
                    <em>*</em><?php echo form_textarea($fobjetivo1); ?><span id="objetivo"></span></td>
            </tr>
            <tr>
                <td class="titulo">Justificaci&oacute;n del Proyecto: </td>
                <td><?php
                    $jus = array('value' => $infoProyecto['justificacion'], 'disabled' => '');
                    $fjustificacion1 = array_merge($jus, $fjustificacion);
?>
                    <em>*</em><?php echo form_textarea($fjustificacion1); ?><span id="justificacion"></span> </td>
            </tr>
            <tr>
                <td class="titulo">Tipo Proyecto:</td>
                <td><?php echo $infoProyecto['tipo_proyecto']; ?>
                </td>
            </tr>
            <tr>
                <td class="titulo">Responsable:</td>
                <td><?php echo $infoProyecto['responsable']; ?></td>
            </tr>
        </tbody>
    </table>
</form>