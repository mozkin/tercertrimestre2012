<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Informes Trimestrales 2011-2012</title>
   <?php echo link_tag('css/estilos.css') ?>
   <?php echo link_tag('css/css_menu.css') ?>
   <?php echo link_tag('css/forms.css') ?>
   <?php echo link_tag('css/jquery-ui-1.8.13.custom.css') ?>
   <?php echo script_tag('js/jquery-1.6.1.min.js') ?>
   <?php echo script_tag('js/jquery-ui-1.8.13.custom.min.js') ?>
   <?php echo script_tag('js/jquery.validate.js') ?>
   <?php echo script_tag('js/responsable.js') ?>
   </head>

<body>
   <input type="hidden" name="url_base" id="url_base" value="<?php echo site_url('proyecto/') ?>" />
   <div id="header" class="ui-priority-primary">
     <?php echo $sistema;  ?><br/>
     <div id="subheader">
     <?php if($es_PROFEN==1){
             echo $escuela;
           }else if($es_PROFEN==2){
             echo $entidad;
           } 
     ?>
     </div>
   </div>

<div id="menu_nav"><?php echo $menu;  ?></div>

<div id="content" class="ui-widget-content ui-corner-all">
   <?php 
   if($permisosResponsable['ver']){
   ?>
   <?php $this->load->view('verResponsable', $permisosResponsable); ?>
   <?php
   }
   else{
     echo "No tienes permiso para ver esta sección";
   }
   ?>
</div>

</body>
</html>
