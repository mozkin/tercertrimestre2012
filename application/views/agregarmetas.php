<?php
$logged_in=$this->session->userdata('logged_in');
$id_usuario=$this->session->userdata('id_usuario');
$user=$this->session->userdata('user');
$tipo_usuario=$this->session->userdata('tipo_usuario');
$id_entidad=$this->session->userdata('id_entidad');
$id_escuela=$this->session->userdata('id_escuela');

//tipo de objetivos
$arrUnidadMedida[0] = 'Selecciona';
foreach($unidadMedida_arr as $indice){
	$arrUnidadMedida[$indice->id_unidad_de_medida] = $indice->unidad_de_medida;
}

//subtipo de objetivos
$arrSubTipoObjetivos[0] = 'Selecciona';
foreach($subtipoObjetivo_arr as $indice){
	$arrSubTipoObjetivos[$indice->id_subtipo_de_objetivo] = $indice->subtipo_de_objetivo;
}



	 echo form_open("Proyecto/metas",array('id' => 'FormularioMetas')); 
	 echo form_hidden('id_entidad', $id_entidad);


?>
 	<fieldset>
	<legend class=title>alta de metas</legend>

    <p>
     <?php echo form_label($objetivo_particular, $objetivo_particular);?>
     <em>*</em><?php echo form_textarea($fobjetivo_particular);?>
   </p>

    <p>
     <?php echo form_label($area_responsable, $area_responsable);?>
     <em>*</em><?php echo form_input($farea_responsable);?>
   </p>

    <p>
     <?php echo form_label($justificacion, $justificacion);?>
     <em>*</em><?php echo form_input($fjustificacion);?>
   </p>

    <p>
     <label for="Tipo de objetivo"><?php echo "Tipo de objetivo"; ?></label>
     <em>*</em>
			<select id="id_tipo_de_objetivo" name="id_tipo_de_objetivo" title="Por favor elige una opci&oacute;n" >
			<?php
				foreach($arrTipoObjetivos as $indice1=>$valor1){
					echo "<option value=$indice1>$valor1</option>'";
				}
			?>
			</select>
   </p>

    <p>
     <label for="subTipo de objetivo"><?php echo "Subtipo de objetivo"; ?></label>
     <em>*</em>
			<select id="id_subtipo_de_objetivo" name="id_subtipo_de_objetivo" title="Por favor elige una opci&oacute;n" >
			<?php
				foreach($arrSubTipoObjetivos as $indice1=>$valor1){
					echo "<option value=$indice1>$valor1</option>'";
				}
			?>
			</select>
   </p>
  <p>
	<? $class="class='btn_enviar'";
		echo form_submit('guardarObjetivos','Guardar',$class );  ?>
	</p>


 </fieldset>
 
<?php
	 echo form_close(); 
?>
