<?php
if ($this->session->userdata('logged_in')) {//verificando 'logueo'
?>
    <br>
    <br>
<?php echo script_tag('js/rubros.js') ?>

    <input type="hidden" name="id_accion" id="id_accion" value="<?= $id_accion ?>">

<?php if ($permisosRubros['agregar']) {
?>
        <!-- input type="button" id="AgregarRub" value="Agregar Rubros" -->
<?php } ?>

    <table class="info" id="rubros" align="center">
        <caption>
            <p class="title">Rubros</p>
        </caption>
        <tbody>
            <tr>
                <td>
                    <table class="info" style='width:350px' id="rubro1">
                        <caption>
                            <p class="title">2012</p>
                        </caption>
                        <thead>
                            <tr>
                                <th class="titulo" width="100px">Clasificador</th>
                                <th class="titulo">Total</th>
                            <?php if ($permisosRubros['ver']) {
                            ?>
                                <th class="titulo">Ver</th>
                            <?php } ?>
                            <?php if ($permisosRubros['borrar']) {
                            ?>
                                <!-- th class="titulo">Eliminar</th -->
                            <?php } ?>
                        </tr>
                    </thead>

                    <?php
                            $total = 0;
                            foreach ($mostrarRubros1 as $indice) {
                                $subtotal = $indice->cantidad * $indice->precio_unitario;
                                $total = $subtotal + $total;
                    ?>

                                <tr class="seleccionar" id="<?php echo $indice->id_rubro; ?>">
                                    <td class="borde"><?php echo $indice->clasificador; ?></td>
                                    <td class="borde"><?php echo $subtotal; ?></td>

                        <?php if ($permisosRubros['ver']) {
                        ?>
                                    <td class="ver" align="center"><span class="verRubro ui-icon ui-icon-circle-triangle-s" id="verRubro"><input type="hidden" value="<?= $indice->id_rubro; ?>" id="rubro"/></span></td>
                            <?php } ?>
                            <?php if ($permisosRubros['borrar']) {
                            ?>
                                <!-- td class="ver" align="center"><span class="eliminarRubro ui-icon ui-icon-circle-triangle-s" id="eliminarRubro"><input type="hidden" value="<?= $indice->id_rubro; ?>" id="rubro"/></span></td -->
                            <?php } ?>
                        </tr>
                    <?php } ?>
                            <tr>
                                <td><b>Total</b></td>
                                <td align="right"><b>$<?php echo $total; ?></b></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        
                    </td>
                </tr>        
            </tbody>
        </table>


        <div id="divRubro" >
    <?php if ($permisosRubros['agregar']) {
    ?>
                                <form id="insertarRubro" >
                                    <table class="info" align="center">
                                        <tfoot>
                                            <tr align="center">
                                                <td colspan="2">
                                                    <input type="submit" name="guardarRubro" class="guardarRubro" value="Guardar" />
                                                </td>
                                            </tr>
                                        </tfoot>
                                        <thead>
                                        <th colspan="2">
                                            <p class="title">Alta de Rubros</p>
                                        </th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="titulo">
                        <?php echo form_label($anyo, $anyo); ?>
                                <em>*</em>
                            </td>
                            <td>
                                <select id="anyo" name="anyo" title="Por favor elige una opci&oacute;n" class=required>
                                    <option value="">Selecciona...</option>
                            <?php
                                foreach ($anyos as $indice1 => $valor1) {
                                    echo "<option value=$indice1>$valor1</option>'";
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo">
                            <label for="Rubro de gasto"><?php echo "Rubro de gasto"; ?></label>
                            <em>*</em>
                        </td>
                        <td>
                            <select id="id_clasificador" name="id_clasificador" title="Por favor elige una opci&oacute;n" class=required>
                                <option value="">Selecciona...</option>
                            <?php
                                foreach ($clasificador as $indice1 => $valor1) {
                                    echo "<option value=$indice1>$valor1</option>'";
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo">
                        <?php echo form_label($cantidad, $cantidad); ?>
                                <em>*</em>
                            </td>
                            <td>
                        <?php echo form_input($fcantidad); ?>
                            </td>
                        </tr>
                    <p  id="divmedida">
                        <label for="Metros cuadrados" id="metroslabel">Metros cuadrados</label>
                        <input type="text" name="metros" id="metros" value="0">
                    </p>
                    <tr>
                        <td class="titulo">
                    <?php echo form_label($precio_unitario, $precio_unitario); ?>
                                <em>*</em>
                            </td>
                            <td>

                    <?php echo form_input($fprecio_unitario); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="titulo">
                                <label for="Periodo de Aplicacion"><?php echo "Periodo de Aplicacion"; ?></label>
                                <em>*</em>
                            </td>
                            <td>
                                <select id="id_periodo_aplicacion" name="id_periodo_aplicacion" title="Por favor elige una opci&oacute;n" class=required>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
    <?php } ?>
                        </div>
<?php
                        } else {//sino está logueado
                            $this->load->view('sinAcceso');
                        }
?>

