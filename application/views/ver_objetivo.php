<script>
    $(document).ready(function(){
        $("input:submit, input:button").button();
        $('textarea').autoResize({
            // On resize:
            onResize : function() {
                $(this).css({opacity:0.8});
            },
            // After resize:
            animateCallback : function() {
                $(this).css({opacity:1});
            },
            // Quite slow animation:
            animateDuration : 300,
            // More extra space:
            extraSpace : 40,
            limit: 300
        });

        //Aplicando la función textlimit a cada textarea
        $("textarea").click(function(){
            $(this).each(function(){
                var idd = $(this).attr('id');
                var maximo = $(this).attr('maxlength');
                $(this).textlimit('span[id='+idd+']', maximo);
            });
        });

        base=$("#url_base").val();
        $("#actualizarObjetivo").hide();
        $("em").hide();
        id_objetivo = $('#id_objetivo').val();

        $("#editarObjetivo").click(function () {
            $('*').removeAttr('disabled');//le quita el disable a todos los inputs
            $("#editarObjetivo").hide();//oculando el boton de edtar
            $("#actualizarObjetivo").show();//aparece el boton de guardar
            $("em").show();
        });

        $("#updateGuardarObjetivo").validate({
            submitHandler: function() {
                var descripcion=$("#descripcion").val();
                var justificacion=$("#justificacion").val();
                //var id_tipo_de_objetivo=$("#id_tipo_de_objetivo").val();
                var id_subtipo_de_objetivo=$("#id_subtipo_de_objetivo").val();
                var submit=2;
                $.ajax({
                    type: "POST",
                    url: base+"/objetivos",
                    data: "submit="+submit+"&id_proyecto="+id_proyecto+"&descripcion="+descripcion+"&justificacion="+justificacion + "&id_subtipo_de_objetivo="+id_subtipo_de_objetivo+"&id_objetivo="+id_objetivo,
                    success: function(msg){
                        alert("El objetivo se ha actualizado exitosamente.");
                        $('#subContent').html(msg).show('slow');
                    }
                });
            }
        });
    });
</script>
<form id="updateGuardarObjetivo">
    <input type="hidden" value="<?php echo $id_objetivo; ?>" name="id_objetivo" id="id_objetivo">
    <table class="info"align="center">
        <tfoot>
            <tr>
                <?php if ($permisoEditar == 1) {
                ?>
                    <td colspan="2" style="text-align:center; border: 0px">
                        <input type="button"  id="editarObjetivo" value="Editar">
                        <input id="actualizarObjetivo" type="submit" name="guardarUpdateObjetivo" class="guardarUpdateObjetivo" value="Guardar">
                    </td>
                <?php } ?>
            </tr>
        </tfoot>
        <thead>
            <tr>
                <th colspan="2">
                    <p class="title">Ver Objetivo</p>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="titulo"><?php echo form_label($objetivo_particular, $objetivo_particular); ?>
                    <?php
                    $obj = array('value' => $objetivo['descripcion'], 'disabled' => '');
                    $fobjetivo1 = array_merge($obj, $fobjetivo_particular);
                    ?>
                    <em>*</em>
                </td>
                <td><?php echo form_textarea($fobjetivo1); ?><span id="descripcion"></span></td>
            </tr>
            <tr>
                <td class="titulo"> <?php echo form_label($justificacion, $justificacion); ?>
                    <?php
                    $jus = array('value' => $objetivo['justificacion'], 'disabled' => '');
                    $fjustificacion1 = array_merge($jus, $fjustificacion);
                    ?>
                    <em>*</em></td>
                <td><?php echo form_textarea($fjustificacion1); ?><span id="justificacion"></span></td>
            </tr>
            <tr>
                <td class="titulo"><label for="SubTipo de objetivo"><?php echo "Sub Tipo de objetivo"; ?></label>
                    <?php $idsubtipodeobjetivo = $objetivo['id_subtipo_de_objetivo']; ?>
                    <em>*</em></td>
                <td> <select id="id_subtipo_de_objetivo" name="id_subtipo_de_objetivo" title="Por favor elige una opci&oacute;n" class="required" disabled>
                        <?php
                        foreach ($tipo_subtipoObjetivo_arr as $indice1 => $valor1) {
                            if ($idsubtipodeobjetivo == $indice1) {
                                echo "<option value=$indice1 selected>$valor1</option>'";
                            } else {
                                echo "<option value=$indice1>$valor1</option>'";
                            }
                        }
                        ?>
                    </select></td>
            </tr>
        </tbody>
    </table>
</form>

