<?php echo script_tag('js/responsable.js') ?>
<form id="formResponsable" name="formResponsable">
    <table class="info" align="center">

   <?php 
   foreach($infoResponsable as $indice){
   $id_responsable=$indice->id_responsable;
   $nombre=$indice->nombre;
   $cargo=$indice->cargo;
   $telefono=$indice->telefono;
   $email=$indice->email;
   $id_grado_academico=$indice->id_grado_academico;

   ?>
   <input type="hidden" name="responsableUpd" id="responsableUpd" value="<?php echo $id_responsable ?>">    

   <tr>
   <td class="titulo">Responsable</td>
   <td><em>*</em><input type="text" class="required" name="nombre" value="<?= $nombre ?>" size="30" disabled="disabled" /></td>
   </tr>
   <tr>
   <td class="titulo">Cargo</td>
   <td><em>*</em><input type="text" class="required" name="cargo" value="<?= $cargo ?>" size="30" disabled="disabled" /></td>
   </tr>
   <tr>
   <td class="titulo">Grado académico</td>
   <td><em>*</em>
   <select id="id_grado_academico" name="id_grado_academico"  disabled="disabled" class="required">
   <?php
   foreach($gradosAcademicos as $indice=>$valor){
     if($id_grado_academico==$indice){	
       echo '<option value="'.$indice.'" selected="selected">'.$valor.'</option>';					
     }
     else{
       echo '<option value="'.$indice.'">'.$valor.'</option>';
     }
   }
   ?>
   </select>
   </td>
   </tr>
   <tr>
   <td class="titulo">Teléfono</td>
   <td><em>*</em><input type="text" class="required" name="telefono" value="<?= $telefono ?>" size="30" disabled="disabled" /></td>
   </tr>
   <tr>
   <td class="titulo">Correo electrónico</td>
   <td><em>*</em><input type="text" class="email required" name="email" value="<?= $email ?>" size="30" disabled="disabled" /></td>
   </tr>

   <?php 
 }
   ?>
   <tr>
   <td colspan="2" style="text-align:center; border: 0px">
   <?php if($editar){?>
   <input type="button" id="editarResponsable" value="Editar" />
   <input type="submit" id="updateResponsable" value="Guardar"  />
   <?php }?>
   </td>
   </tr>
   </table>
   </form>
