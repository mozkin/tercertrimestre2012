<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 

/** 
 * menu_p 
 * Genera un parrafo con la clase current en el elemento seleccionado 
 * Modificado para jqueryui framework css
 */ 

if ( ! function_exists('menu_p')) 
{ 
    function menu_p($sel = 'responsable', $separator = '') 
   { 
        $CI =& get_instance(); 
        $items = $CI->config->item('navigation'); 
 
        $count = count($items); 
        $i = 0; 
        $menu = "\n".'<p class="bottom_nav">'; 
        foreach($items as $item) 
        { 
            $current = (in_array($sel, $item)) ? ' class="ui-state-active"' : ''; 
            $id = (!empty($item['id'])) ? ' id="'.$item['id'].'"' : ''; 
            $menu .= '<a'.$current.' href="'.$item['link'].'"'.$id.'>'.$item['title'].'</a>'; 
            $i++; 
            if($count != $i) 
            { 
                $menu .= ' '.$separator.' '; 
            } 
        } 
        $menu .= '</p>'."\n"; 
        return $menu; 
    } 
} 
 
 
/* End of file navigation_helper.php */ 
/* Location: ./system/helpers/navigation_helper.php */ 