<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 
/** 
 * menu_ul() 
 * Genera una lista desordenada con la clase current en el elemento seleccionado. 
 * Modificado para jqueryui framework css
 */ 
if ( ! function_exists('menu_ul')) 
{ 
    function menu_ul($sel = 'responsable') 
   { 
        $CI =& get_instance(); 
        $items = $CI->config->item('navigation'); 
         
        $menu = '<ul id="menu">'."\n"; 
        foreach($items as $item){ 
            $current = (in_array($sel, $item)) ? ' class="ui-state-active"' : ' class="ui-state-default"'; 
            $id = (!empty($item['id'])) ? ' id="'.$item['id'].'"' : ''; 
            $menu .= '<li'.$current.'><a href="'.$item['link'].'"'.$id.'>'.$item['title'].'</a></li>'."\n"; 
        } 
        $menu .= '</ul>'."\n"; 
        return $menu; 
    } 
} 
 
/* End of file navigation_helper.php */ 
/* Location: ./system/helpers/navigation_helper.php */ 