<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Responsable extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $this->load->model('Responsable_model');

        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_periodo = $this->session->userdata('id_periodo');
        $escuela = $this->session->userdata('escuela');
        $entidad = $this->session->userdata('entidad');
        $profen = $this->session->userdata('es_PROFEN');

        if ($this->session->userdata('logged_in')) {
            $data['escuela'] = $escuela;
            $data['entidad'] = $entidad;
            $data['es_PROFEN'] = $profen;
            $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
            $data['menu'] = menu_ul('responsable');

            //Nivel 6: Responsable
            $data['permisosResponsable'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 6);

            $data['gradosAcademicos'] = $this->Responsable_model->obtenerGradosAcademicos();
            $data['responsables'] = $this->Responsable_model->mostrarResponsable($profen, $id_escuela, $id_entidad, $id_periodo);

            $this->load->view("responsable/responsables", $data);
        } else {
            redirect("login");
        }
    }

    function responsable($id_responsable) {
        $this->load->model('Responsable_model');

        $logged_in = $this->session->userdata('logged_in');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_periodo = $this->session->userdata('id_periodo');
        $escuela = $this->session->userdata('escuela');
        $entidad = $this->session->userdata('entidad');
        $profen = $this->session->userdata('es_PROFEN');

        if ($logged_in) {
            $data['escuela'] = $escuela;
            $data['entidad'] = $entidad;
            $data['es_PROFEN'] = $profen;
            $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
            $data['menu'] = menu_ul('responsable');
            //Nivel 6: Responsable
            $data['permisosResponsable'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 6);

            $data['gradosAcademicos'] = $this->Responsable_model->obtenerGradosAcademicos();
            $data['infoResponsable'] = $this->Responsable_model->InfoResponsable($id_responsable);

            $this->load->view("responsable/mostrar", $data);
        } else {
            redirect("login");
        }
    }

    public function updateResponsable() {
        $id_nivel = 6;
        $this->load->model("Responsable_model");
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_periodo = $this->session->userdata('id_periodo');


        if ($logged_in) {
            $data['gradosAcademicos'] = $this->Responsable_model->obtenerGradosAcademicos();

            $dataModificados = $_POST;
            $this->Responsable_model->updateResponsable($dataModificados);

            $data['infoResponsable'] = $this->Responsable_model->mostrarResponsable($es_PROFEN, $id_escuela, $id_entidad, $id_periodo);
            $permisosResponsable = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, $id_nivel);
            $data['editar'] = $permisosResponsable['editar'];


            $this->load->view("verResponsable", $data);
        } else {
            redirect("login");
        }
    }

}

?>
