<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proyecto extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $data['log'] = "5";
        $data['log_texto'] = "";
        $this->load->view('login', $data);
        $this->output->enable_profiler(FALSE);
    }

    /**
     * Función que verifica si existe el email y contrasena,
     * y se puede entrar al sistema, de lo contrario regresa
     * a la pantalla de login
     * @ params email, contrasena
     */
    public function verificarLogin() {
        $this->load->model('Usuario_model');
        $user = $this->input->post('usuario');
        $pass = $this->input->post('contrasena');
        $login = $this->Login_model->checarLogin($user, $pass);
        if ($login == 1) {
            $infoUsuario = $this->Usuario_model->obtenerInfoUsuario($user);
            $id_usuario = $infoUsuario['id_usuario'];
            $es_PROFEN = $infoUsuario['es_PROFEN'];
            $id_tipo = $infoUsuario['id_tipo'];
            /* //Verificando el tipo de usuario */
            if ($es_PROFEN == 2) {
                //entidad
                $id_entidad = $id_tipo;
                $entidad = $this->Usuario_model->obtener_entidad($id_entidad);
                $id_escuela = "";
                $escuela = "";
            } elseif ($es_PROFEN == 1) {
                //escuela
                $id_escuela = $id_tipo;
                $infoEscuela = $this->Usuario_model->obtenerInfoEscuela($id_escuela);
                $id_entidad = $infoEscuela['id_entidad'];
                $escuela = $infoEscuela['escuela'];
                $entidad = $this->Usuario_model->obtener_entidadEsc($id_escuela);
            }
            //Permisos para el usuario logeado.
            //            $permisos_proyectos = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '1');
            //            $permisos_objetivos = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '2');
            //            $permisos_metas = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '3');
            //            $permisos_acciones = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '4');
            //            $permisos_rubros = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '5');
            //            $permisos_responsables = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '6');
            //            $permisos_reportes = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '7');
            $sesionProyecto = array(
                'user' => $user, 'id_usuario' => $id_usuario,
                'id_entidad' => $id_entidad, 'entidad' => $entidad,
                'id_escuela' => $id_escuela, 'escuela' => $escuela,
                'es_PROFEN' => $es_PROFEN, 'logged_in' => TRUE,
                'id_periodo' => 11, // El id del periodo en que se reporta.
                'id_sistema' => 11, // De acuerdo a la tabla de sistema es el 11
                'id_periodo_reprogramacion' => 8, // De acuerdo con la tabla periodo es el id 8.
                'id_anio' => 2 //Ejercicio bianual el 2012 es el año 2
            );
            $this->session->set_userdata($sesionProyecto);
            redirect("proyecto/responsable");
        } else {
            //cualquier otra opcion
            $data['log'] = $login;
            if ($login == '0') {
                $data['log_texto'] = "Contrase&ntilde;a Incorrecta";
            } elseif ($login == '2') {
                $data['log_texto'] = "El usuario no existe";
            } elseif ($login == '3') {
                $data['log_texto'] = "No puedes acceder al sistema";
            } else {
                $data['log_texto'] = "Error desconocido";
            }
            $this->load->view('login', $data);
        }
    }

    /**
     * Menú: responsable
     */
    function responsable() {
        $id_nivel = 6;
        //de acuerdo a la tabla "nivel"
        $this->load->model('Responsable_model');
        //$this->output->enable_profiler(TRUE);
        $logged_in = $this->session->userdata('logged_in');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_periodo = $this->session->userdata('id_periodo');
        $id_periodo_reprogramacion = $this->session->userdata('id_periodo_reprogramacion');
        $data['escuela'] = $this->session->userdata('escuela');
        $data['entidad'] = $this->session->userdata('entidad');
        $data['es_PROFEN'] = $this->session->userdata('es_PROFEN');
        if ($logged_in) {
            $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
            $data['menu'] = menu_ul('responsable');
            $data['gradosAcademicos'] = $this->Responsable_model->obtenerGradosAcademicos();
            $data['infoResponsable'] = $this->Responsable_model->mostrarResponsable($data['es_PROFEN'], $id_escuela, $id_entidad, $id_periodo_reprogramacion);
            $data['permisosResponsable'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, $id_nivel);
            $this->load->view("responsable", $data);
        } else {
            // echo "Ya no tienes sesión!";
            redirect("proyecto/index");
        }
    }

    public function updateResponsable() {
        $id_nivel = 6;
        $this->load->model("Responsable_model");
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_periodo = $this->session->userdata('id_periodo');
        if ($logged_in) {
            $data['gradosAcademicos'] = $this->Responsable_model->obtenerGradosAcademicos();
            $dataModificados = $_POST;
            $this->Responsable_model->updateResponsable($dataModificados);
            $data['infoResponsable'] = $this->Responsable_model->mostrarResponsable($es_PROFEN, $id_escuela, $id_entidad, $id_periodo);
            $permisosResponsable = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, $id_nivel);
            $data['editar'] = $permisosResponsable['editar'];
            $this->load->view("verResponsable", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    /**
     * Menú: proyectos
     */
    public function proyectos() {
        $this->load->model('Proyecto_model');
        $this->output->enable_profiler(FALSE);
        $logged_in = $this->session->userdata('logged_in');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $id_periodo_reprogramacion = $this->session->userdata('id_periodo_reprogramacion');
        $data['escuela'] = $this->session->userdata('escuela');
        $data['entidad'] = $this->session->userdata('entidad');
        $data['es_PROFEN'] = $es_PROFEN;
        if ($logged_in) {
            $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
            $data['proyectos'] = $this->Proyecto_model->obtenerProyectos($es_PROFEN, $id_entidad, $id_escuela, $id_periodo_reprogramacion);
            $data['menu'] = menu_ul('proyectos');
            //Nivel 1: Proyectos
            $data['permisosProyecto'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 1);
            //Nivel 2 : Objetivos
            $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
            $this->load->view("proyectos", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    function verProyecto() {
        $this->load->model('Proyecto_model');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        //$id_entidad=$this->session->userdata('id_entidad');
        $data = $this->Proyecto_model->formularioCapturaProyecto();
        //Nivel 1: Proyectos
        $data['permisosProyecto'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 1);
        if ($logged_in) {
            $idProyecto = $_POST['proyecto'];
            // $data['editar'] = $permisoProyecto['editar'];
            $data['id_proyecto'] = $idProyecto;
            $data['infoProyecto'] = $this->Proyecto_model->ver_proyecto($idProyecto);
            $this->load->view("verProyecto", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    public function updateProyecto() {
        $id_nivel = 1;
        $this->load->model("Proyecto_model");
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_periodo = $this->session->userdata('id_periodo');
        $data = $this->Proyecto_model->formularioCapturaProyecto();
        //Nivel 1: Proyectos
        $data['permisosProyecto'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 1);
        //Nivel 2 : Objetivos
        $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
        //$data['menu'] = menu_ul('proyectos');
        if ($logged_in) {
            $dataModificados = $_POST;
            $this->Proyecto_model->updateProyecto($dataModificados);
            $id_proyecto = $dataModificados['id_proyecto'];
            $data['id_proyecto'] = $id_proyecto;
            $data['infoProyecto'] = $this->Proyecto_model->infoProyecto($id_proyecto);
            //$data['proyectos'] = $this->Proyecto_model->obtenerProyectos($es_PROFEN, $id_entidad, $id_escuela, $id_periodo);
            $this->load->view("verProyecto", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    //Eliminado lógico del proyecto
    function eliminarProyecto() {
        $this->load->model("Proyecto_model");
        $logged_in = $this->session->userdata('logged_in');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $data['escuela'] = $this->session->userdata('escuela');
        $data['entidad'] = $this->session->userdata('entidad');
        $data['es_PROFEN'] = $es_PROFEN;
        if ($logged_in) {
            $id_proyecto = $_POST['id_proyecto'];
            $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
            $this->Proyecto_model->eliminado_logico_proyecto($id_proyecto);
            $data['proyectos'] = $this->Proyecto_model->obtenerProyectos($es_PROFEN, $id_entidad, $id_escuela, $id_periodo);
            $data['menu'] = menu_ul('proyectos');
            //Nivel 1: Proyectos
            $data['permisosProyecto'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 1);
            //Nivel 2 : Objetivos
            $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
            $this->load->view("proyectos", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    /**
     * Proyectos -> Objetivos
     */
    function objetivos() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->library('session');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $data = $this->Objetivos_model->formularioCapturaObjetivo();
        $id_proyecto = $_POST['id_proyecto'];
        $data['numObjetivos'] = $this->Objetivos_model->numObjetivos($id_proyecto);
        $submit = $_POST['submit'];
        $data['id_proyecto'] = $id_proyecto;
        //Nivel 2: Objetivos
        $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        if ($submit == 1) {
            //si hicieron onsubmit en guardar proyecto
            $data['submit'] = "si";
            $dataInsert['id_proyecto'] = $id_proyecto;
            $dataInsert['descripcion'] = $_POST['descripcion'];
            $dataInsert['justificacion'] = $_POST['justificacion'];
            $dataInsert['id_subtipo_de_objetivo'] = $_POST['id_subtipo_de_objetivo'];
            $this->Objetivos_model->insert_objetivo($dataInsert);
            $data['numObjetivos'] = $this->Objetivos_model->numObjetivos($id_proyecto);
            $data['objetivos'] = $this->Objetivos_model->obtenerObjetivos($id_proyecto);
            $data['tipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoObjetivosSel();
            //catalogo tipo_de_objetivos
            $data['subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerSubTipoObjetivosSel();
            //catalogo subtipo_de_objetivos
            $data['tipo_subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoSubtipo();
            //catalogo tipo-subtipo
            $this->load->view('objetivos', $data);
        } elseif ($submit == 2) {
            //si hicieron onsubmit en update proyecto
            $data['submit'] = "si";
            $dataInsert['id_proyecto'] = $id_proyecto;
            $dataInsert['id_objetivo'] = $_POST['id_objetivo'];
            $dataInsert['descripcion'] = $_POST['descripcion'];
            $dataInsert['justificacion'] = $_POST['justificacion'];
            $dataInsert['id_subtipo_de_objetivo'] = $_POST['id_subtipo_de_objetivo'];
            $dataInsert['tipo_subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoSubtipo();
            //catalogo tipo-subtipo
            $this->Objetivos_model->update_objetivo($dataInsert);
            $data['objetivos'] = $this->Objetivos_model->obtenerObjetivos($id_proyecto);
            $data['tipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoObjetivosSel();
            //catalogo tipo_de_objetivos
            $data['subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerSubTipoObjetivosSel();
            //catalogo subtipo_de_objetivos
            $this->load->view('objetivos', $data);
        } else {
            $data['submit'] = "no";
            $data['objetivos'] = $this->Objetivos_model->obtenerObjetivos($id_proyecto);
            $data['tipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoObjetivosSel();
            //catalogo tipo_de_objetivos
            $data['subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerSubTipoObjetivosSel();
            //catalogo subtipo_de_objetivos
            $data['tipo_subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoSubtipo();
            //catalogo tipo-subtipo
            $this->load->view('objetivos', $data);
        }
    }

    function verObjetivos() {
        $this->load->model('Objetivos_model');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        //Nivel 2 : Objetivos
        $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        if ($logged_in) {
            $idProyecto = $_POST['proyecto'];
            $data['id_proyecto'] = $idProyecto;
            $data['numObjetivos'] = $this->Objetivos_model->numObjetivos($idProyecto);
            $data['objetivos'] = $this->Objetivos_model->obtenerObjetivos($idProyecto);
            $this->load->view("objetivos", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    function ver_objetivo() {
        $id_nivel = 2;
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Objetivos_model');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        if ($logged_in) {
            $permisos = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, $id_nivel);
            $data = $this->Objetivos_model->formularioCapturaObjetivo();
            $data['permisoAgregar'] = $permisos['agregar'];
            $data['permisoEditar'] = $permisos['editar'];
            $data['permisoEliminar'] = $permisos['borrar'];
            $data['tipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoObjetivosSel();
            //catalogo tipo_de_objetivos
            $data['subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerSubTipoObjetivosSel();
            //catalogo subtipo_de_objetivos
            $data['tipo_subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoSubtipo();
            //catalogo tipo-subtipo
            $id_objetivo = $_POST['objetivo'];
            $data['id_objetivo'] = $id_objetivo;
            $data['objetivo'] = $this->Objetivos_model->InfoObjetivo($id_objetivo);
            $this->load->view("ver_objetivo", $data);
        } else {
            redirect("proyecto/index");
        }
    }

    function eliminarObjetivo() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->library('session');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $data = $this->Objetivos_model->formularioCapturaObjetivo();
        $id_proyecto = $_POST['id_proyecto'];
        $id_objetivo = $_POST['id_objetivo'];
        $data['id_proyecto'] = $id_proyecto;
        $data['numObjetivos'] = $this->Objetivos_model->numObjetivos($id_proyecto);
        //Nivel 2: Objetivos
        $data['permisosObjetivo'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 2);
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        $this->Objetivos_model->eliminado_logico_objetivo($id_objetivo);
        $data['submit'] = "no";
        $data['objetivos'] = $this->Objetivos_model->obtenerObjetivos($id_proyecto);
        $data['tipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoObjetivosSel();
        //catalogo tipo_de_objetivos
        $data['subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerSubTipoObjetivosSel();
        //catalogo subtipo_de_objetivos
        $data['tipo_subtipoObjetivo_arr'] = $this->Objetivos_model->obtenerTipoSubtipo();
        //catalogo tipo-subtipo
        $this->load->view('objetivos', $data);
    }

    /**
     * Proyectos -> Objetivos -> Metas
     */
    function metas() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->model('Metas_model');
        $this->load->library('session');
        $submit = $_POST['submit'];
        $id_objetivo = $_POST['id_objetivo'];
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->Metas_model->formularioCapturaMetas();
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        //Nivel 4 : Acciones
        $data['permisosAcciones'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 4);
        //echo "num metas ".$data['numMetas'];
        if ($submit == 1) {
            //si hicieron onsubmit en guardar proyecto
            $data['submit'] = "si";
            //$id_objetivo=$_POST['id_objetivo'];
            $dataInsert['id_objetivo'] = $id_objetivo;
            $dataInsert['descripcion'] = $_POST['descripcion'];
            $dataInsert['id_unidad_de_medida'] = $_POST['unidad_de_medida'];
            $dataInsert['beneficiarios_hombres'] = $_POST['beneficiarios_hombres'];
            $dataInsert['beneficiarios_mujeres'] = $_POST['beneficiarios_mujeres'];
            $dataInsert['id_tipo_de_beneficiario'] = $_POST['id_tipo_de_beneficiario'];
            $this->Metas_model->insert_meta($dataInsert);
            $data['numMetas'] = $this->Metas_model->numMetas($id_objetivo);
            //echo "num- metas ".$data['numMetas'];
            $data['id_objetivo'] = $id_objetivo;
            $data['mostrarMetas'] = $this->Metas_model->mostrarMetas($id_objetivo);
            $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
            //catalogo tipo de beneficiario
            //$data['unidadMedida']=$this->metas_model->obtenerUnidadMedida();//catalogo unidad de medida
            $this->load->view('metas', $data);
        } elseif ($submit == 2) {
            $data['submit'] = "si";
            $dataInsert['id_meta'] = $_POST['id_meta'];
            $dataInsert['descripcion'] = $_POST['descripcion'];
            $dataInsert['unidad_medida'] = $_POST['unidad_medida'];
            $dataInsert['beneficiarios_hombres'] = $_POST['beneficiarios_hombres'];
            $dataInsert['beneficiarios_mujeres'] = $_POST['beneficiarios_mujeres'];
            $dataInsert['id_tipo_de_beneficiario'] = $_POST['id_tipo_de_beneficiario'];
            $this->Metas_model->update_meta($dataInsert);
            $data['id_objetivo'] = $id_objetivo;
            $data['mostrarMetas'] = $this->Metas_model->mostrarMetas($id_objetivo);
            $data['numMetas'] = $this->Metas_model->numMetas($id_objetivo);
            $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
            //catalogo tipo de beneficiario
            $this->load->view('metas', $data);
        } else {
            $data['submit'] = "no";
            //            $data = $this->Metas_model->formularioCapturaMetas();
            $data['numMetas'] = $this->Metas_model->numMetas($id_objetivo);
            $data['id_objetivo'] = $id_objetivo;
            $data['mostrarMetas'] = $this->Metas_model->mostrarMetas($id_objetivo);
            $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
            //catalogo tipo de beneficiario
            $this->load->view('metas', $data);
        }
    }

    function verMeta() {
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Metas_model');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $data = $this->Metas_model->formularioCapturaMetas();
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
        //catalogo tipo de beneficiario
        //$data['unidadMedida']=$this->metas_model->obtenerUnidadMedida();//catalogo unidad de medida
        $id_meta = $_POST['meta'];
        //echo $id_periodo;
        $data['id_meta'] = $id_meta;
        $data['id_periodo'] = $id_periodo;
        $data['meta'] = $this->Metas_model->InfoMeta($id_meta);
        $this->load->view('ver_meta', $data);
    }

    function verInformeMeta() {
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Metas_model');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $data = $this->Metas_model->formularioCapturaMetas();
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
        //catalogo tipo de beneficiario
        //$data['unidadMedida']=$this->metas_model->obtenerUnidadMedida();//catalogo unidad de medida
        $id_meta = $_POST['meta'];
        //echo $id_periodo;
        $data['id_meta'] = $id_meta;
        $data['id_periodo'] = $id_periodo;
        $data['meta'] = $this->Metas_model->InfoMeta($id_meta);
        $this->load->view('ver_meta_prog', $data);
    }

    function eliminarMeta() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->model('Metas_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->Metas_model->formularioCapturaMetas();
        $id_objetivo = $_POST['id_objetivo'];
        $id_meta = $_POST['id_meta'];
        $data['id_objetivo'] = $id_objetivo;
        //Nivel 3 : Metas
        $data['permisosMetas'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 3);
        //Nivel 4 : Acciones
        $data['permisosAcciones'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 4);
        $this->Metas_model->eliminado_logico_meta($id_meta);
        $data['numMetas'] = $this->Metas_model->numMetas($id_objetivo);
        $data['mostrarMetas'] = $this->Metas_model->mostrarMetas($id_objetivo);
        $data['tipoBeneficiario'] = $this->Metas_model->obtenerTipoBeneficiario();
        //catalogo tipo de beneficiario
        $this->load->view('metas', $data);
    }

    /**
     * Proyectos -> Objetivos -> Metas -> Acciones
     */
    function acciones() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->model('Metas_model');
        $this->load->model('Acciones_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->Acciones_model->formularioCapturaAcciones();
        $submit = $_POST['submit'];
        $id_meta = $_POST['id_meta'];
        //Nivel 4 : Acciones
        $data['permisosAcciones'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 4);
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        if ($submit == 1) {
            //si hicieron onsubmit en guardar accion
            $data['submit'] = "si";
            $dataInsert['id_meta'] = $id_meta;
            $dataInsert['descripcion1'] = $_POST['descripcion1'];
            $dataInsert['descripcion2'] = $_POST['descripcion2'];
            $dataInsert['accion'] = $_POST['accion'];
            $this->Acciones_model->insert_accion($dataInsert);
            $data['numAcciones'] = $this->Acciones_model->numAcciones($id_meta);
            $data['id_meta'] = $id_meta;
            $data['mostrarAcciones'] = $this->Acciones_model->mostrarAcciones($id_meta);
            $this->load->view('acciones', $data);
        } elseif ($submit == 2) {
            $dataInsert['id_meta'] = $id_meta;
            $dataInsert['descripcion1'] = $_POST['descripcion1'];
            $dataInsert['descripcion2'] = $_POST['descripcion2'];
            $dataInsert['accion'] = $_POST['accion'];
            $this->Acciones_model->update_accion($dataInsert);
            $data['numAcciones'] = $this->Acciones_model->numAcciones($id_meta);
            $data['id_meta'] = $id_meta;
            $data['mostrarAcciones'] = $this->Acciones_model->mostrarAcciones($id_meta);
            $this->load->view('acciones', $data);
        } else {
            $data['submit'] = "no";
            $data['numAcciones'] = $this->Acciones_model->numAcciones($id_meta);
            $data['id_meta'] = $id_meta;
            $data['mostrarAcciones'] = $this->Acciones_model->mostrarAcciones($id_meta);
            $this->load->view('acciones', $data);
        }
    }

    function verAccion() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Acciones_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->Acciones_model->formularioCapturaAcciones();
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        $id_accion = $_POST['accion'];
        $data['id_accion'] = $id_accion;
        $data['accion1'] = $this->Acciones_model->InfoAccion($id_accion);
        $data['id_periodo'] = $this->session->userdata('id_periodo');
        $this->load->view('ver_accion', $data);
    }

    function eliminarAccion() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Objetivos_model');
        $this->load->model('Metas_model');
        $this->load->model('Acciones_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->Acciones_model->formularioCapturaAcciones();
        $id_meta = $_POST['id_meta'];
        $id_accion = $_POST['id_accion'];
        $data['id_meta'] = $id_meta;
        //Nivel 4 : Acciones
        $data['permisosAcciones'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 4);
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        $this->Acciones_model->eliminado_logico_accion($id_accion);
        $data['numAcciones'] = $this->Acciones_model->numAcciones($id_meta);
        $data['mostrarAcciones'] = $this->Acciones_model->mostrarAcciones($id_meta);
        $this->load->view('acciones', $data);
    }

    /**
     * Proyectos -> Objetivos -> Metas -> Acciones -> Rubros
     */

    /**
     * $edoRubro, se refiere al estado en que va el guardado del rubrro: parte (1) o parte (2), de inicio esta en 0
     */
    function rubros() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('Proyecto_model');
        $this->load->model('Rubros_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $id_anio = $this->session->userdata('id_anio');
        $data = $this->Rubros_model->formularioCapturaRubros();
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        $submit = $_POST['submit'];
        $id_accion = $_POST['id_accion'];
        $proy = $this->Proyecto_model->obtenerIdProyecto($id_accion);
        $id_proyecto = $proy['id_proyecto'];
        if ($submit == 1) {
            $dataInsert['id_accion'] = $id_accion;
            $dataInsert['cantidad'] = $_POST['cantidad'];
            $dataInsert['precio_unitario'] = $_POST['precio_unitario'];
            $dataInsert['id_clasificador'] = $_POST['id_clasificador'];
            $dataInsert['id_periodo_aplicacion'] = $_POST['id_periodo_aplicacion'];
            $dataInsert['anyo'] = $_POST['anyo'];
            $id_rubro = $this->Rubros_model->insert_rubro($dataInsert);
            $data['id_rubro'] = $id_rubro;
            $data['id_accion'] = $id_accion;
            $data['clasificador'] = $this->Rubros_model->obtenerClasificador();
            $data['anyos'] = $this->Proyecto_model->obtenerAnyos();
            //$data['mostrarRubros']=$this->rubros_model->mostrarRubros($id_accion);
            $data['mostrarRubros1'] = $this->Rubros_model->mostrarRubrosAnio($id_accion, $id_anio, 8);
            //$data['mostrarRubros2'] = $this->Rubros_model->mostrarRubros2($id_accion);
            //$data['mostrarRubros2']=$this->rubros_model->mostrarRubros2($id_accion);
            //$data['presupuestoTotal'] = $this->Rubros_model->presupuestoTotal($id_proyecto, $id_periodo);
            $this->load->view('rubros', $data);
        } elseif ($submit == 2) {
            $dataInsert['id_accion'] = $id_accion;
            $dataInsert['cantidad'] = $_POST['cantidad'];
            $dataInsert['id_clasificador'] = $_POST['id_clasificador'];
            $dataInsert['precio_unitario'] = $_POST['precio_unitario'];
            $dataInsert['id_rubro'] = $_POST['id_rubro'];
            $dataInsert['id_periodo_aplicacion'] = $_POST['id_periodo_aplicacion'];
            $dataInsert['anyo'] = $_POST['anyo'];
            $this->Rubros_model->update_rubro($dataInsert);
            $data['anyos'] = $this->Proyecto_model->obtenerAnyos();
            $data['id_accion'] = $id_accion;
            $data['clasificador'] = $this->Rubros_model->obtenerClasificador();
            //$data['mostrarRubros']=$this->rubros_model->mostrarRubros($id_accion);
            $data['mostrarRubros1'] = $this->Rubros_model->mostrarRubrosAnio($id_accion, $id_anio, 8);
            //$data['mostrarRubros2'] = $this->Rubros_model->mostrarRubros2($id_accion);
            //$data['presupuestoTotal'] = $this->Rubros_model->presupuestoTotal($id_proyecto, $id_periodo);
            $this->load->view('rubros', $data);
        } else {
            $data['submit'] = "no";
            $data['id_accion'] = $id_accion;
            $data['clasificador'] = $this->Rubros_model->obtenerClasificador();
            //$data['mostrarRubros']=$this->rubros_model->mostrarRubros($id_accion);
            $data['mostrarRubros1'] = $this->Rubros_model->mostrarRubrosAnio($id_accion, $id_anio, 8);
            //$data['mostrarRubros2'] = $this->Rubros_model->mostrarRubros2($id_accion);
            $data['anyos'] = $this->Proyecto_model->obtenerAnyos();
            $data['edoRubro'] = 0;
            //$data['presupuestoTotal'] = $this->Rubros_model->presupuestoTotal($id_proyecto, $id_periodo);
            $this->load->view('rubros', $data);
        }
    }

    function verRubro() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('proyecto_model');
        $this->load->model('rubros_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $data = $this->rubros_model->formularioCapturaRubros();
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        $data['anyos'] = $this->proyecto_model->obtenerAnyos();
        $data['clasificador'] = $this->rubros_model->obtenerClasificador();
        $id_rubro = $_POST['rubro'];
        $data['id_rubro'] = $id_rubro;
        $data['rubro'] = $this->rubros_model->InfoRubro($id_rubro);
        $this->load->view('ver_rubro', $data);
    }

    function eliminarRubro() {
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->model('proyecto_model');
        $this->load->model('rubros_model');
        $this->load->library('session');
        $id_usuario = $this->session->userdata('id_usuario');
        $logged_in = $this->session->userdata('logged_in');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo = $this->session->userdata('id_periodo');
        $data = $this->rubros_model->formularioCapturaRubros();
        //Nivel 5 : Rubros
        $data['permisosRubros'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 5);
        $id_accion = $_POST['id_accion'];
        $id_rubro = $_POST['id_rubro'];
        $data['id_accion'] = $id_accion;
        $proy = $this->proyecto_model->obtenerIdProyecto($id_accion);
        $id_proyecto = $proy['id_proyecto'];
        $this->rubros_model->eliminado_logico_rubro($id_rubro);
        $data['clasificador'] = $this->rubros_model->obtenerClasificador();
        $data['mostrarRubros1'] = $this->rubros_model->mostrarRubros1($id_accion);
        $data['mostrarRubros2'] = $this->rubros_model->mostrarRubros2($id_accion);
        $data['anyos'] = $this->proyecto_model->obtenerAnyos();
        //$data['presupuestoTotal'] = $this->rubros_model->presupuestoTotal($id_proyecto, $id_periodo);
        $this->load->view('rubros', $data);
    }

    /**
     * Menú: reportes
     */
    public function reportes() {
        //$this->output->enable_profiler(TRUE);
        $this->load->model('Proyecto_model');
        $this->load->model('Responsable_model');
        $this->load->model('Informetrimestral_model');
        $this->load->model('escuela_model');
        $this->load->model('estado_model');
        $this->load->library('session');
        $logged_in = $this->session->userdata('logged_in');
        $id_usuario = $this->session->userdata('id_usuario');
        $id_sistema = $this->session->userdata('id_sistema');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_escuela = $this->session->userdata('id_escuela');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_periodo_reprogramacion = $this->session->userdata('id_periodo_reprogramacion');
        $id_periodo = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');
        $data['escuela'] = $this->session->userdata('escuela');
        $data['entidad'] = $this->session->userdata('entidad');
        $data['nombrePeriodo'] = $this->Proyecto_model->getPeriodo($id_periodo);
        $data['es_PROFEN'] = $es_PROFEN;
        /** Nivel 7 : Reportes* */
        $data['permisosReportes'] = $this->Login_model->obtenerPermisos($id_sistema, $id_usuario, 7);
        if ($logged_in) {
            if (!isset($_POST['submit'])) {
                $submit = 0;
            } else {
                $submit = $_POST['submit'];
            }
            if ($submit == 1) {
                $id_proyecto = $_POST['id_proyecto'];
                if ($es_PROFEN == 1) {
                    // $proyecto = $this->Proyecto_model->InfoProyectoProfen($id_proyecto);
                    $proyecto = $this->Proyecto_model->InfoProyectoProfen($id_proyecto);
                    $id_responsable = $proyecto['id_responsable'];
                    $responsable = $this->Responsable_model->InfoResponsable($id_responsable);
                    $data = array_merge($proyecto, $responsable);
                    $data['nombreUsuario'] = $this->escuela_model->getNombreEscuela($id_escuela);
                    $data['id_periodo'] = $this->session->userdata('id_periodo');
                    $data['periodo'] = $this->Proyecto_model->getPeriodo(9);
                    $data['id_anio'] = $anio_sistema_id;
                    //print_r($data);
                    $archivo = $this->Informetrimestral_model->reporteProfen($data);
                } elseif ($es_PROFEN == 2) {
                    $proyecto = $this->Proyecto_model->InfoProyectoProgen($id_proyecto);
                    $id_responsable = $proyecto['id_responsable'];
                    $responsable = $this->Responsable_model->InfoResponsable($id_responsable);
                    $data = array_merge($proyecto, $responsable);
                    $data['nombreUsuario'] = $this->estado_model->getNombreEntidad($id_entidad, 0);
                    $data['id_periodo'] = $this->session->userdata('id_periodo');
                    $data['periodo'] = $this->Proyecto_model->getPeriodo(9);
                    $data['id_anio'] = $anio_sistema_id;
                    $nombreArchivo = $this->Informetrimestral_model->reporteProgen($data);
                    //$archivo = $this->Reporte_model->reporte_progen($data);
                    $archivo = $nombreArchivo . '_' . $proyecto['id_tipo_proyecto'];
                }
                $this->Proyecto_model->proyectoCompleto($id_proyecto);
                $data['base_uri'] = base_url();
                $data['archivo'] = $archivo . '.pdf';
                $this->load->view('verUrlPdf', $data);
            } elseif ($submit == 0) {
                $data['sistema'] = $this->Sistema_model->obtenerSistema($id_sistema);
                $data['proyectos'] = $this->Proyecto_model->obtenerProyectos($es_PROFEN, $id_entidad, $id_escuela, $id_periodo_reprogramacion);
                $data['menu'] = menu_ul('reportes');
                $this->load->view('reportes', $data);
            }
        } else {
            redirect("proyecto/index");
        }
    }

    function probando() {
        $this->load->library('Pdf');
        $this->load->model('reporte_model');
        //$data['dah'] = "";
        //$archivo = $this->reporte_model->reporte_progen($data);
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Reporte_model');
        $this->load->model('Proyecto_model');
        $this->load->model('Responsable_model');
        $es_PROFEN = $this->session->userdata('es_PROFEN');
        $id_entidad = $this->session->userdata('id_entidad');
        $id_escuela = $this->session->userdata('id_escuela');
    }

    function setAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');
        $idMeta = $_POST['id_meta'];
        $idPeriodo = $_POST['id_periodo'];
        $benMujeres = $_POST['ben_mujeres'];
        $benHombres = $_POST['ben_hombres'];
        $anio_sistema_id = $this->session->userdata('id_anio');
        $metaAvance = $this->metas_model->getAvanceProgramatico($idMeta, $idPeriodo);
        if (is_array($metaAvance)) {
            $this->metas_model->updateAvanceProgramatico($metaAvance['idMetaAvance'], $benHombres, $benMujeres);
        } else {
            $this->metas_model->insertAvanceProgramatico($idMeta, $idPeriodo, $benHombres, $benMujeres);
        }
        $idTrimestre = $this->session->userdata('id_periodo');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $metaAvance = $this->metas_model->getAvanceProgramatico($idMeta, $periodo, $anio_sistema_id);
            $trimestre[$metaAvance['idPeriodo']]['periodo'] = $this->metas_model->getPeriodo($periodo);
            $trimestre[$metaAvance['idPeriodo']]['beneficiarios_hombres'] = $metaAvance['hombres'];
            $trimestre[$metaAvance['idPeriodo']]['beneficiarios_mujeres'] = $metaAvance['mujeres'];
        }
        $datos['avance_trimestre'] = $trimestre;
        $this->load->view('verDatosAvance', $datos);
    }

    function setInformeAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');

        $idMeta = $_POST['id_meta'];
        $idPeriodo = $_POST['id_periodo'];
        $medida_alcanzada = $_POST['medida_alcanzada'];
        $porcentaje_alcanzado = $_POST['porcentaje_alcanzado'];
        $justificacion = $_POST['justificacion'];

        $anio_sistema_id = $this->session->userdata('id_anio');
        $metaAvance = $this->metas_model->getInformeAvanceProgramatico($idMeta, $idPeriodo);
        if (is_array($metaAvance)) {
            $this->metas_model->updateInformeAvanceProgramatico($metaAvance['idMetaJustificacion'], $medida_alcanzada, $porcentaje_alcanzado, $justificacion);
        } else {
            $this->metas_model->insertInformeAvanceProgramatico($idMeta, $idPeriodo, $medida_alcanzada, $porcentaje_alcanzado, $justificacion);
        }
        $idTrimestre = $this->session->userdata('id_periodo');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $metaAvance = $this->metas_model->getInformeAvanceProgramatico($idMeta, $periodo, $anio_sistema_id);
            $trimestre[$metaAvance['idPeriodo']]['periodo'] = $this->metas_model->getPeriodo($periodo);
            $trimestre[$metaAvance['idPeriodo']]['medida_alcanzada'] = $metaAvance['medida_alcanzada'];
            $trimestre[$metaAvance['idPeriodo']]['porcentaje_alcanzado'] = $metaAvance['porcentaje_alcanzado'];
            $trimestre[$metaAvance['idPeriodo']]['justificacion'] = $metaAvance['justificacion'];
        }
        $datos['avance_informe_trimestre'] = $trimestre;
        $this->load->view('verDatosInformeAvance', $datos);
    }

    function getAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');
        $this->load->helper('form');
        $idMeta = $_POST['id_meta'];
        $idTrimestre = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $metaAvance = $this->metas_model->getAvanceProgramatico($idMeta, $periodo);
            $trimestre[$metaAvance['idPeriodo']]['periodo'] = $this->metas_model->getPeriodo($periodo);
            $trimestre[$metaAvance['idPeriodo']]['beneficiarios_hombres'] = $metaAvance['hombres'];
            $trimestre[$metaAvance['idPeriodo']]['beneficiarios_mujeres'] = $metaAvance['mujeres'];
        }
        $datos['avance_trimestre'] = $trimestre;
        $this->load->view('verDatosAvance', $datos);
    }

    function showFormAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');
        $this->load->helper('form');
        $idMeta = $_POST['id_meta'];
        $idTrimestre = $this->session->userdata('id_periodo');
        //$anio_sistema_id = $this->session->userdata('id_anio');
        $metaAvance = $this->metas_model->getAvanceProgramatico($idMeta, $idTrimestre);
        if (!is_array($metaAvance)) {
            $datos['beneficiarios_hombres'] = 0;
            $datos['beneficiarios_mujeres'] = 0;
        } else {
            $datos['beneficiarios_hombres'] = $metaAvance['hombres'];
            $datos['beneficiarios_mujeres'] = $metaAvance['mujeres'];
        }
        $this->load->view('ver_avance_programatico', $datos);
    }

    function getInformeAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');
        $this->load->helper('form');
        $idMeta = $_POST['id_meta'];
        $idTrimestre = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');

        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $metaAvance = $this->metas_model->getInformeAvanceProgramatico($idMeta, $periodo);
            $trimestre[$metaAvance['idPeriodo']]['periodo'] = $this->metas_model->getPeriodo($periodo);
            $trimestre[$metaAvance['idPeriodo']]['medida_alcanzada'] = $metaAvance['medida_alcanzada'];
            $trimestre[$metaAvance['idPeriodo']]['porcentaje_alcanzado'] = $metaAvance['porcentaje_alcanzado'];
            $trimestre[$metaAvance['idPeriodo']]['justificacion'] = $metaAvance['justificacion'];
        }
        $datos['avance_informe_trimestre'] = $trimestre;
        $this->load->view('verDatosInformeAvance', $datos);
    }

    function showFormInformeAvanceProgramatico() {
        $this->load->model('metas_model');
        $this->load->library('session');
        $this->load->helper('form');
        $idMeta = $_POST['id_meta'];
        $idTrimestre = $this->session->userdata('id_periodo');
        //$anio_sistema_id = $this->session->userdata('id_anio');
        $metaAvance = $this->metas_model->getInformeAvanceProgramatico($idMeta, $idTrimestre);
        if (!is_array($metaAvance)) {
            $datos['medida_alcanzada'] = '';
            $datos['porcentaje_alcanzado'] = '';
            $datos['justificacion'] = '';
        } else {
            $datos['medida_alcanzada'] = $metaAvance['medida_alcanzada'];
            $datos['porcentaje_alcanzado'] = $metaAvance['porcentaje_alcanzado'];
            $datos['justificacion'] = $metaAvance['justificacion'];
        }
        $this->load->view('ver_informe_avance_programatico', $datos);
    }

    function showFormAlcanceAccion() {
        $this->load->model('acciones_model');
        $this->load->helper('form');
        $this->load->library('session');
        $idAccion = $_POST['id_accion'];
        $idTrimestre = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');
        $accionAlcance = $this->acciones_model->getAccionAlcance($idAccion, $idTrimestre);
        $datos['id_accion'] = $idAccion;
        if (!is_array($accionAlcance)) {
            $datos['especificaciones'] = "";
            $datos['breve_descripcion'] = "";
        } else {
            $datos['especificaciones'] = $accionAlcance['especificaciones'];
            $datos['breve_descripcion'] = $accionAlcance['especificaciones'];
        }
        $this->load->view('ver_alcance_accion', $datos);
    }

    function getAlcanceAccion() {
        $this->load->model('acciones_model');
        $this->load->library('session');
        $this->load->helper('form');
        $idAccion = $_POST['id_accion'];
        $idTrimestre = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $accionAlcance = $this->acciones_model->getAccionAlcance($idAccion, $periodo);
            $trimestre[$accionAlcance['idPeriodo']]['periodo'] = $this->acciones_model->getPeriodo($periodo);
            $trimestre[$accionAlcance['idPeriodo']]['especificaciones'] = $accionAlcance['especificaciones'];
            $trimestre[$accionAlcance['idPeriodo']]['breveDescripcion'] = $accionAlcance['breveDescripcion'];
        }
        $datos['alcance_trimestre'] = $trimestre;
        $this->load->view('verDatosAlcance', $datos);
    }

    function setAlcanceAccion() {
        $this->load->model('acciones_model');
        $this->load->library('session');
        $anio_sistema_id = $this->session->userdata('id_anio');
        $idAccion = $_POST['id_accion'];
        $idPeriodo = $_POST['id_periodo'];
        $especificaciones = $_POST['especificaciones'];
        $breveDescripcion = $_POST['breve_descripcion'];
        $accionAlcance = $this->acciones_model->getAccionAlcance($idAccion, $idPeriodo);
        if (is_array($accionAlcance)) {
            $this->acciones_model->updateAccionAlcance($accionAlcance['idAccionAlcance'], $especificaciones, $breveDescripcion);
        } else {
            $this->acciones_model->insertAccionAlcance($idAccion, $idPeriodo, $especificaciones, $breveDescripcion);
        }
        $idTrimestre = $this->session->userdata('id_periodo');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $accionAlcance = $this->acciones_model->getAccionAlcance($idAccion, $periodo);
            $trimestre[$accionAlcance['idPeriodo']]['periodo'] = $this->acciones_model->getPeriodo($periodo);
            $trimestre[$accionAlcance['idPeriodo']]['especificaciones'] = $accionAlcance['especificaciones'];
            $trimestre[$accionAlcance['idPeriodo']]['breveDescripcion'] = $accionAlcance['breveDescripcion'];
        }
        $datos['alcance_trimestre'] = $trimestre;
        $this->load->view('verDatosAlcance', $datos);
    }

    function agregarPeriodo() {
        //$data = $this->loadIntro();
        $this->load->model('rubros_model');
        $this->load->library('session');
        $anio_sistema_id = $this->session->userdata('id_anio');
        $data = $this->rubros_model->formularioCapturaRubros();
        $id_rubro = $_POST['id_rubro'];
        $id_accion = $_POST['id_accion'];
        $data['id_rubro'] = $id_rubro;
        $data['id_accion'] = $id_accion;
        $id_periodo = $this->session->userdata('id_periodo');
        ;
        $reprogramacion = $this->rubros_model->InfoRubroPeriodos($id_rubro, $id_periodo, $anio_sistema_id);
        $data['cantidadS'] = $reprogramacion['cantidad'];
        $data['precio_unitarioS'] = $reprogramacion['precio_unitario'];
        $data['id_monto'] = $reprogramacion['id_monto'];
        $data['periodo_id'] = $id_periodo;
        $this->load->view('reporte_periodo', $data);
    }

    function muestraPeriodos() {
        //$data = $this->loadIntro();
        $this->load->model('rubros_model');
        $this->load->library('session');
        $idRubro = $_POST['id_rubro'];
        $idTrimestre = $this->session->userdata('id_periodo');
        $anio_sistema_id = $this->session->userdata('id_anio');
        for ($periodo = 4; $periodo <= $idTrimestre; $periodo++) {
            $rubroTrimestre = $this->rubros_model->getRubroMontoPeriodo($idRubro, $periodo, $anio_sistema_id);
            $trimestre[$rubroTrimestre['idPeriodo']]['periodo'] = $rubroTrimestre['periodo'];
            $trimestre[$rubroTrimestre['idPeriodo']]['cantidad'] = $rubroTrimestre['cantidad'];
            $trimestre[$rubroTrimestre['idPeriodo']]['precioUnitario'] = $rubroTrimestre['precioUnitario'];
        }
        $data['rubro_trimestre'] = $trimestre;
        $this->load->view('ver_periodo', $data);
    }

    function guardarReportePeriodo() {
        //$data = $this->loadIntro();
        $this->load->model('rubros_model');
        $this->load->library('session');
        $anio_sistema_id = $this->session->userdata('id_anio');
        $id_periodo = $this->session->userdata('id_periodo');
        $idRubro = $_POST['id_rubro'];
        $dataInsert['id_rubro'] = $idRubro;
        $id_accion = $_POST['id_accion'];
        $dataInsert['cantidad'] = $_POST['cantidad'];
        $dataInsert['precio_unitario'] = $_POST['precio_unitario'];
        $dataInsert['id_periodo'] = $id_periodo;
        $existePeriodo = $this->rubros_model->existePeriodo($idRubro, $id_periodo);
        if ($existePeriodo >= 1) {
            //hacer update
            $info = $this->rubros_model->InfoRubroPeriodos($idRubro, $id_periodo, $anio_sistema_id);
            $dataInsert['id_monto'] = $info['id_monto'];
            $this->rubros_model->updatePeriodo($dataInsert);
        } else {
            //guardar nuevo
            $this->rubros_model->guardarPeriodo($dataInsert);
        }
        for ($periodo = 4; $periodo <= $id_periodo; $periodo++) {
            $rubroTrimestre = $this->rubros_model->getRubroMontoPeriodo($idRubro, $periodo, $anio_sistema_id);
            $trimestre[$rubroTrimestre['idPeriodo']]['periodo'] = $rubroTrimestre['periodo'];
            $trimestre[$rubroTrimestre['idPeriodo']]['cantidad'] = $rubroTrimestre['cantidad'];
            $trimestre[$rubroTrimestre['idPeriodo']]['precioUnitario'] = $rubroTrimestre['precioUnitario'];
        }
        //print_r ($dataInsert);
        $data['rubro_trimestre'] = $trimestre;
        $this->load->view('ver_periodo', $data);
    }

    /**
     * Función salir del sistema, destruir variables de sesión
     */
    public function salir() {
        //destruyendo sesiones
        $this->session->sess_destroy();
        redirect('proyecto/index');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
