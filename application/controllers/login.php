<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $data['log'] = "5";
        $data['log_texto'] = "";
        $this->load->view('login', $data);
        $this->output->enable_profiler(FALSE);
    }

    /**
     * Función que verifica si existe el usuario,
     * y se puede entrar al sistema, de lo contrario regresa
     * a la pantalla de login
     */
    public function verificar() {
        $user = $this->input->post('usuario');
        $pass = $this->input->post('contrasena');

        $login = $this->Login_model->checarLogin($user, $pass);

        if ($login == 1) {
            $infoUsuario = $this->Usuario_model->obtenerInfoUsuario($user);

            $id_usuario = $infoUsuario['id_usuario'];
            $es_PROFEN = $infoUsuario['es_PROFEN'];
            $id_tipo = $infoUsuario['id_tipo'];

            /*
             * Verificando el tipo de usuario
             * 1 - Escuela
             * 2 - Entidad
             */
            if ($es_PROFEN == 2) {
                $id_entidad = $id_tipo;
                $entidad = $this->Usuario_model->obtener_entidad($id_entidad);
                $id_escuela = "";
                $escuela = "";
            } else if ($es_PROFEN == 1) {
                $id_escuela = $id_tipo;
                $infoEscuela = $this->Usuario_model->obtenerInfoEscuela($id_escuela);
                $id_entidad = $infoEscuela['id_entidad'];
                $escuela = $infoEscuela['escuela'];
                $entidad = $this->Usuario_model->obtener_entidadEsc($id_escuela);
            }

            //Permisos para el usuario logeado.
//            $permisos_proyectos = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '1');
//            $permisos_objetivos = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '2');
//            $permisos_metas = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '3');
//            $permisos_acciones = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '4');
//            $permisos_rubros = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '5');
//            $permisos_responsables = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '6');
//            $permisos_reportes = $this->usuario_model->obtenerPermisoUsuario($id_usuario, $id_sistema, '7');

            $usuario_sesion = array(
                'user' => $user,
                'id_usuario' => $id_usuario,
                'id_entidad' => $id_entidad,
                'entidad' => $entidad,
                'id_escuela' => $id_escuela,
                'escuela' => $escuela,
                'es_PROFEN' => $es_PROFEN,
                'logged_in' => TRUE,
                'id_periodo' => 6, //simulando, en realidad debe ir el periodo
                'id_sistema' => 6  //aqui asignamos de acuerdo a la tabla de sistema
            );

            $this->session->set_userdata($usuario_sesion);
            redirect("responsable/responsables");
        } else {//cualquier otra opcion
            $data['log'] = $login;
            if ($login == '0') {
                $data['log_texto'] = "Contrase&ntilde;a Incorrecta";
            } else if ($login == '2') {
                $data['log_texto'] = "El usuario no existe";
            } else if ($login == '3') {
                $data['log_texto'] = "No puedes acceder al sistema";
            } else {
                $data['log_texto'] = "Error desconocido";
            }
            $this->load->view('login', $data);
        }
    }

}

?>
