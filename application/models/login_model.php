<?php

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * función que verifica usuario y password
     */
    function checarLogin($user, $pass) {
        $pass = substr(md5($pass), 0, 39);
        $this->load->database();
        $id_sistema = 11; //
        $id_nivel = 1; //Proyectos


        $this->db->select('id_usuario, usuario, contrasena');
        $this->db->from('usuario');
        $this->db->where('usuario', $user);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {//existe el usuario
            foreach ($query->result() as $row) {
                $id_ = $row->id_usuario;
                $username_ = $row->usuario;
                $password_ = $row->contrasena;
            }

            $permiteLogin = $this->Login_model->permitirLogin($id_sistema, $id_, $id_nivel);

            if ($password_ == $pass and $permiteLogin == 1) {//la contraseña es la misma
                $log = 1; //si esta logueado
                return $log;
            } else if ($password_ == $pass and $permiteLogin == 0) {//la contraseña es la misma pero no tiene permisos de loguearse
                $log = 3; //si esta logueado
                return $log;
            } else {
                $log = 0; //la contraseña es incorrecta
                return $log;
            }
        } else {//no existe el usuario
            $log = 2; //el usuario no existe
            return $log;
        }
    }

    /**
     * funcion que permite revisar los permisos que tiene el usuario al entrar al sistema
     * @params id_usuario, sistema, nivel
     * Regresa un array con los permisos
     * */
    function obtenerPermisos($id_sistema, $id_usuario, $id_nivel) {
        $this->load->database();
        $sql = "SELECT permiso FROM permiso_sistema WHERE id_sistema=$id_sistema AND id_usuario=$id_usuario AND id_nivel=$id_nivel";

        $query = $this->db->query($sql);

        $row = $query->row();
        
        if (is_null($row) || count($row) == 0 ) {
            $permisos = null;
        }else{
            $permisos = $this->Login_model->permisos($row->permiso);
        }

        return $permisos;
    }

    function permitirLogin($id_sistema, $id_usuario, $id_nivel) {
        $this->load->database();

        $this->db->select('permiso');
        $this->db->from('permiso_sistema');
        $this->db->where('id_sistema', $id_sistema);
        $this->db->where('id_usuario', $id_usuario);
        $this->db->where('id_nivel', $id_nivel);

        $query = $this->db->get();


        if ($query->num_rows() > 0) {//existe en la tabla
            foreach ($query->result() as $row) {
                $permiso = $row->permiso;
                $permisos = $this->Login_model->permisos($permiso);
                $login = $permisos['login'];
                return $login;
            }
        } else {
            return 0;
        }
    }

    //obtiene cuáles son los permisos segun el número dado en decimal
    function permisos($permisoDecimal) {
        $binario = decbin($permisoDecimal);
        $numCarac = strlen($binario);
        if ($numCarac == 5)
            $binario = $binario;
        else if ($numCarac == 4)
            $binario = "0" . $binario;
        else if ($numCarac == 3)
            $binario = "00" . $binario;
        else if ($numCarac == 2)
            $binario = "000" . $binario;
        else if ($numCarac == 1)
            $binario = "0000" . $binario;

        $perm['login'] = substr($binario, -1);
        $perm['ver'] = substr($binario, -2, 1);
        $perm['editar'] = substr($binario, -3, 1);
        $perm['borrar'] = substr($binario, -4, 1);
        $perm['agregar'] = substr($binario, -5, 1);

        return $perm;
    }

}

?>
