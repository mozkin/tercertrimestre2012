<?php

class Usuario_model extends CI_Model {

  function __construct()
  {
    // Call the Model constructor
    parent::__construct();
  }


  /**
   *función que sirve para obtener la información básica del usuario
   *
   */
  function obtenerInfoUsuario($usuario){
    $this->load->database();
    $sql="SELECT id_usuario, es_profen, id_tipo FROM usuario WHERE usuario='$usuario'";
    #echo "sql".$sql;
    $query = $this->db->query($sql);
		
    if($query->num_rows()>0){
      $fila=$query->row_array();
      $user['id_usuario']=$fila['id_usuario'];
      $user['es_PROFEN']=$fila['es_profen'];
      $user['id_tipo']=$fila['id_tipo'];
    }

    return $user;
  }

	
  function obtenerInfoEscuela($id_escuela){
    $this->load->database();
      
    $query=$this->db->query("SELECT * from escuela NATURAL JOIN entidad where id_escuela=$id_escuela");

    if($query->num_rows()>0){
      $fila=$query->row_array();
		  
      $escuela['escuela']=$fila['escuela'];
      $escuela['id_entidad']=$fila['id_entidad'];
      $escuela['entidad']=$fila['entidad'];


    }
      
    return $escuela;
      
  }
	
  function obtener_entidad($id_entidad){
    $this->load->database();
    //buscar informacion propia del usuario
    $query = $this->db->query("select entidad from  entidad where id_entidad=$id_entidad");

    $row = $query->row();	
    $entidad=$row->entidad;
    return $entidad;
  }

  function obtener_escuela($id_escuela){
    $this->load->database();
    //buscar informacion propia del usuario
    $query = $this->db->query("select escuela from  escuela where id_escuela=$id_escuela");

    $row = $query->row();	
    $escuela=$row->escuela;
    return $escuela;
  }

  function obtener_entidadEsc($id_escuela){
    $this->load->database();
    //buscar informacion propia del usuario
    $query = $this->db->query("select id_entidad from escuela where id_escuela='$id_escuela';");

    $row = $query->row();	
    $id_entidad=$row->id_entidad;
    return $id_entidad;
  }





  }
?>
