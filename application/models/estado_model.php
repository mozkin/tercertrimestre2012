<?php
class Estado_model extends CI_Model {


  function __construct() {
        parent::__construct();
  }

  function getEstados() {
    $this->load->database();
    $query = $this->db->query("SELECT * FROM entidad");
    if($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $estados[$row->id_entidad] = $row->entidad;
      }
    } else {
      $estados[] = 0;
    }
    return $estados;
  }

  /**
   * Funcion que recibe el id de Entidad o el id de Escuela y retorna el nombre del estado correspondiente
   *
   * @param integer $idEntidad
   * @param integer $idEscuela
   * @return string
   */
  function getNombreEntidad($idEntidad, $idEscuela) {
    $this->load->database();
    if(!($idEscuela)) {
      $query = $this->db->query("SELECT * FROM entidad WHERE id_entidad =".$idEntidad);
    } else {
      $query = $this->db->query("SELECT * FROM entidad, escuela WHERE id_escuela = ".$idEscuela." AND entidad.id_entidad = escuela.id_entidad");
    }
    foreach ($query->result_array() as $entidad) {
      $nombreEntidad = $entidad['entidad'];
    }
    return $nombreEntidad;
  }

  function getNombreEntidadByProyecto ($idProyecto) {
      $this->load->database();
      $query = $this->db->query("SELECT entidad.*
                                FROM entidad, proyecto 
                                WHERE entidad.id_entidad = proyecto.id_entidad 
                                    AND proyecto.id_proyecto = ".$idProyecto." 
                                    AND es_PROFEN = 2;");
      foreach ($query->result_array() as $entidad) {
          $nombreEntidad = $entidad['entidad'];
      }
      return $nombreEntidad;
  }



}

?>
