<?php

class Objetivos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function Objetivo_model() {
        parent::Model();
        $this->load->helper('url');
    }

    function formularioCapturaObjetivo() {
        //agragndo los datos que van para el formulario
        $data['objetivo_particular'] = "Objetivo Particular";
        $data['area_responsable'] = "Responsable del Objetivo";
        $data['justificacion'] = "Justificaci&oacute;n";
        $data['tipo_objetivo'] = "Tipo de Objetivo";
        $data['subtipo_objetivo'] = "Subtipo de Objetivo";
        //	$data['monto_solicitado']="Monto Solicitado";

        $data['fobjetivo_particular'] = array('name' => 'objetivo_particular',
            'cols' => 50,
            'rows' => 5,
            'maxlength' => '200',
            'minlength' => '5',
            'id' => 'descripcion',
            'class' => 'required');
        $data['farea_responsable'] = array('name' => 'area_responsable',
            'size' => 50,
            'maxlength' => '100',
            'id' => 'area_responsable', 'class' => 'required'
        );
        $data['fjustificacion'] = array('name' => 'justificacion',
            'maxlength' => '200',
            'cols' => 50,
            'rows' => 5,
            'id' => 'justificacion',
            'class' => 'required');
        //	$data['fmonto_solicitado']=array('name'=>'monto_solicitado',
        //								'size'=>17,
        //							'id'=>'monto_solicitado','class'=>'required') ;

        return $data;
    }

    function obtenerTipoObjetivosSel() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_tipo_de_objetivo, tipo_de_objetivo from tipo_de_objetivo");
        foreach ($query->result() as $indice) {
            $tipoObjetivos[$indice->id_tipo_de_objetivo] = $indice->tipo_de_objetivo;
        }
        return $tipoObjetivos;
    }

    function obtenerSubTipoObjetivosSel() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_subtipo_de_objetivo, subtipo_de_objetivo from subtipo_de_objetivo");

        foreach ($query->result() as $indice) {
            $subTipoObjetivos[$indice->id_subtipo_de_objetivo] = $indice->subtipo_de_objetivo;
        }
        return $subTipoObjetivos;

        return $query->result();
    }

    function obtenerTipoSubtipo() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select tipo_de_objetivo.id_tipo_de_objetivo, subtipo_de_objetivo.id_subtipo_de_objetivo, tipo_de_objetivo.tipo_de_objetivo, subtipo_de_objetivo.subtipo_de_objetivo from subtipo_de_objetivo, tipo_de_objetivo where tipo_de_objetivo.id_tipo_de_objetivo=subtipo_de_objetivo.id_tipo_de_objetivo order by tipo_de_objetivo, subtipo_de_objetivo");

        foreach ($query->result() as $indice) {
            $subTipoObjetivos[$indice->id_subtipo_de_objetivo] = $indice->tipo_de_objetivo . "-" . $indice->subtipo_de_objetivo;
        }
        return $subTipoObjetivos;

        return $query->result();
    }

    function obtenerObjetivos($idProyecto) {
        $this->load->database();

        $sql = "SELECT * from objetivos NATURAL JOIN subtipo_de_objetivo WHERE id_proyecto=$idProyecto AND eliminado = false ORDER BY id_objetivos";
        //    echo $sql;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function InfoObjetivo($id_objetivo) {
        $this->load->database();

        $query = $this->db->query("select id_objetivos, descripcion, area_responsable, justificacion,  subtipo_de_objetivo,  subtipo_de_objetivo.id_subtipo_de_objetivo from objetivos,  subtipo_de_objetivo where  objetivos.id_subtipo_de_objetivo=subtipo_de_objetivo.id_subtipo_de_objetivo and  id_objetivos=$id_objetivo");


        foreach ($query->result_array() as $objetivo) {
            $objetivo['id_objetivos'];
            $objetivo['descripcion'];
            $objetivo['area_responsable'];
            $objetivo['justificacion'];
            // $objetivo['monto_solicitado'];
            $objetivo['subtipo_de_objetivo'];
            $objetivo['id_subtipo_de_objetivo'];
        }

        return $objetivo;
    }

    function insert_objetivo($data) {
        $this->load->database();
        $id_proyecto = $data['id_proyecto'];
        $descripcion = $data['descripcion'];
        $justificacion = $data['justificacion'];
        $id_subtipo_de_objetivo = $data['id_subtipo_de_objetivo'];

        $data = array(
            "descripcion" => $descripcion,
            "justificacion" => $justificacion,
            "id_proyecto" => $id_proyecto,
            "id_subtipo_de_objetivo" => $id_subtipo_de_objetivo
        );
        $this->db->insert('objetivos', $data);
    }

    function update_objetivo($data) {
        $this->load->database();
        $id_proyecto = $data['id_proyecto'];
        $descripcion = $data['descripcion'];
        $justificacion = $data['justificacion'];
        $id_subtipo_de_objetivo = $data['id_subtipo_de_objetivo'];
        $id_objetivo = $data['id_objetivo'];

        $data = array(
            "descripcion" => $descripcion,
            "justificacion" => $justificacion,
            "id_proyecto" => $id_proyecto,
            "id_subtipo_de_objetivo" => $id_subtipo_de_objetivo
        );
        $this->db->where('id_objetivos', $this->input->post('id_objetivo'));
        $this->db->update('objetivos', $data);
    }

     //Elimina lógicamente el objetivo, asignando la bandera de eliminado como verdadero
    function eliminado_logico_objetivo($id_objetivo){
         $this->load->database();

        $dataInsert = array(
             "eliminado" => 'true'
        );

        $this->db->where('id_objetivos', $id_objetivo);
        $this->db->update('objetivos', $dataInsert);
    }

    function eliminarObjetivo($id_objetivo) {
        $this->load->database();

        //eliminar definitivamente
        $this->db->delete('objetivos', array('id_objetivos' => $id_objetivo));
    }

    function numObjetivos($id_proyecto) {
        $this->load->database();

        $query = $this->db->query("select id_objetivos, descripcion from objetivos where id_proyecto=$id_proyecto ");

        return $query->num_rows();
    }

}

?>
