<?php

//aqui captura_model


class Informetrimestral_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function fecha() {
        $mes = date("n");
        $mesArray = array(1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
        $semana = date("D");
        $semanaArray = array("Mon" => "Lunes", "Tue" => "Martes", "Wed" => "Miercoles", "Thu" => "Jueves", "Fri" => "Viernes", "Sat" => "Sábado", "Sun" => "Domingo",);
        $mesReturn = $mesArray[$mes];
        $semanaReturn = $semanaArray[$semana];
        $dia = date("d");
        $anyo = date("Y");
        return $semanaReturn . " " . $dia . " de " . $mesReturn . " de " . $anyo;
    }

    function reporteProfen($proyecto) {
        $this->load->database();
        $this->load->library('fpdf');
        $this->load->library('session');
        $filename = $this->session->userdata('user');
        $basepath = dirname(FCPATH);
        $base = $this->config->item('base_url');
        /*
         * TODO: agregar este path estatico por uno dinamico
         */
        $link = "tercertrimestre2012/pdf/" . $filename . ".pdf";
        $archivo = $basepath . "/" . $link;
        //echo $archivo;
        //echo implode("|" ,$proyecto);
        $id_proyecto = $proyecto['id_proyecto'];
        $id_periodo = $proyecto['id_periodo'];
        $periodo = $proyecto['periodo'];
        $id_anio = $proyecto['id_anio'];
        $id_reprogramacion = 8;
        $cadenaPeriodo = "Reporte Tercer Trimestre 2012"; // . $periodo;
        $w_total = 111;
        $this->fpdf->AliasNbPages();
        $this->fpdf->AddPage();
        $this->fpdf->SetFont('Arial', 'B', 12);
        $tx1 = utf8_decode('Proyecto Integral para el Programa de Fortalecimiento de la Escuela Normal');
        $this->fpdf->Ln(10);
        $this->fpdf->Cell(0, 6, $tx1, 0, 1, 'C');
        $this->fpdf->SetFont('Arial', 'B', 11);
        $this->fpdf->Cell(0, 6, utf8_decode($cadenaPeriodo), 0, 1, 'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial', '', 8);
        $fecha = $this->Informetrimestral_model->fecha();
        $this->fpdf->Cell(0, 4, utf8_decode($fecha), 0, 0, 'R');
        $this->fpdf->Ln(20);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetFont('Arial', 'B', 9);
        $this->fpdf->Cell(30, 5, "Entidad: ", 1, 0, '', 1);
        $this->fpdf->SetFont('Arial', '', 9);
        $entidad = $proyecto['entidad'];
        $this->fpdf->Cell(120, 5, utf8_decode($entidad), 1, 0, '', 1);
        $this->fpdf->Ln(5);
        $this->fpdf->SetFont('Arial', 'B', 9);
        $this->fpdf->Cell(30, 5, "Escuela: ", 1, 0, '', 1);
        $this->fpdf->SetFont('Arial', '', 9);
        $escuela = $proyecto['escuela'];
        $this->fpdf->Cell(120, 5, utf8_decode($escuela), 1, 0, '', 1);
        $this->fpdf->Ln(5);
        $this->fpdf->SetFont('Arial', 'B', 9);
        $this->fpdf->Cell(30, 5, "Tipo de proyecto: ", 1, 0, '', 1);
        $this->fpdf->SetFont('Arial', '', 9);
        $tipo_proyecto = $proyecto['tipo_proyecto'];
        $this->fpdf->Cell(120, 5, utf8_decode($tipo_proyecto), 1, 0, '', 1);
        $this->fpdf->Ln(5);
        $nombre_proyecto = "\" " . $proyecto['nombre'] . " \"";
        $this->fpdf->MultiCell(150, 5, utf8_decode($nombre_proyecto), 0, 'C', 1);
        $this->fpdf->Ln(10);
        //responsable
        $responsable = "Datos del responsable: \nNombre: " . $proyecto['nombre_responsable'] . "\nCargo:  " . $proyecto['cargo'] . "\nGrado Academico:  " . $proyecto['grado_academico'] . "\nTelefono: " . $proyecto['telefono'] . "\nDireccion de correo: " . $proyecto['email'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($responsable), 1, '', 0);
        //objetivo general
        $objetivo_proyecto = "Objetivo general del proyecto: " . $proyecto['objetivo'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($objetivo_proyecto), 1, '', 0);
        //objetivo general
        $justificacion_proyecto = "Justificacion del proyecto: " . $proyecto['justificacion'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($justificacion_proyecto), 1, '', 0);
        $this->fpdf->ln(10);
        $TotalProyecto = 0;
        $this->getReporteCompleto($id_proyecto, $id_periodo, $id_reprogramacion, $periodo, $cadenaPeriodo, $base, $link, $archivo, $id_anio);
        return $filename;
    }

    function reporteProgen($proyecto) {
        //echo $proyecto;
        $this->load->database();
        $this->load->library('fpdf');
        $this->load->library('session');
        $filename = $this->session->userdata('user');
        $basepath = dirname(FCPATH);
        $base = $this->config->item('base_url');
        $link = "tercertrimestre2012/pdf/" . $filename . '_' . $proyecto['id_tipo_proyecto'] . ".pdf";
        $archivo = $basepath . "/" . $link;
        //echo $archivo;
        $id_proyecto = $proyecto['id_proyecto'];
        $id_periodo = $proyecto['id_periodo'];
        $periodo = $proyecto['periodo'];
        $id_anio = $proyecto['id_anio'];
        //echo($id_anio);
        $id_reprogramacion = 8; // TODO: Reprogramacion el 2 agregar getIdReprogramacion();
        $cadenaPeriodo = "Reporte Tercer Trimestre 2012"; // . $periodo;
        $this->fpdf->AliasNbPages();
        $this->fpdf->AddPage();
        $this->fpdf->SetFont('Arial', 'B', 12);
        $tx1 = utf8_decode('Proyecto Integral para el Programa de Gestión de la Escuela Normal');
        $this->fpdf->Ln(10);
        $this->fpdf->Cell(0, 6, $tx1, 0, 1, 'C');
        $this->fpdf->SetFont('Arial', 'B', 11);
        $this->fpdf->Cell(0, 6, utf8_decode($cadenaPeriodo), 0, 1, 'C');
        $this->fpdf->Ln();
        $this->fpdf->SetFont('Arial', '', 8);
        $fecha = $this->Informetrimestral_model->fecha();
        $this->fpdf->Cell(0, 4, $fecha, 0, 0, 'R');
        $this->fpdf->Ln(20);
        $this->fpdf->SetTextColor(255);
        $this->fpdf->SetFont('Arial', 'B', 9);
        $this->fpdf->Cell(30, 5, "Entidad: ", 1, 0, '', 1);
        $this->fpdf->SetFont('Arial', '', 9);
        $entidad = $proyecto['entidad'];
        $this->fpdf->Cell(104, 5, utf8_decode($entidad), 1, 0, '', 1);
        $this->fpdf->Ln(5);
        $this->fpdf->SetFont('Arial', 'B', 9);
        $this->fpdf->Cell(30, 5, "Tipo de proyecto: ", 1, 0, '', 1);
        $this->fpdf->SetFont('Arial', '', 9);
        $tipo_proyecto = $proyecto['tipo_proyecto'];
        $this->fpdf->Cell(104, 5, utf8_decode($tipo_proyecto), 1, 0, '', 1);
        $this->fpdf->Ln(5);
        $nombre_proyecto = "\" " . $proyecto['nombre'] . " \"";
        $this->fpdf->MultiCell(134, 5, utf8_decode($nombre_proyecto), 1, 'C', 1);
        $this->fpdf->Ln(10);
        //responsable
        $this->fpdf->SetFont('Arial', '', 8);
        $responsable = "Datos del responsable: \nNombre: " . $proyecto['nombre_responsable'] . "\nCargo:  " . $proyecto['cargo'] . "\nGrado Academico:  " . $proyecto['grado_academico'] . "\nTelefono: " . $proyecto['telefono'] . "\nDireccion de correo: " . $proyecto['email'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($responsable), 1, '', 0);
        //objetivo general
        $objetivo_proyecto = "Objetivo general del proyecto: " . $proyecto['objetivo'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($objetivo_proyecto), 1, '', 0);
        //objetivo general
        $justificacion_proyecto = "	Justificacion del proyecto: " . $proyecto['justificacion'];
        $this->fpdf->SetTextColor(0);
        $this->fpdf->MultiCell(190, 5, utf8_decode($justificacion_proyecto), 1, '', 0);
        $this->fpdf->ln(10);
        $TotalProyecto = 0;
        $TotalGastado = 0;
        //echo ($id_proyecto);
        $this->getReporteCompleto($id_proyecto, $id_periodo, $id_reprogramacion, $periodo, $cadenaPeriodo, $base, $link, $archivo, $id_anio);
        return $filename;
    }

    function getReporteCompleto($id_proyecto, $id_periodo, $id_reprogramacion, $periodo, $cadenaPeriodo, $base, $link, $archivo, $id_anio, $id_documento_nivel = 4) {
        $this->load->library('fpdf');
        $this->load->library('digitocontrol');
        $w_total = 111;
        $primerTrimestre = 9;
        $ResiduoTotal = 0;
        $TotalProyecto = 0;
        $TotalGastado = 0;
        //print_r($periodo);
        $queryObjetivos = $this->db->query("
            SELECT o.id_objetivos, o.descripcion as descripcion_objetivo,  o.id_subtipo_de_objetivo,
                o.justificacion as justificacion_objetivo, tob.tipo_de_objetivo, so.subtipo_de_objetivo
            FROM objetivos AS o, tipo_de_objetivo AS tob, subtipo_de_objetivo AS so
            WHERE tob.id_tipo_de_objetivo=so.id_tipo_de_objetivo
                AND so.id_subtipo_de_objetivo=o.id_subtipo_de_objetivo
                AND id_proyecto=$id_proyecto
                AND eliminado = FALSE
                ORDER BY o.id_objetivos"
        );
        foreach ($queryObjetivos->result_array() as $objetivo) {
            $id_objetivo = $objetivo['id_objetivos'];
            //$this->fpdf->SetLeftMargin(10);
            $this->fpdf->SetFont('Arial', 'B', 9);
            $this->fpdf->Cell(17, 5, "Objetivo: ", 0, 0, '', 0);
            $this->fpdf->SetFont('Arial', '', 9);
            $this->fpdf->MultiCell(190, 5, utf8_decode($objetivo['descripcion_objetivo']), 0, '', 0);
            $this->fpdf->MultiCell(190, 5, "Tipo: " . utf8_decode($objetivo['tipo_de_objetivo']), 0, '', 0);
            $this->fpdf->MultiCell(190, 5, "SubTipo: " . utf8_decode($objetivo['subtipo_de_objetivo']), 0, '', 0);
            $this->fpdf->MultiCell(190, 5, "Justificacion: " . utf8_decode($objetivo['justificacion_objetivo']), 0, '', 0);
            $queryMetas = $this->db->query("
                    SELECT m.id_metas, m.descripcion as descripcion_meta, m.unidad_de_medida,
                        tipo_de_beneficiario, beneficiarios_hombres, beneficiarios_mujeres
                    FROM metas AS m,  tipo_de_beneficiario AS tb
                    WHERE  tb.id_tipo_de_beneficiario=m.id_tipo_de_beneficiario
                        AND id_objetivos=$id_objetivo
			AND eliminado=FALSE
		    ORDER BY 1 ASC"
            );
            foreach ($queryMetas->result_array() as $meta) {
                $id_meta = $meta['id_metas'];
                //$this->fpdf->SetLeftMargin(15);
                $this->fpdf->SetFont('Arial', 'B', 9);
                $this->fpdf->Cell(10, 5, "Meta: ", 0, 0, '', 0);                
                $this->fpdf->SetFont('Arial', '', 9);
                $this->fpdf->MultiCell(180, 5, utf8_decode($meta['descripcion_meta']), 0, '', 0);
                $this->fpdf->MultiCell(180, 5, "Unidad de Medida: " . utf8_decode($meta['unidad_de_medida']), 0, '', 0);
                $this->fpdf->MultiCell(180, 5, "Tipo de Beneficiario: " . utf8_decode($meta['tipo_de_beneficiario']), 0, '', 0);
                $beneficiarios_hombres = $meta['beneficiarios_hombres'];
                $beneficiarios_mujeres = $meta['beneficiarios_mujeres'];
                $num_beneficiarios = $beneficiarios_mujeres + $beneficiarios_hombres;
                $this->fpdf->MultiCell(180, 5, "No. Beneficiarios: " . $num_beneficiarios, 0, '', 0);
                $this->fpdf->MultiCell(180, 5, "Hombres: " . $beneficiarios_hombres, 0, '', 0);
                $this->fpdf->MultiCell(180, 5, "Mujeres: " . $beneficiarios_mujeres, 0, '', 0);

                
                

               //INFORME PROGRAMATICO

                //echo  "meta" . $id_meta . "periodo" . $id_periodo;

               $queryInformeAvanceProgramatico = $this->db->query("
                SELECT * FROM meta_justificacion WHERE id_metas = $id_meta  AND id_periodo = $id_periodo;");

               $medida_alcanzada = "";
               $porcentaje_alcanzado = "";
               $justificacion = "";

               foreach ($queryInformeAvanceProgramatico->result_array() as $avance) {
                
                $medida_alcanzada = $avance['medida_alcanzada'];
                $porcentaje_alcanzado = $avance['porcentaje_alcanzado'];
                $justificacion = $avance['justificacion'];
            }
         
                $this->fpdf->Cell($w_total + 64, 7, "Informe Programatico", 1, 0, 'C'); 

                $w = array(60, 60, 55);
                $headerInfProg = array("Unidad de Medida Alcanzada", "Porcentaje Alcanzado", utf8_decode("Justificación"));
                        $this->fpdf->SetFont('Arial', 'B', 8);                        
                        $this->fpdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($headerInfProg); $i++) {
                            $this->fpdf->Cell($w[$i], 7, $headerInfProg[$i], 1, 0, 'C');
                        }
                $this->fpdf->Ln();

                $this->fpdf->Cell($w[0], 6, utf8_decode($medida_alcanzada), 1);
                $this->fpdf->Cell($w[1], 6, utf8_decode($porcentaje_alcanzado), 1);
                $this->fpdf->Cell($w[2], 6, utf8_decode($justificacion), 1);



                $this->fpdf->Ln();
                $this->fpdf->Ln();
                //INFORME PROGRAMATICO


                /* Meta Avance */
                for ($a = $primerTrimestre; $a <= $id_periodo; $a++) {
                    $queryMetaAvance = $this->db->query("SELECT * FROM v_meta_avance_periodo
                                            WHERE id_metas = $id_meta
                                            AND id_periodo = $a
                                            AND anyo = $id_anio");
                    if ($queryMetaAvance->num_rows() > 0) {
                        foreach ($queryMetaAvance->result_array() as $metaAvance) {
                            $dataMetaAvance['idMetaAvance'] = $metaAvance['id_meta_avance'];
                            $dataMetaAvance['idMetas'] = $metaAvance['id_metas'];
                            $dataMetaAvance['idPeriodo'] = $metaAvance['id_periodo'];
                            $dataMetaAvance['periodo'] = $metaAvance['periodo'];
                            $dataMetaAvance['mujeres'] = $metaAvance['mujeres'];
                            $dataMetaAvance['hombres'] = $metaAvance['hombres'];
                        }
                        $this->fpdf->SetFont('Arial', 'B', 8);
                        $w = array(63, 20, 20);
                        $header = array("Periodo", "Hombres", "Mujeres");
                        $this->fpdf->Cell($w_total - 8, 7, utf8_decode("Avance Programatico"), 1, 0, 'C');
                        $this->fpdf->Ln();
                        for ($i = 0; $i < count($header); $i++) {
                            $this->fpdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        }
                        $this->fpdf->Ln();
                        $this->fpdf->SetFont('Arial', '', 8);
                        $this->fpdf->Cell($w[0], 6, $dataMetaAvance['periodo'], 'LR');
                        $this->fpdf->Cell($w[1], 6, utf8_decode($dataMetaAvance['hombres']), 'LR');
                        $this->fpdf->Cell($w[2], 6, utf8_decode($dataMetaAvance['mujeres']), 'LR');
                        $this->fpdf->ln();
                        $this->fpdf->Cell($w_total - 8, 0, " ", 'B');
                        $this->fpdf->ln();
                    }
                }

                //acciones
                $queryAccion = $this->db->query("
                            SELECT a.id_accion, a.accion, a.descripcion" . $id_anio . " as descripcion_accion
                            FROM accion AS a
                            WHERE id_metas=$id_meta
			    AND eliminado = FALSE
			    ORDER BY 1 ASC");
                //          ORDER BY 1" --cambiado para el orden de las acciones
//                );
                foreach ($queryAccion->result_array() as $accion) {
                    //$this->fpdf->ln(5);
                    $id_accion = $accion['id_accion'];
                    //$this->fpdf->SetLeftMargin(20);
                    $this->fpdf->SetFont('Arial', 'B', 9);
                    $this->fpdf->Cell(12, 5, "Accion: ", 0, 0, '', 0);
                    $this->fpdf->SetFont('Arial', '', 9);
                    $this->fpdf->MultiCell(180, 5, utf8_decode($accion['accion']), 0, '', 0);
                    $this->fpdf->MultiCell(180, 5, "Especificaciones: " . utf8_decode($accion['descripcion_accion']), 0, '', 0);

                    /* Accion alcance */
                    for ($b = $primerTrimestre; $b <= $id_periodo; $b++) {
                        $queryAvance = $this->db->query("SELECT * FROM v_accion_alcance_periodo
                                                        WHERE id_accion = $id_accion
                                                            AND id_periodo = $b
                                                            AND anyo = $id_anio");
                        if ($queryAvance->num_rows() > 0) {
                            foreach ($queryAvance->result_array() as $accionAlcance) {
                                $dataAccionAlcance['idAccionAlcance'] = $accionAlcance['id_accion_alcance'];
                                $dataAccionAlcance['idAccion'] = $accionAlcance['id_accion'];
                                $dataAccionAlcance['idPeriodo'] = $accionAlcance['id_periodo'];
                                $dataAccionAlcance['periodo'] = $accionAlcance['periodo'];
                                $dataAccionAlcance['especificaciones'] = $accionAlcance['especificaciones'];
                                $dataAccionAlcance['breve_descripcion'] = $accionAlcance['breve_descripcion'];
                            }
                            $this->fpdf->SetFont('Arial', 'B', 8);
                            $w = array(43, 140);
                            $this->fpdf->Cell($w_total + 72, 7, utf8_decode("Alcance Accion"), 1, 0, 'C');
                            $this->fpdf->Ln();
                            $this->fpdf->SetFont('Arial', '', 8);
                            $this->fpdf->Cell($w[0], 5, "Periodo: ", 'LR');
                            $this->fpdf->Cell($w[1], 5, $dataAccionAlcance['periodo'], 'LR');
                            $this->fpdf->Ln();
                            $this->fpdf->Cell($w[0], 5, "Especificacion: ", 'LR');
                            $this->fpdf->MultiCell($w[1], 5, utf8_decode($dataAccionAlcance['especificaciones']), 'LR', 'J');
                            $this->fpdf->Cell($w[0], 5, "Breve Descripcion: ", 'LR');
                            $this->fpdf->MultiCell($w[1], 5, utf8_decode($dataAccionAlcance['breve_descripcion']), 'LR', 'J');
                            $this->fpdf->Cell($w_total + 72, 0, " ", 'B');
                            $this->fpdf->ln();
                            $this->fpdf->SetFont('Arial', 'B', 8);
                            $this->fpdf->Cell($w_total + 50, 15, utf8_decode("Reporte Financiero"), 0, 0, 'C');
                            $this->fpdf->ln();
                        }
                    }

                    //Fin Alcance por accion
                    $numRubros = 0;
                    $this->fpdf->SetFont('Arial', '', 8);
                    $queryRubros = $this->db->query("
                                SELECT r.id_rubro, r.id_clasificador, c.clasificador, c.id_concepto_de_gasto, cg.concepto_de_gasto, precio_unitario, cantidad
                                FROM rubro AS r, clasificador AS c, concepto_de_gasto AS cg, monto AS m
                                WHERE r.id_clasificador=c.id_clasificador
                                    AND cg.id_concepto_de_gasto=c.id_concepto_de_gasto
                                    AND r.id_rubro=m.id_rubro
                                    AND  id_accion=" . $id_accion . "
                                    AND id_periodo=" . $id_reprogramacion . "
                                    AND anyo=" . $id_anio . "
				    AND r.eliminado = FALSE
				    ORDER BY 1 ASC");
                    $numRubros = $queryRubros->num_rows();
                    if ($numRubros >= 1) {
                        //tablas de rubros
                        $totales['residuoRubro'] = 0;
                        $totales['sumaGastado'] = 0;
                        $totales['sumaReportado'] = 0;
                        $w = array(30, 60, 35, 14, 18, 18);
                        $header = array("Concepto", "Rubro de gasto", "Periodo", "Cantidad", "P/unidad", "Total");
                        $this->fpdf->SetFont('Arial', 'B', 8);
                        $this->fpdf->Cell($w_total + 64, 7, "RECURSOS 2011-2012", 1, 0, 'C');
                        $this->fpdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($header); $i++) {
                            $this->fpdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        }
                        $this->fpdf->Ln();
                        $precioUnitario = 0;
                        $cantidad = 0;
                        $totalRubroActual = 0;
                        $totalRubros = 0;
                        $this->fpdf->SetFont('Arial', '', 8);
                        foreach ($queryRubros->result_array() as $rubro) {
                            $id_rubro = $rubro['id_rubro'];
                            $precioUnitario = $rubro['precio_unitario'];
                            $cantidad = $rubro['cantidad'];
                            $totalRubroActual = $precioUnitario * $cantidad;
                            $totalRubros = $totalRubroActual + $totalRubros;
                            $this->fpdf->Cell($w[0], 6, utf8_decode($rubro['concepto_de_gasto']), 'LR');
                            $this->fpdf->Cell($w[1], 6, utf8_decode($rubro['clasificador']), 'LR');
                            $this->fpdf->Cell($w[2], 6, " ", 'LR');
                            $this->fpdf->Cell($w[3], 6, " ", 'LR');
                            $this->fpdf->Cell($w[4], 6, " ", 'LR');
                            $this->fpdf->Cell($w[5], 6, " ", 'LR');
                            $this->fpdf->ln(3);
                            $this->fpdf->Cell($w[0], 6, " ", 'LR');
                            $this->fpdf->Cell($w[1], 6, " ", 'LR');
                            $this->fpdf->Cell($w[2], 6, "Programado", 'LR');
                            $this->fpdf->Cell($w[3], 6, utf8_decode($cantidad), 'LR', 0, 'R');
                            $this->fpdf->Cell($w[4], 6, number_format($precioUnitario, 2, '.', ','), 'LR', 0, 'R');
                            $this->fpdf->Cell($w[5], 6, number_format($totalRubroActual, 2, '.', ','), 'LR', 0, 'R');
                            $this->fpdf->ln();
                            $totales = $this->getReportePeriodo($totales, $totalRubroActual, $w, $id_rubro, $periodo, $id_anio, $id_periodo);
                            $this->fpdf->Cell(173, 0, " ", 'B');
                            $this->fpdf->ln();
                        }
                        //Fin Rubro
                        $TotalProyecto = $TotalProyecto + $totalRubros;
                        $tempReportado = $totales['sumaReportado'] + $totales['sumaReportado'];
                        $totalReportadoAccion = $totales['sumaReportado'] - $tempReportado;
                        $TotalGastado = $TotalGastado + $totalReportadoAccion;
                        $this->fpdf->Cell($w_total + 64, 7, "TOTALES POR ACCION", 1, 0, 'C');
                        $this->fpdf->ln();
                        $this->fpdf->Cell($w_total + 46, 7, "Total Programado", 1, 0, 'C');
                        $this->fpdf->Cell($w[5], 7, number_format($totalRubros, 2, '.', ','), 1, 0, 'R');
                        $this->fpdf->ln();
                        $this->fpdf->Cell($w_total + 46, 7, "Totales por Trimestre", 1, 0, 'C');
                        $this->fpdf->Cell($w[5], 7, number_format($totalReportadoAccion, 2, '.', ','), 1, 0, 'R');
                        $this->fpdf->ln();
                        $this->fpdf->Cell($w_total + 46, 7, "Balance por Accion", 1, 0, 'C');
                        $this->fpdf->Cell($w[5], 7, number_format($totales['residuoRubro'], 2, '.', ','), 1, 0, 'R');
                        $this->fpdf->ln(8);
                    }
                    //Fin Rubros por accion
                }
                //Fin accion
                $this->fpdf->ln(5);
            }
            //Fin meta
            $this->fpdf->ln(5);
        }
        // foreach objetivos
        //Descomentar siguiente linea, si las firmas han quedado solas.
        //$this->fpdf->AddPage();
        $this->fpdf->Cell(175, 5, "Resumen del Proyecto integral", 1, 0, 'C');
        $this->fpdf->ln();
        $this->fpdf->Cell(175, 5, "Monto total del Proyecto : " . number_format($TotalProyecto, 2, '.', ','), 1, 0, 'R');
        $this->fpdf->ln();
        $this->fpdf->Cell(175, 5, "Monto total de los reportes trimestrales del Proyecto : " . number_format($TotalGastado, 2, '.', ','), 1, 0, 'R');
        $this->fpdf->ln();
        $ResiduoTotal = $TotalProyecto + $TotalGastado;
        $this->fpdf->Cell(175, 5, "Monto total del Residuo del Proyecto: " . number_format($ResiduoTotal, 2, '.', ','), 1, 0, 'R');
        $this->fpdf->ln(30);
        $this->fpdf->Cell(50, 5, "Firma Responsable de la Entidad", 'T', 0, 'L');
        $this->fpdf->Cell(80, 5, "                               ", 0, 0, 'L');
        $this->fpdf->Cell(50, 5, "Firma Responsable de la Escuela", 'T', 0, 'R');
        $this->fpdf->ln(5);
        /*
         * Aquí la generaciń del digito de control
         */
        $cadenaControl = $this->setCadenaControl($id_proyecto, $id_periodo, $id_documento_nivel);
        $this->fpdf->Cell(190, 5, "Firma digital : " . $cadenaControl['cadena'], 0, 0, 'R');
        $this->fpdf->ln(5);
        //Hasta aquí la generación
        $this->fpdf->Output($archivo);
        //$ruta = $base .'/../'.$link;
        //echo "<a href=" . $ruta . ">Click para ver Reporte</a>";
    }

    function getReportePeriodo($totales, $total_rubro, $w, $rubro_id, $datosPeriodo, $id_anio, $id_periodo) {
        $totalPeriodo = 0;
        $this->load->database();
        $this->load->library('fpdf');
        $primerTrimestre = 9;
        //print_r($datosPeriodo);
        $queryMontos = $this->db->query("
                    SELECT *
                    FROM v_rubro_periodo
                    WHERE id_rubro =" . $rubro_id . "
                        AND id_periodo BETWEEN $primerTrimestre AND " . $id_periodo . "
                        AND anyo = " . $id_anio . "
                    ORDER BY id_periodo "
        );
        $totalTrimestres = 0;
        if ($queryMontos->num_rows() == 0) {
            $totalResiduo = $total_rubro;
            $totalPeriodo = 0;
            $cantidad = 0;
            $precioUnitario = 0;
        } else {
            foreach ($queryMontos->result_array() as $periodo) {
                $precioUnitario = $periodo['precio_unitario'];
                $cantidad = $periodo['cantidad'];
                $totalPeriodo = $precioUnitario * $cantidad;
                $totalTrimestres = $totalTrimestres + $totalPeriodo;
                $temp = $totalPeriodo + $totalPeriodo;
                $totalPeriodo = $totalPeriodo - $temp;
                $this->fpdf->Cell($w[0], 6, " ", 'LR');
                $this->fpdf->Cell($w[1], 6, " ", 'LR');
                $this->fpdf->Cell($w[2], 6, $periodo['periodo'], 'LR');
                $this->fpdf->Cell($w[3], 6, utf8_decode($cantidad), 'LR', 0, 'R');
                $this->fpdf->Cell($w[4], 6, number_format($precioUnitario, 2, '.', ','), 'LR', 0, 'R');
                $this->fpdf->Cell($w[5], 6, number_format($totalPeriodo, 2, '.', ','), 'LR', 0, 'R');
                $this->fpdf->ln();
            }
        }
        $totalResiduo = $total_rubro - $totalTrimestres;
        $this->fpdf->ln(0);
        $this->fpdf->Cell($w[0], 6, " ", 'LR');
        $this->fpdf->Cell($w[1], 6, " ", 'LR');
        $this->fpdf->Cell($w[2], 6, "Balance", 'LR');
        $this->fpdf->Cell($w[3], 6, " ", 'LR');
        $this->fpdf->Cell($w[4], 6, " ", 'LR');
        $this->fpdf->Cell($w[5], 6, number_format($totalResiduo, 2, '.', ','), 'LR', 0, 'R');
        $this->fpdf->ln();
        $totales['residuoRubro'] = $totalResiduo + $totales['residuoRubro'];
        $totales['sumaGastado'] = $totalPeriodo + $totales['sumaGastado'];
        $totales['sumaReportado'] = $totalTrimestres + $totales['sumaReportado'];
        return $totales;
    }

    function getAvanceMeta($id_meta, $id_periodo, $anyo) {
        $queryAvance = $this->db->query("SELECT *
						FROM meta_avance
						WHERE id_metas = $id_meta
						AND id_periodo = $id_periodo
                                                AND anyo = $anyo");
        if ($queryAvance->num_rows() > 0) {
            $this->fpdf->SetFont('Arial', 'B', 8);
            $w = array(63, 20, 20);
            $header = array("Periodo", "Hombres", "Mujeres");
            $this->fpdf->Cell($w_total - 8, 7, utf8_decode("Avance Programatico"), 1, 0, 'C');
            $this->fpdf->Ln();
            for ($i = 0; $i < count($header); $i++) {
                $this->fpdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
            }
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Arial', '', 8);
            foreach ($queryAvance->result_array() as $avanceMeta) {
                $this->fpdf->Cell($w[0], 6, "Periodo Uno", 'LR');
                $this->fpdf->Cell($w[1], 6, utf8_decode($avanceMeta['hombres']), 'LR');
                $this->fpdf->Cell($w[2], 6, utf8_decode($avanceMeta['mujeres']), 'LR');
                $this->fpdf->ln();
                $this->fpdf->Cell($w_total - 8, 0, " ", 'B');
                $this->fpdf->ln();
            }
        }
    }

    function setCadenaControl($idProyecto, $idPeriodo, $idDocumentoNivel) {
        $cadenaControl = '';
        $this->load->library('digitocontrol');
        $documentoNivel ['nivel'] = 'rubro';
        $query = $this->db->query("
                SELECT id_" . $documentoNivel['nivel'] . ", SUM(total) AS cantidad
                FROM v_rubro_to_proyecto WHERE
                id_proyecto = " . $idProyecto . " AND id_periodo = " . $idPeriodo . "
                AND id_objetivos IS NOT NULL GROUP BY id_" . $documentoNivel['nivel']
        );

        foreach ($query->result_array() as $nivel) {
            $numero = intval($nivel['cantidad']);
            $digitoControl = $this->digitocontrol->calculaSuma($numero);
            //echo $digitoControl;
            $cadenaControl = $cadenaControl . $digitoControl;
        }

        $hoy = date("Y-m-d H:i:s");
        $sql = "SELECT * FROM documento WHERE id_proyecto = " . $idProyecto . " AND id_periodo = " . $idPeriodo;
        $query2 = $this->db->query($sql);
        if ($query2->num_rows() > 0) {
            $data = array(
                'codigo_control' => $cadenaControl,
                'fecha_impresion' => $hoy
            );
            $this->db->where('id_proyecto', $idProyecto);
            $this->db->where('id_periodo', $idPeriodo);
            $query2 = $this->db->update('documento', $data);
        } else {
            $data = array(
                'id_proyecto' => $idProyecto,
                'id_periodo' => $idPeriodo,
                'codigo_control' => $cadenaControl,
                'fecha_impresion' => $hoy
            );
            $query2 = $this->db->insert('documento', $data);
        }
        $res['res_query'] = $query2;
        $res['cadena'] = $cadenaControl . "|" . $hoy;
        return $res;
    }

}

?>
