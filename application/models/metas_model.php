<?php

//aqui captura_model


class Metas_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function Metas_model() {
        parent::Model();
        $this->load->helper('url');
    }

    function formularioCapturaMetas() {
        //agragndo los datos que van para el formulario
        $data['descripcion'] = "Meta";
        $data['cantidad'] = "Cantidad";
        $data['monto_solicitado'] = "Monto solicitado";
        $data['beneficiarios_hombres'] = "Beneficiarios Hombres";
        $data['beneficiarios_mujeres'] = "Beneficiarios Mujeres";
        $data['tipo_de_beneficiario'] = "Tipo de Beneficiarios";

        $data['fdescripcion'] = array('name' => 'descripcion',
            'maxlength' => '200',
            'cols' => 50,
            'rows' => 5,
            'class' => 'required',
            'id' => 'descripcion');

        $data['fcantidad'] = array('name' => 'cantidad',
            'size' => '30',
            'class' => 'required',
            'id' => 'cantidad');
        $data['funidad_de_medida'] = array('name' => 'unidad_de_medida',
            'size' => '30',
            'maxlength' => '40',
            'class' => 'required',
            'id' => 'unidad_de_medida');

        $data['fbeneficiarios_hombres'] = array('name' => 'beneficiarios_hombres',
            'size' => '30',
            'class' => 'required digits',
            'id' => 'beneficiarios_hombres');
        $data['fbeneficiarios_mujeres'] = array('name' => 'beneficiarios_mujeres',
            'size' => '30',
            'class' => 'required digits',
            'id' => 'beneficiarios_mujeres');

        return $data;
    }

    function insert_meta() {
        $this->load->database();
        $id_objetivo = $_POST['id_objetivo'];
        $descripcion = $_POST['descripcion'];
        $id_unidad_de_medida = $_POST['unidad_de_medida'];
        $beneficiarios_hombres = $_POST['beneficiarios_hombres'];
        $beneficiarios_mujeres = $_POST['beneficiarios_mujeres'];
        $id_tipo_de_beneficiario = $_POST['id_tipo_de_beneficiario'];


        $data = array(
            "descripcion" => $descripcion,
            "beneficiarios_hombres" => $beneficiarios_hombres,
            "beneficiarios_mujeres" => $beneficiarios_mujeres,
            "id_objetivos" => $id_objetivo,
            "unidad_de_medida" => $id_unidad_de_medida,
            "id_tipo_de_beneficiario" => $id_tipo_de_beneficiario
        );

        $this->db->insert('metas', $data);
    }

    function update_meta($data) {
        $this->load->database();
        $id_meta = $data['id_meta'];
        $descripcion = $data['descripcion'];
        $unidad_de_medida = $data['unidad_medida'];
        $id_tipo_de_beneficiario = $data['id_tipo_de_beneficiario'];
        $beneficiarios_hombres = $data['beneficiarios_hombres'];
        $beneficiarios_mujeres = $data['beneficiarios_mujeres'];


        $data = array(
            "descripcion" => $descripcion,
            "unidad_de_medida" => $unidad_de_medida,
            "beneficiarios_hombres" => $beneficiarios_hombres,
            "id_tipo_de_beneficiario" => $id_tipo_de_beneficiario,
            "beneficiarios_mujeres" => $beneficiarios_mujeres
        );
        $this->db->where('id_metas', $this->input->post('id_meta'));
        $this->db->update('metas', $data);
    }

    function obtenerUnidadMedidaSel() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_unidad_de_medida,unidad_de_medida from unidad_de_medida");

        return $query->result();
    }

    /**
     * mostrar las meta del objetivo  seleccionado
     */
    function mostrarMetas($id_objetivo) {
        $this->load->database();

        $query = $this->db->query("select id_metas, descripcion,  unidad_de_medida from metas where id_objetivos=$id_objetivo AND eliminado = false ORDER BY 1 ASC");

        return $query->result();
    }

    function obtenerUnidadMedida() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_unidad_de_medida, unidad_de_medida from unidad_de_medida");
        foreach ($query->result() as $indice) {
            $unidadMedida[$indice->id_unidad_de_medida] = $indice->unidad_de_medida;
        }
        return $unidadMedida;
    }

    function obtenerTipoBeneficiario() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_tipo_de_beneficiario, tipo_de_beneficiario from tipo_de_beneficiario");
        foreach ($query->result() as $indice) {
            $tipoBeneficiario[$indice->id_tipo_de_beneficiario] = $indice->tipo_de_beneficiario;
        }
        return $tipoBeneficiario;
    }

    function numMetas($id_objetivo) {
        $this->load->database();

        $query = $this->db->query("select id_metas from metas where id_objetivos=$id_objetivo");

        return $query->num_rows();
    }

    //Elimina lógicamente la meta, asignando la bandera de eliminado como verdadero
    function eliminado_logico_meta($id_meta) {
        $this->load->database();

        $dataInsert = array(
            "eliminado" => 'true'
        );

        $this->db->where('id_metas', $id_meta);
        $this->db->update('metas', $dataInsert);
    }

    function eliminarMeta($id_meta) {
        $this->load->database();

        $this->db->delete('metas', array('id_metas' => $id_meta));
    }

    function InfoMeta($id_meta) {
        $this->load->database();

        /* 		$query=$this->db->query("select id_metas, descripcion, beneficiarios_hombres, beneficiarios_mujeres, unida_de_medida 			tipo_de_beneficiario.id_tipo_de_beneficiario
          from metas,  tipo_de_beneficiario  where metas.id_tipo_de_beneficiario=tipo_de_beneficiario.id_tipo_de_beneficiario and id_metas=$id_meta");
         */
        $query = $this->db->query("select * from metas,  tipo_de_beneficiario  where metas.id_tipo_de_beneficiario=tipo_de_beneficiario.id_tipo_de_beneficiario and id_metas=$id_meta");



        foreach ($query->result_array() as $meta) {
            $meta['id_metas'];
            $meta['beneficiarios_hombres'];
            $meta['beneficiarios_mujeres'];
            $meta['unidad_de_medida'];
            $meta['id_tipo_de_beneficiario'];
        }

        return $meta;
    }

    function updateAvanceProgramatico($idMetaAvance, $benHombres, $benMujeres) {
        $this->load->database();
        $datos = array(
            "hombres" => $benHombres,
            "mujeres" => $benMujeres
        );
        $this->db->where('id_meta_avance', $idMetaAvance);
        $this->db->update('meta_avance', $datos);
    }

    function insertAvanceProgramatico($idMeta, $idPeriodo, $benHombres, $benMujeres) {
        $this->load->database();
        $data = array(
            "id_metas" => $idMeta,
            "id_periodo" => $idPeriodo,
            "hombres" => $benHombres,
            "mujeres" => $benMujeres,
        );
        $this->db->insert('meta_avance', $data);
    }

    function getAvanceProgramatico($idMeta, $idPeriodo) {
        $this->load->database();
        $query = $this->db->query("SELECT * FROM meta_avance
                                    WHERE id_metas = $idMeta
                                    AND id_periodo = $idPeriodo");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $metaAvance) {
                $dataMetaAvance['idMetaAvance'] = $metaAvance['id_meta_avance'];
                $dataMetaAvance['idMetas'] = $metaAvance['id_metas'];
                $dataMetaAvance['idPeriodo'] = $metaAvance['id_periodo'];
                $dataMetaAvance['mujeres'] = $metaAvance['mujeres'];
                $dataMetaAvance['hombres'] = $metaAvance['hombres'];
            }
        } else {
            $dataMetaAvance = 0;
        }
        return $dataMetaAvance;
    }

    function updateInformeAvanceProgramatico($idMetaJustificacion, $medida_alcanzada, $porcentaje_alcanzado, $justificacion) {
        $this->load->database();
        $datos = array(
            "medida_alcanzada" => $medida_alcanzada,
            "porcentaje_alcanzado" => $porcentaje_alcanzado,
            "justificacion" => $justificacion
        );
        $this->db->where('id_meta_justificacion', $idMetaJustificacion);
        $this->db->update('meta_justificacion', $datos);
    }

    function insertInformeAvanceProgramatico($idMeta, $idPeriodo, $medida_alcanzada, $porcentaje_alcanzado, $justificacion) {
        $this->load->database();
        $data = array(
            "id_metas" => $idMeta,
            "id_periodo" => $idPeriodo,
            "medida_alcanzada" => $medida_alcanzada,
            "porcentaje_alcanzado" => $porcentaje_alcanzado,
            "justificacion" => $justificacion
        );
        $this->db->insert('meta_justificacion', $data);
    }

    function getInformeAvanceProgramatico($idMeta, $idPeriodo) {
        $this->load->database();
        $query = $this->db->query("SELECT * FROM meta_justificacion
                                    WHERE id_metas = $idMeta
                                    AND id_periodo = $idPeriodo");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $metaAvance) {
                $dataMetaAvance['idMetaJustificacion'] = $metaAvance['id_meta_justificacion'];
                $dataMetaAvance['idMetas'] = $metaAvance['id_metas'];
                $dataMetaAvance['idPeriodo'] = $metaAvance['id_periodo'];
                $dataMetaAvance['medida_alcanzada'] = $metaAvance['medida_alcanzada'];
                $dataMetaAvance['porcentaje_alcanzado'] = $metaAvance['porcentaje_alcanzado'];
                $dataMetaAvance['justificacion'] = $metaAvance['justificacion'];
            }
        } else {
            $dataMetaAvance = 0;
        }
        return $dataMetaAvance;
    }

    function getPeriodo($idPeriodo) {
        $this->load->database();
        $query = $this->db->query("SELECT * FROM periodo
                                    WHERE id_periodo = $idPeriodo");
        foreach ($query->result_array() as $periodo) {
            $nombrePeriodo = $periodo['periodo'];
        }
        return $nombrePeriodo;
    }

}

?>
