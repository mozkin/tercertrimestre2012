<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

class Escuela_model extends CI_Model {

 
  function __construct() {
        parent::__construct();
    }

  function getEscuelas() {
    $this->load->database();
    $query = $this->db->query("SELECT * FROM escuela");
    if($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $listaEscuelas[$row->id_escuela] = $row->escuela;
      }
    } else {
      $listaEscuelas[] = 0;
    }
    return $listaEscuelas;
  }

  /**
   * funcion que retorna un listado de escuelas dependiendo de la entidad.     *
   * @param integer $idEstado
   * @return array $listaEscuelas
   */
  function getEscuelasEstado($idEstado) {
    $this->load->database();
    $query = $this->db->query("SELECT * FROM escuela WHERE id_entidad = $idEstado");
    if($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $listaEscuelas[$row->id_escuela] = $row->escuela;
      }
    } else {
      $listaEscuelas[] = 0;
    }
    return $listaEscuelas;
  }

  function getNombreEscuela($idEscuela) {
    $this->load->database();
    $query = $this->db->query('SELECT * FROM escuela WHERE id_escuela = '.$idEscuela);
    foreach ($query->result_array() as $escuela) {
      $nombreEscuela = $escuela['escuela'];
    }
    return $nombreEscuela;
  }

  function getNombreEscuelaByProyecto ($idProyecto){
      $this->load->database();
    $query = $this->db->query('SELECT escuela.*
                               FROM escuela, proyecto
                               WHERE escuela.id_escuela = proyecto.id_escuela
                                    AND proyecto.id_proyecto ='.$idProyecto.'
                                    AND es_PROFEN = 1;');
    foreach ($query->result_array() as $escuela) {
      $nombreEscuela = $escuela['escuela'];
    }
    return $nombreEscuela;
  }
}


?>
