<?php

class Sistema_model extends CI_Model {

  function __construct(){
    parent::__construct();
  }

  function obtenerSistema($id_sistema){
    $this->load->database();

    $this->db->select('sistema');
    $this->db->from('sistema');
    $this->db->where('id_sistema', $id_sistema);

    $query = $this->db->get();
    $row= $query->row();
    $sist=$row->sistema;
    
    return $sist;
    
  }
   
}

?>
