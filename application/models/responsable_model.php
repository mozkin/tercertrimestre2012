<?php

class Responsable_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function mostrarResponsable($es_PROFEN, $id_escuela, $id_entidad, $id_periodo) {
        $this->load->database();

        if ($es_PROFEN == 2) {
            $sql = "SELECT DISTINCT responsable.id_responsable, responsable.nombre, cargo, telefono, email, id_grado_academico FROM proyecto, responsable WHERE proyecto.id_responsable=responsable.id_responsable AND es_PROFEN='$es_PROFEN' AND  id_entidad='$id_entidad' AND id_escuela is NULL and proyecto.id_periodo=$id_periodo";

            $query = $this->db->query($sql);
        } else if ($es_PROFEN == 1) {
            $sql = "SELECT DISTINCT responsable.id_responsable, responsable.nombre, cargo, telefono, email, id_grado_academico FROM proyecto, responsable WHERE proyecto.id_responsable=responsable.id_responsable AND  es_PROFEN='$es_PROFEN' AND  id_entidad='$id_entidad' AND id_escuela='$id_escuela' and proyecto.id_periodo=$id_periodo";
            $query = $this->db->query($sql);
        }

        return $query->result();
    }

    function InfoResponsable($id_responsable) {
        $this->load->database();
        $query = $this->db->query("select nombre as nombre_responsable, cargo, telefono, email, responsable.id_grado_academico, grado_academico from responsable, grado_academico where grado_academico.id_grado_academico=responsable.id_grado_academico and id_responsable=$id_responsable ");

        foreach ($query->result_array() as $responsable) {
            $responsable['nombre_responsable'];
            $responsable['cargo'];
            $responsable['telefono'];
            $responsable['email'];
            $responsable['id_grado_academico'];
            $responsable['grado_academico'];
        }
        return $responsable;
    }

    function obtenerGradosAcademicos() {
        $this->load->database();

        $query = $this->db->query("SELECT id_grado_academico, grado_academico FROM grado_academico ");

        foreach ($query->result() as $indice) {
            $gradosAcademicos[$indice->id_grado_academico] = $indice->grado_academico;
        }
        return $gradosAcademicos;
    }

    function updateResponsable($data) {
        $this->load->database();

        $id_responsable = $data['responsableUpd'];
        $cargo = $data['cargo'];
        $id_grado_academico = $data['id_grado_academico'];
        $email = $data['email'];
        $telefono = $data['telefono'];
        $nombre = $data['nombre'];

        $dataInsert = array(
            "nombre" => $nombre,
            "cargo" => $cargo,
            "id_grado_academico" => $id_grado_academico,
            "email" => $email,
            "telefono" => $telefono
        );

        $this->db->where('id_responsable', $id_responsable);
        $this->db->update('responsable', $dataInsert);
    }

}

?>
