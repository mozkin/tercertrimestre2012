<?php

class Proyecto_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function formularioCapturaProyecto() {
        //agragndo los datos que van para el formulario
        $data['nombre'] = "Proyecto Integral";
        $data['objetivo'] = "Objetivo General";
        $data['justificacion'] = "Justificaci&oacute;n del proyecto";
        $data['tipo_proyecto'] = "Tipo de proyecto";
        $data['responsable'] = "Responsable";


        $data['fnombre'] = array('name' => 'nombre',
            'cols' => 50,
            'rows' => 5,
            'maxlength' => '200',
            'minlength' => '5',
            'id' => 'nombre',
            'class' => 'required');
        $data['fobjetivo'] = array('name' => 'objetivo',
            'cols' => 50,
            'rows' => 5,
            'id' => 'objetivo',
            'maxlength' => '300',
            'class' => 'required'
        );
        $data['fjustificacion'] = array('name' => 'justificacion',
            'cols' => 50,
            'rows' => 5,
            'class' => 'required',
            'maxlength' => '300',
            'id' => 'justificacion');
        $data['fresponsable'] = array('name' => 'id_responsable',
            'size' => 50,
            'class' => 'required',
            'id' => 'id_responsable');
        return $data;
    }

    /** Función que obtiene la información de los proyectos
     *  si es es_profen==1 regresa un id
     *  si es es_profen=2 regresa un array con los dos proyectos
     */
    function obtenerProyectos($es_PROFEN, $id_entidad, $id_escuela, $id_periodo) {
        $this->load->database();

        if ($es_PROFEN == 1) {//Si es profen(escuela)
            $sql = "SELECT id_proyecto,  proyecto.nombre proyecto, tipo_proyecto.nombre tipo_proyecto, objetivo, justificacion from proyecto, tipo_proyecto where proyecto.id_tipo_proyecto=tipo_proyecto.id_tipo_proyecto AND proyecto.id_entidad=$id_entidad AND proyecto.id_escuela=$id_escuela AND proyecto.es_profen=$es_PROFEN AND proyecto.id_periodo=$id_periodo AND eliminado = false";
        } else if ($es_PROFEN == 2) {//Si es progen(entidad)
            $sql = "select  id_proyecto,  proyecto.nombre proyecto, tipo_proyecto.nombre tipo_proyecto, objetivo, justificacion from proyecto, tipo_proyecto where proyecto.id_tipo_proyecto=tipo_proyecto.id_tipo_proyecto AND proyecto.id_entidad=$id_entidad AND id_escuela is NULL AND proyecto.es_profen=$es_PROFEN AND proyecto.id_periodo=$id_periodo AND eliminado = false";
        }

        $query = $this->db->query($sql);
        return $query->result();
    }

    function ver_proyecto($id_proyecto) {
        $this->load->database();
        $query = $this->db->query("SELECT p.id_proyecto, p.nombre as proyecto, p.objetivo, p.justificacion, p.observaciones, p.id_escuela, p.id_responsable, r.nombre as responsable, p.id_tipo_proyecto, tp.nombre as tipo_proyecto FROM proyecto p, responsable r, tipo_proyecto tp WHERE p.id_tipo_proyecto=tp.id_tipo_proyecto AND p.id_responsable = r.id_responsable AND p.id_proyecto=$id_proyecto");

        foreach ($query->result_array() as $proyecto1) {
            $proyecto1['id_proyecto'];
            $proyecto1['proyecto'];
            $proyecto1['objetivo'];
            $proyecto1['justificacion'];
            $proyecto1['observaciones'];
            $proyecto1['id_escuela'];
            $proyecto1['id_responsable'];
            $proyecto1['responsable'];
            $proyecto1['id_tipo_proyecto'];
            $proyecto1['tipo_proyecto'];
        }
        return $proyecto1;
        #return $query->result_array();
    }

    function InfoProyecto($id_proyecto) {
        $this->load->database();
        $query = $this->db->query("select id_proyecto, proyecto.nombre, objetivo, justificacion, observaciones, entidad, escuela.id_escuela, escuela,  id_responsable, proyecto.id_tipo_proyecto, tipo_proyecto.nombre as tipo_proyecto from proyecto, entidad, escuela, tipo_proyecto where proyecto.id_entidad=entidad.id_entidad and proyecto.id_escuela=escuela.id_escuela and tipo_proyecto.id_tipo_proyecto=proyecto.id_tipo_proyecto and id_proyecto=$id_proyecto");

        foreach ($query->result_array() as $proyecto1) {
            $proyecto1['id_proyecto'];
            $proyecto1['nombre'];
            $proyecto1['objetivo'];
            $proyecto1['justificacion'];
            $proyecto1['observaciones'];
            $proyecto1['entidad'];
            $proyecto1['id_escuela'];
            $proyecto1['id_tipo_proyecto'];
            $proyecto1['tipo_proyecto'];
            $proyecto1['escuela'];
            $proyecto1['id_responsable'];
        }
        return $proyecto1;
    }
    
    function InfoProyectoProfen($id_proyecto) {
        $this->load->database();
        $query=$this->db->query("SELECT id_proyecto, proyecto.nombre, objetivo, justificacion, observaciones, entidad, escuela.id_escuela, escuela,  id_responsable, proyecto.id_tipo_proyecto, tipo_proyecto.nombre as tipo_proyecto
                                FROM proyecto, entidad, escuela, tipo_proyecto
                                WHERE proyecto.id_entidad=entidad.id_entidad
                                    AND proyecto.id_escuela=escuela.id_escuela
                                    AND tipo_proyecto.id_tipo_proyecto=proyecto.id_tipo_proyecto
                                    AND id_proyecto=$id_proyecto");

        //		print_r($query->result_array());
        foreach ($query->result_array() as $proyecto) {
            $proyecto['id_proyecto'];
            $proyecto['nombre'];
            $proyecto['objetivo'];
            $proyecto['justificacion'];
            $proyecto['observaciones'];
            $proyecto['entidad'];
            $proyecto['id_escuela'];
            $proyecto['id_tipo_proyecto'];
            $proyecto['tipo_proyecto'];
            $proyecto['escuela'];
            $proyecto['id_responsable'];
        }

        return $proyecto;
    }


    function InfoProyectoProgen($id_proyecto) {
        $this->load->database();
        $sql="SELECT id_proyecto, proyecto.nombre, objetivo, justificacion, observaciones, entidad,  id_responsable, proyecto.id_tipo_proyecto, tipo_proyecto.nombre as tipo_proyecto 
			FROM proyecto, entidad,  tipo_proyecto 
			WHERE proyecto.id_entidad=entidad.id_entidad 
				AND  tipo_proyecto.id_tipo_proyecto=proyecto.id_tipo_proyecto 
				AND id_proyecto=$id_proyecto";
        $query=$this->db->query($sql);
        foreach ($query->result_array() as $proyecto1) {
            $proyecto1['id_proyecto'];
            $proyecto1['nombre'];
            $proyecto1['objetivo'];
            $proyecto1['justificacion'];
            $proyecto1['observaciones'];
            $proyecto1['entidad'];
            $proyecto1['id_tipo_proyecto'];
            $proyecto1['tipo_proyecto'];
            $proyecto1['id_responsable'];
        }
        return $proyecto1;
    }


    function InfoProyecto2($id_proyecto) {
        $this->load->database();
        $query = $this->db->query("select id_proyecto, proyecto.nombre, objetivo, justificacion, observaciones, entidad,  id_responsable, proyecto.id_tipo_proyecto, tipo_proyecto.nombre as tipo_proyecto from proyecto, entidad, escuela, tipo_proyecto where proyecto.id_entidad=entidad.id_entidad and  tipo_proyecto.id_tipo_proyecto=proyecto.id_tipo_proyecto and id_proyecto=$id_proyecto");

//		print_r($query->result_array());
        foreach ($query->result_array() as $proyecto1) {
            $proyecto1['id_proyecto'];
            $proyecto1['nombre'];
            $proyecto1['objetivo'];
            $proyecto1['justificacion'];
            $proyecto1['observaciones'];
            $proyecto1['entidad'];
            //  $proyecto1['id_escuela'];
            $proyecto1['id_tipo_proyecto'];
            $proyecto1['tipo_proyecto'];
            // $proyecto1['escuela'];
            $proyecto1['id_responsable'];
        }
        return $proyecto1;
    }

    //Elimina lógicamente el proyecto, asignando la bandera de eliminado como verdadero
    function eliminado_logico_proyecto($id_proyecto){
         $this->load->database();
         
        $dataInsert = array(
            "eliminado" => 'true'
        );

        $this->db->where('id_proyecto', $id_proyecto);
        $this->db->update('proyecto', $dataInsert);
    }

    function updateProyecto($data) {
        $this->load->database();
        $id_proyecto = $data['id_proyecto'];
        $nombre = $data['nombre'];
        $objetivo = $data['objetivo'];
        $justificacion = $data['justificacion'];


        $dataInsert = array(
            "nombre" => $nombre,
            "objetivo" => $objetivo,
            "justificacion" => $justificacion
        );

        $this->db->where('id_proyecto', $data['id_proyecto']);
        $this->db->update('proyecto', $dataInsert);
    }

    /*
     * Función que obtiene el id de proyecto, dado un id_acción
     */

    function obtenerIdProyecto($id_accion) {
        $this->load->database();
        $sql = "select p.id_proyecto , p.nombre from proyecto p, objetivos o,metas m, accion a where p.id_proyecto=o.id_proyecto and o.id_objetivos=m.id_objetivos and m.id_metas=a.id_metas and a.id_accion=$id_accion";

        //echo $sql;

        $query = $this->db->query($sql);

        foreach ($query->result_array() as $proyecto) {
            $proyecto['id_proyecto'];
            $proyecto['nombre'];
        }
        return $proyecto;
    }

    /*
     * función para obtener los años disponibles para el proyecto
     */

    function obtenerAnyos() {
        $this->load->database();

        $query = $this->db->query("select id_anio, anio from anio");

        foreach ($query->result() as $indice) {
            $anio[$indice->id_anio] = $indice->anio;
        }

        //print_r($tipoProyecto);
        return $anio;
    }

    /* función para obtener los años */

    function obtenerPeriodos($id_anio) {
        $this->load->database();

        $query = $this->db->query(" select id_anio, periodo, meses 
									from periodo, periodo_fecha 
									where periodo_fecha.id_periodo=periodo.id_periodo 
										and id_anio=$id_anio");

        foreach ($query->result() as $indice) {
            $anio[$indice->id_anio] = $indice->anio;
        }

        //print_r($tipoProyecto);
        return $anio;
    }
    
    function getPeriodo ($idPeriodo){
		$this->load->database();
        $query = $this->db->query("SELECT * FROM periodo 
                                    WHERE id_periodo = $idPeriodo" );
		foreach ($query->result_array() as $periodo) {
			$nombrePeriodo = $periodo['periodo'];
		}
		return $nombrePeriodo;	 
	}
	
	function proyectoCompleto($id_proyecto) {
		$this->load->database();
		$this->load->model('reporte_model');
		//contar num de objetivos entre 4 y 6
		$query1=$this->db->query("SELECT * FROM objetivos 
								WHERE id_proyecto=$id_proyecto 
									AND eliminado = FALSE");
		$numObjetivos=$query1->num_rows();
		//echo "num Objetivos ".$numObjetivos."<br>";
		if($numObjetivos>=4 and $numObjetivos<=6) {
			//verificando que e num de metas si sea el correcto
		    $query2=$this->db->query("SELECT * FROM metas 
					WHERE id_objetivos IN (
								SELECT id_objetivos FROM objetivos 
								WHERE id_proyecto=$id_proyecto AND eliminado = FALSE
					)"
			);
		    $numMetas=$query2->num_rows();
		    $numMetasObligatorias=$numObjetivos*4;
		    $numMetasProyecto=$numMetas*$numObjetivos;
		    if($numMetasObligatorias==$numMetasProyecto) {
			echo "correcto num de metas<br>";
		    } else {
		    //	echo "el Proyecto no está terminado, cada objetivo debe tener 4 metas<br>";
		    }
		} else {
		    echo "";
		}
    }

}

?>
