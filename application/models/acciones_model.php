<?php

//aqui captura_model


class Acciones_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function Acciones_model() {
        parent::Model();
        $this->load->helper('url');
    }

    function formularioCapturaAcciones() {
        //agragndo los datos que van para el formulario
        $data['accion'] = "Acci&oacute;n";
        $data['descripcion1'] = "Justificaci&oacute;n 2011";
        $data['descripcion2'] = "Justificaci&oacute;n 2012";

        $data['faccion'] = array('name' => 'accion',
            'cols' => 50,
            'rows' => 5,
            'maxlength' => '200',
            'minlength' => '5',
            'class' => 'required',
            'id' => 'accion');
        $data['fdescripcion1'] = array('name' => 'descripcion1',
            'cols' => 50,
            'rows' => 5,
            'maxlength' => '200',
            'minlength' => '5',
            'class' => 'required',
            'id' => 'descripcion1');
        $data['fdescripcion2'] = array('name' => 'descripcion2',
            'cols' => 50,
            'rows' => 5,
            'maxlength' => '200',
            'minlength' => '5',
            'class' => 'required',
            'id' => 'descripcion2');

        return $data;
    }

    function insert_accion($dataInsert) {
        $this->load->database();
        $id_meta = $_POST['id_meta'];
        $descripcion1 = $_POST['descripcion1'];
        $descripcion2 = $_POST['descripcion2'];
        $accion = $_POST['accion'];

        /* insertar la acción 	 */
        $data = array(
            "descripcion1" => $descripcion1,
            "accion" => $accion,
            "descripcion2" => $descripcion2,
            "id_metas" => $id_meta
        );
        $this->db->insert('accion', $data);
    }

    function update_accion($data) {
        $this->load->database();
        $descripcion1 = $data['descripcion1'];
        $descripcion2 = $data['descripcion2'];
        $accion = $data['accion'];

        $dataInsert = array(
            "descripcion1" => $descripcion1,
            "descripcion2" => $descripcion2,
            "accion" => $accion
        );

        $this->db->where('id_accion', $this->input->post('id_accion'));
        $this->db->update('accion', $dataInsert);
    }

    /**
     * mostrar las meta del objetivo  seleccionado
     */
    function mostrarAcciones($id_meta) {
        $this->load->database();


        $query = $this->db->query("select id_accion, accion, descripcion1, descripcion2 from accion where id_metas=$id_meta AND eliminado = false  ORDER BY 1 ASC");

        return $query->result();
    }

    function numAcciones($id_meta) {
        $this->load->database();

        $query = $this->db->query("select id_accion, accion, descripcion1, descripcion2 from accion where id_metas=$id_meta ");

        return $query->num_rows();
    }

     //Elimina lógicamente la acción, asignando la bandera de eliminado como verdadero
    function eliminado_logico_accion($id_accion){
         $this->load->database();

        $dataInsert = array(
              "eliminado" => 'true'
        );

        $this->db->where('id_accion', $id_accion);
        $this->db->update('accion', $dataInsert);
    }

    function eliminarAccion($id_accion) {
        $this->load->database();

        $this->db->delete('accion', array('id_accion' => $id_accion));
    }

    function InfoAccion($id_accion) {
        $this->load->database();

        $query = $this->db->query("select id_accion, accion, descripcion1, descripcion2 from accion where id_accion=$id_accion");


        foreach ($query->result_array() as $accion1) {
            $accion1['id_accion'];
            $accion1['accion'];
            $accion1['descripcion1'];
            $accion1['descripcion2'];
        }

        return $accion1;
    }
    
    function updateAccionAlcance($idAccionAlcance, $especificaciones, $breveDescripcion) {
        $this->load->database();
        $datos = array(
            "especificaciones"=>$especificaciones,
            "breve_descripcion"=>$breveDescripcion
        );
        $this->db->where('id_accion_alcance',$idAccionAlcance);
        $this->db->update('accion_alcance', $datos);
    }

    function insertAccionAlcance($idAccion, $idPeriodo, $especificaciones, $breveDescripcion) {
        $this->load->database();
        $data = array(
            "id_accion"=>$idAccion,
            "id_periodo"=>$idPeriodo,
            "especificaciones"=>$especificaciones,
            "breve_descripcion"=>$breveDescripcion
        );
        $this->db->insert('accion_alcance',$data);
    }

    function getAccionAlcance ($idAccion, $idPeriodo) {
        $this->load->database();
        $query = $this->db->query("SELECT * FROM accion_alcance 
                                    WHERE id_accion = $idAccion 
                                    AND id_periodo = $idPeriodo " );
        if($query->num_rows() > 0) {
            foreach ($query->result_array() as $accionAlcance) {
                $dataAccionAlcance['idAccionAlcance'] = $accionAlcance['id_accion_alcance'];
                $dataAccionAlcance['idAccion'] = $accionAlcance['id_accion'];
                $dataAccionAlcance['idPeriodo'] = $accionAlcance['id_periodo'];
                $dataAccionAlcance['especificaciones'] = $accionAlcance['especificaciones'];
                $dataAccionAlcance['breveDescripcion'] = $accionAlcance['breve_descripcion'];
            }
        } else {
            $dataAccionAlcance = 0;
        }
        return $dataAccionAlcance;
    }
    
    function getPeriodo ($idPeriodo){
		$this->load->database();
        $query = $this->db->query("SELECT * FROM periodo 
                                    WHERE id_periodo = $idPeriodo" );
		foreach ($query->result_array() as $periodo) {
			$nombrePeriodo = $periodo['periodo'];
		}
		return $nombrePeriodo;	 
	}

}

?>
