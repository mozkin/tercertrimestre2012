<?php

//aqui captura_model


class Rubros_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function Rubros_model() {
        parent::Model();
        $this->load->helper('url');
    }

    function formularioCapturaRubros() {
        //agragndo los datos que van para el formulario
        $data['cantidad'] = "Cantidad";
        $data['precio_unitario'] = "Precio unitario";
        $data['anyo'] = "A&ntilde;o de ejecuci&oacute;n";
        $data['periodoApli'] = "Periodo de Aplicaci&oacute;n";

        $data['fcantidad'] = array('name' => 'cantidad',
            'size' => 17,
            'class' => 'required',
            'id' => 'cantidad');
        $data['fprecio_unitario'] = array('name' => 'precio_unitario',
            'size' => 17,
            'class' => 'required',
            'id' => 'precio_unitario');
        $data['fanyo'] = array('name' => 'anyo',
            'size' => 17,
            'class' => 'required',
            'id' => 'anyo');
        $data['fperiodoApli'] = array('name' => 'periodoApli',
            'size' => 17,
            'class' => 'required',
            'id' => 'periodoApli');


        return $data;
    }

    function numRubros($id_accion) {
        $this->load->database();

        $query = $this->db->query("select id_rubro from rubro where id_accion=$id_accion");

        return $query->num_rows();
    }

    function mostrarRubros($id_accion) {
        $this->load->database();

        $query = $this->db->query("select rubro.id_rubro, clasificador, cantidad, precio_unitario, anyo  from rubro, clasificador, monto where rubro.id_clasificador=clasificador.id_clasificador and monto.id_rubro=rubro.id_rubro and id_accion=$id_accion AND rubro.eliminado = false");

        return $query->result();
    }

//funcion que regresa lo rubro del primer año
    function mostrarRubros1($id_accion) {
        $this->load->database();
        $anyo = 1;

        $query = $this->db->query("select rubro.id_rubro, clasificador, cantidad, precio_unitario, anyo, id_periodo_aplicacion  from rubro, clasificador, monto where rubro.id_clasificador=clasificador.id_clasificador and monto.id_rubro=rubro.id_rubro and id_accion=$id_accion and anyo=$anyo AND monto.eliminado = false ORDER BY 1 ASC");

        return $query->result();
    }

//funcion que regresa lo rubro del segundo año
    function mostrarRubros2($id_accion) {
        $this->load->database();
        $anyo = 2;

        $query = $this->db->query("select rubro.id_rubro, clasificador, cantidad, precio_unitario, anyo, id_periodo_aplicacion  from rubro, clasificador, monto where rubro.id_clasificador=clasificador.id_clasificador and monto.id_rubro=rubro.id_rubro and id_accion=$id_accion and anyo=$anyo AND monto.eliminado = false  ORDER BY 1 ASC");

        return $query->result();
    }
    
//funcion que regresa el rubro de acuerdo al año
    function mostrarRubrosAnio($id_accion, $anyo, $id_periodo) {
        $this->load->database();

        $query = $this->db->query("select rubro.id_rubro, clasificador, cantidad, precio_unitario, anyo, id_periodo_aplicacion  from rubro, clasificador, monto where rubro.id_clasificador=clasificador.id_clasificador and monto.id_rubro=rubro.id_rubro AND monto.id_periodo = $id_periodo and id_accion=$id_accion and anyo=$anyo AND monto.eliminado = false ORDER BY 1 ASC");

        return $query->result();
    }

    function obtenerClasificador() {//obtener catalogo para <select>
        $this->load->database();

        $query = $this->db->query("select id_clasificador, concepto_de_gasto, clasificador from clasificador, concepto_de_gasto where concepto_de_gasto.id_concepto_de_gasto=clasificador.id_concepto_de_gasto order by concepto_de_gasto, clasificador;");
        foreach ($query->result() as $indice) {
            $clasificador[$indice->id_clasificador] = $indice->concepto_de_gasto . "-" . $indice->clasificador;
        }
        return $clasificador;
    }

    //Elimina lógicamente el rubro, asignando la bandera de eliminado como verdadero
    function eliminado_logico_rubro($id_rubro) {
        $this->load->database();

        $dataInsert = array(
             "eliminado" => 'true'
        );

        $this->db->where('id_rubro', $id_rubro);
        $this->db->update('rubro', $dataInsert);

        //"eliminamos" los montos del rubro
        $sql = "SELECT id_monto from monto WHERE id_rubro=$id_rubro";
        $query = $this->db->query($sql);
        $montos = $query->result();

        foreach ($montos as $m) {
            eliminado_logico_monto($m->id_monto);
        }
    }

    //Elimina lógicamente el monto, asignando la bandera de eliminado como verdadero
    function eliminado_logico_monto($id_monto) {
        $this->load->database();

        $dataInsert = array(
             "eliminado" => 'true'
        );

        $this->db->where('id_monto', $id_monto);
        $this->db->update('monto', $dataInsert);
    }

    function eliminarRubro($id_rubro) {
        $this->load->database();

        $this->db->delete('rubro', array('id_rubro' => $id_rubro));
    }

    function insert_rubro($dataInsert) {
        $this->load->database();
        $id_accion = $_POST['id_accion'];
        $id_clasificador = $_POST['id_clasificador'];
        $precio_unitario = $_POST['precio_unitario'];
        $cantidad = $_POST['cantidad'];
        $anyo = $_POST['anyo'];
        $id_periodo_aplicacion = $_POST['id_periodo_aplicacion'];


        $data1 = array(
            "id_clasificador" => $id_clasificador,
            "id_accion" => $id_accion,
            "anyo" => $anyo,
            "id_periodo_aplicacion" => $id_periodo_aplicacion
        );

        $id_rubro = 0; //? *P qué es esto?

        $this->db->trans_start();

        $this->db->insert('rubro', $data1);

        $query = $this->db->query("select id_rubro from rubro order by 1 desc limit 1");
        $row = $query->row();
        $id_rubro = $row->id_rubro;
//aquí el id_periodo se refiere a qué parte de los sistemas se hace la captura,
//este caso es scapi, es decir id_periodo=1
        $data2 = array(
            "cantidad" => $cantidad,
            "precio_unitario" => $precio_unitario,
            "id_rubro" => $id_rubro,
            "id_periodo" => '1'
        );

        $this->db->insert('monto', $data2);
        $this->db->trans_complete();

        return $id_rubro;
    }

    function update_rubro($data) {
        $this->load->database();
        $cantidad = $data['cantidad'];
        $precio_unitario = $data['precio_unitario'];
        $id_clasificador = $data['id_clasificador'];
        $id_accion = $data['id_accion'];
        $anyo = $_POST['anyo'];
        $id_periodo_aplicacion = $_POST['id_periodo_aplicacion'];


        $dataInsert1 = array(
            "id_clasificador" => $id_clasificador,
            "id_periodo_aplicacion" => $id_periodo_aplicacion,
            "anyo" => $anyo
        );

        $dataInsert2 = array(
            "cantidad" => $cantidad,
            "precio_unitario" => $precio_unitario
        );

        $this->db->trans_start();
        $this->db->where('id_rubro', $this->input->post('id_rubro'));
        $this->db->update('rubro', $dataInsert1);

        $this->db->where('id_rubro', $this->input->post('id_rubro'));
        $this->db->update('monto', $dataInsert2);
        $this->db->trans_complete();
    }

    function InfoRubro($id_rubro) {
        $this->load->database();

        $query = $this->db->query("select rubro.id_rubro, id_accion, clasificador.id_clasificador,clasificador, cantidad, precio_unitario, id_periodo_aplicacion, anyo  from rubro, clasificador, monto where rubro.id_clasificador=clasificador.id_clasificador and monto.id_rubro=rubro.id_rubro and rubro.id_rubro=$id_rubro");


        foreach ($query->result_array() as $rubro) {
            $rubro['id_rubro'];
            $rubro['id_accion'];
            $rubro['clasificador'];
            $rubro['id_clasificador'];
            $rubro['cantidad'];
            $rubro['precio_unitario'];
            $rubro['id_periodo_aplicacion'];
            $rubro['anyo'];
        }

        return $rubro;
    }
    
    function InfoRubroPeriodos($id_rubro, $id_periodo, $id_anio) {
        $this->load->database();
        $sql = "SELECT rubro.id_rubro, id_accion, clasificador.id_clasificador,clasificador, cantidad, precio_unitario, monto.id_monto
                FROM rubro, clasificador, monto
                WHERE rubro.id_clasificador=clasificador.id_clasificador
                    AND monto.id_rubro=rubro.id_rubro
                    AND rubro.id_rubro=$id_rubro
                    AND id_periodo=$id_periodo
                    AND anyo = ".$id_anio;
        $query = $this->db->query($sql);
        //echo $sql;
        
		if($query->num_rows() > 0) {
			foreach ($query->result_array() as $rubro) {
				$rubro['id_rubro'];
				$rubro['id_accion'];
				$rubro['id_monto'];
				$rubro['clasificador'];
				$rubro['id_clasificador'];
				$rubro['cantidad'];
				$rubro['precio_unitario'];
			}
		} else {
			$rubro = 0;
		}
        return $rubro;
	}
	
	function getRubroMontoPeriodo($idRubro, $idPeriodo,$id_anio) {
        $this->load->database();
        $query = $this->db->query("SELECT periodo, id_periodo, cantidad, precio_unitario
                                    FROM v_rubro_periodo
                                    WHERE id_rubro = $idRubro
                                        AND id_periodo = $idPeriodo");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $rubroPeriodo) {
                $rubroMonto['idPeriodo'] = $rubroPeriodo['id_periodo'];
                $rubroMonto['periodo'] = $rubroPeriodo['periodo'];
                $rubroMonto['cantidad'] = $rubroPeriodo['cantidad'];
                $rubroMonto['precioUnitario'] = $rubroPeriodo['precio_unitario'];
            }
        } else {
            $rubroMonto = 0;
        }
        return $rubroMonto;
    }
    
    function existePeriodo($id_rubro, $id_periodo) {
        $this->load->database();
        $query = $this->db->query("SELECT rubro.id_rubro, id_accion, clasificador.id_clasificador,clasificador, cantidad, precio_unitario
                                    FROM rubro, clasificador, monto
                                    WHERE rubro.id_clasificador=clasificador.id_clasificador
                                        AND monto.id_rubro=rubro.id_rubro
                                        AND rubro.id_rubro=$id_rubro
                                        AND id_periodo=$id_periodo");
        $numRubros = $query->num_rows();

        if ($numRubros >= 1) {
            $existe = 1;
        } else {
            $existe = 0;
        }
        //echo "aqui".$existe;
        return $existe;
    }
    
    function guardarPeriodo($dataInsert) {
        $this->load->database();
        $id_rubro = $dataInsert['id_rubro'];
        $precio_unitario = $dataInsert['precio_unitario'];
        $cantidad = $dataInsert['cantidad'];
        $id_periodo = $dataInsert['id_periodo'];

        $data1 = array(
            "id_rubro" => $id_rubro,
            "cantidad" => $cantidad,
            "precio_unitario" => $precio_unitario,
            "id_periodo" => $id_periodo
        );

        $this->db->insert('monto', $data1);
    }

    function updatePeriodo($dataInsert) {
        $this->load->database();
        $id_monto = $dataInsert['id_monto'];
        $id_rubro = $dataInsert['id_rubro'];
        $precio_unitario = $dataInsert['precio_unitario'];
        $cantidad = $dataInsert['cantidad'];


        $data1 = array(
            "cantidad" => $cantidad,
            "precio_unitario" => $precio_unitario
        );

        $this->db->where('id_monto', $id_monto);
        $this->db->update('monto', $data1);
    }

    

    function presupuestoTotal($id_proyecto, $id_periodo) {
        $registros = 0;
        $total = 0;

        $qryPresupuestoTotal = "SELECT cantidad, precio FROM presupuesto WHERE id_proyecto = $id_proyecto AND id_periodo = $id_periodo";

        $query = $this->db->query($qryPresupuestoTotal);

        $registros = $query->num_rows();
        if ($registros >= 1) {
            $precio = 0;
            $cantidad = 0;
            foreach ($query->result_array() as $reg) {
                $cantidad = $reg['cantidad'];
                $precio = $reg['precio'];
                $total += ($cantidad * $precio);
            }
        }
        return $total;
    }
}

?>
