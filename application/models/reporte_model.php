<?php

class Reporte_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function fecha() {
        date_default_timezone_set('UTC');
        $mes = date("n");
        $mesArray = array(1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
        $semana = date("D");
        $semanaArray = array("Mon" => "Lunes", "Tue" => "Martes", "Wed" => "Miercoles", "Thu" => "Jueves", "Fri" => "Viernes", "Sat" => "Sábado", "Sun" => "Domingo",);
        $mesReturn = $mesArray[$mes];
        $semanaReturn = $semanaArray[$semana];
        $dia = date("d");
        $anyo = date("Y");
        return $semanaReturn . " " . $dia . " de " . $mesReturn . " de " . $anyo;
    }

    function reporte_progen($proyecto) {
        $scapi = 'SCAPI PEFEN 2011-2012';
        $this->load->library('session');
        $this->load->library('pdf');

        $pdf = new Pdf('P', 'mm', 'LETTER', true, 'UTF-8', false);

        $pdf->AddPage("P", "LETTER");
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 25);

        //$id_entidad = $this->session->userdata('id_entidad');
        //$entidad = $this->session->userdata('entidad');
        // si es PROFEN
        //$escuela = $this->session->userdata('escuela');

        $esPROFEN = $this->session->userdata('es_PROFEN');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('DGESPE');
        $pdf->SetTitle($scapi);

        $fecha = $this->fecha();
        $nombre_proyecto = $proyecto['nombre'];
        $id_proyecto = $proyecto['id_proyecto'];

        $responsable = "Datos del responsable: \nNombre: " . $proyecto['nombre_responsable'] . "\nCargo:  " . $proyecto['cargo'] . "\nGrado Académico:  " . $proyecto['grado_academico'] . "\nTeléfono: " . $proyecto['telefono'] . "\nDirección de correo: " . $proyecto['email'];
        $objetivo_proyecto = "Objetivo general del proyecto: " . $proyecto['objetivo'];
        $justificacion_proyecto = "Justificación del proyecto: " . $proyecto['justificacion'];

        //header
        // Logo
        $pdf->Image("http://localhost/tercertrimestre20112012/images/dgespe.png", 170, 20, 20);


        $pdf->SetFont('times', 'S', 9);
        $pdf->Cell(20, 20, $fecha, 0, 1);


        $pdf->SetFont('times', 'B', 13);
        $pdf->MultiCell(190, 5, "Proyecto Integral para el Programa de Gestión de la Escuela Normal XXXXXX", "TLR", 'C', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, "PEFEN 2011-2012", "LR", 'C', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $proyecto['entidad'], "BLR", 'C', 0, 1, '', '', true);
        if ($esPROFEN == 1) {
            $pdf->MultiCell(190, 5, $proyecto['escuela'], "BLR", 'C', 0, 1, '', '', true);
        }


        $pdf->SetFont('times', '', 10);

        $pdf->ln("10");
        $pdf->MultiCell(190, 5, "Proyecto " . $nombre_proyecto, "", 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $objetivo_proyecto, '', 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $justificacion_proyecto, 'B', 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $responsable, "B", 'L', 0, 1, '', '', true);


        //////////Objetivos
        $TotalProyecto = 0;

        //objetivos
        $query1 = $this->db->query("select objetivos.id_objetivos, objetivos.descripcion as descripcion_objetivo,  objetivos.id_subtipo_de_objetivo, objetivos.justificacion as justificacion_objetivo, tipo_de_objetivo.tipo_de_objetivo, subtipo_de_objetivo.subtipo_de_objetivo from objetivos, tipo_de_objetivo, subtipo_de_objetivo where tipo_de_objetivo.id_tipo_de_objetivo=subtipo_de_objetivo.id_tipo_de_objetivo and subtipo_de_objetivo.id_subtipo_de_objetivo=objetivos.id_subtipo_de_objetivo and id_proyecto=$id_proyecto order by id_objetivos");

        foreach ($query1->result_array() as $objetivo) {//ini obj
            $id_objetivo = $objetivo['id_objetivos'];
            $pdf->MultiCell(200, 5, "Objetivo. " . $objetivo['descripcion_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "Tipo: " . $objetivo['tipo_de_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "SubTipo: " . $objetivo['subtipo_de_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "Justificación: " . $objetivo['justificacion_objetivo'], 0, '', 0);
            $pdf->ln();

            //metas
            $query2 = $this->db->query("select metas.id_metas, metas.descripcion as descripcion_meta, metas.unidad_de_medida,  tipo_de_beneficiario, beneficiarios_hombres, beneficiarios_mujeres from metas,  tipo_de_beneficiario where  tipo_de_beneficiario.id_tipo_de_beneficiario=metas.id_tipo_de_beneficiario and id_objetivos=$id_objetivo  order by 1 asc");
            foreach ($query2->result_array() as $meta) {//ini met
                $id_meta = $meta['id_metas'];
                $pdf->MultiCell(200, 5, "Meta " . $meta['descripcion_meta'], 0, '', 0);
                $pdf->MultiCell(200, 5, "Meta " . "HOLAAA", 0, '', 0);
                $pdf->MultiCell(200, 5, "Unidad de Medida: " . $meta['unidad_de_medida'], 0, '', 0);
                $pdf->MultiCell(200, 5, "Tipo de Beneficiario: " . $meta['tipo_de_beneficiario'], 0, '', 0);
                $beneficiarios_hombres = $meta['beneficiarios_hombres'];
                $beneficiarios_mujeres = $meta['beneficiarios_mujeres'];
                $num_beneficiarios = $beneficiarios_mujeres + $beneficiarios_hombres;
                $pdf->MultiCell(200, 5, "No. Beneficiarios: " . $num_beneficiarios, 0, '', 0);
                $pdf->MultiCell(200, 5, "Hombres: " . $beneficiarios_hombres, 0, '', 0);
                $pdf->MultiCell(200, 5, "Mujeres: " . $beneficiarios_mujeres, 0, '', 0);
                $pdf->ln();
                //acciones
                $query3 = $this->db->query("select accion.id_accion, accion.accion, accion.descripcion1 as
descripcion_accion1,  accion.descripcion2 as
descripcion_accion2 from accion where id_metas=$id_meta order by 1 asc");
                foreach ($query3->result_array() as $accion) {//ini acc
                    $id_accion = $accion['id_accion'];
                    $pdf->MultiCell(200, 5, "Acción. " . $accion['accion'], 0, '', 0);
                    $pdf->MultiCell(200, 5, "Justificación 2011: " . $accion['descripcion_accion1'], 0, '', 0);
                    $pdf->MultiCell(200, 5, "Justificación 2012: " . $accion['descripcion_accion2'], 0, '', 0);
                    $pdf->ln();
                    //rubros
                    //primer año anyo=1
                    $numRubros = 0;
                    $query4 = $this->db->query("select rubro.id_rubro, rubro.id_clasificador, clasificador.clasificador, clasificador.id_concepto_de_gasto, concepto_de_gasto.concepto_de_gasto, precio_unitario, cantidad , idperiodonomenclatura  from rubro, clasificador, concepto_de_gasto, monto, periodo where rubro.id_clasificador=clasificador.id_clasificador and concepto_de_gasto.id_concepto_de_gasto=clasificador.id_concepto_de_gasto and rubro.id_rubro=monto.id_rubro and  id_accion=$id_accion and anyo=1 and monto.id_periodo=1 and periodo.id_periodo=rubro.id_periodo_aplicacion  order by 1 asc"); //precio_unitario, cantidad , (precio_unitario*cantidad) as total_rubro, rubro.id_concepto_de_gasto

                    $numRubros = $query4->num_rows();
                    if ($numRubros >= 1) {
                        //tablas de rubros

                        $w = array(32, 66, 10, 25, 25, 25);
                        $w_total = 158;
                        $header = array("Concepto de gasto ", "Rubro de gasto", "P", "Cantidad", "Precio unitario", "Total");
                        $pdf->Cell($w_total + 25, 7, "RECURSOS 2011", 1, 0, 'C');
                        $pdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($header); $i++)
                            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        $pdf->Ln();
                        $precio_unitario = 0;
                        $cantidad = 0;
                        $total_rubro = 0;
                        $totalRubros = 0;
                        foreach ($query4->result_array() as $rubro) {
                            $pdf->SetFont('times', '', 8);
                            $id_rubro = $rubro['id_rubro'];
                            $pdf->Cell($w[0], 6, $rubro['concepto_de_gasto'], 'LR');
                            $pdf->Cell($w[1], 6, $rubro['clasificador'], 'LR');
                            $pdf->Cell($w[2], 6, $rubro['idperiodonomenclatura'], 'LR');
                            $pdf->Cell($w[3], 6, $rubro['cantidad'], 'LR');
                            $pdf->Cell($w[4], 6, $rubro['precio_unitario'], 'LR');
                            $precio_unitario = $rubro['precio_unitario'];
                            $cantidad = $rubro['cantidad'];
                            $total_rubro = $precio_unitario * $cantidad;
                            $totalRubros = $total_rubro + $totalRubros;
                            $pdf->Cell($w[4], 6, $total_rubro, 'LR');
                            $pdf->ln();
                            $pdf->SetFont('times', '', 10);
                        }
                        //echo $totalRubros."<br>";
                        $TotalProyecto = $TotalProyecto + $totalRubros;

                        //Línea de cierre
                        $pdf->Cell($w_total, 7, "Total", 1, 0, 'C');
                        $pdf->Cell($w[4], 7, "$" . $totalRubros, 1, 0, 'C');
                        $pdf->ln(10);
                        //fin rubros
                    }                             //fin de if num rubros
                    //segundo año anyo=2
                    $numRubros = 0;
                    $query4 = $this->db->query("select rubro.id_rubro, rubro.id_clasificador, clasificador.clasificador, clasificador.id_concepto_de_gasto, concepto_de_gasto.concepto_de_gasto, precio_unitario, cantidad , idperiodonomenclatura  from rubro, clasificador, concepto_de_gasto, monto, periodo where rubro.id_clasificador=clasificador.id_clasificador and concepto_de_gasto.id_concepto_de_gasto=clasificador.id_concepto_de_gasto and rubro.id_rubro=monto.id_rubro and  id_accion=$id_accion and anyo=2 and monto.id_periodo=1 and periodo.id_periodo=rubro.id_periodo_aplicacion  order by 1 asc"); //precio_unitario, cantidad , (precio_unitario*cantidad) as total_rubro, rubro.id_concepto_de_gasto
                    $numRubros = $query4->num_rows();
                    if ($numRubros >= 1) {
                        //tablas de rubros

                        $w = array(32, 66, 10, 25, 25, 25);
                        $w_total = 158;
                        $header = array("Concepto de gasto ", "Rubro de gasto", "P", "Cantidad", "Precio unitario", "Total");
                        $pdf->Cell($w_total + 25, 7, "RECURSOS 2012", 1, 0, 'C');
                        $pdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($header); $i++)
                            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        $pdf->Ln();
                        $precio_unitario = 0;
                        $cantidad = 0;
                        $total_rubro = 0;
                        $totalRubros = 0;
                        foreach ($query4->result_array() as $rubro) {
                            $pdf->SetFont('times', '', 8);
                            $id_rubro = $rubro['id_rubro'];
                            $pdf->Cell($w[0], 6, $rubro['concepto_de_gasto'], 'LR');
                            $pdf->Cell($w[1], 6, $rubro['clasificador'], 'LR');
                            $pdf->Cell($w[2], 6, $rubro['idperiodonomenclatura'], 'LR');
                            $pdf->Cell($w[3], 6, $rubro['cantidad'], 'LR');
                            $pdf->Cell($w[4], 6, $rubro['precio_unitario'], 'LR');
                            $precio_unitario = $rubro['precio_unitario'];
                            $cantidad = $rubro['cantidad'];
                            $total_rubro = $precio_unitario * $cantidad;
                            $totalRubros = $total_rubro + $totalRubros;
                            $pdf->Cell($w[4], 6, $total_rubro, 'LR');
                            $pdf->ln();
                            $pdf->SetFont('times', '', 10);
                        }
                        //echo $totalRubros."<br>";
                        $TotalProyecto = $TotalProyecto + $totalRubros;

                        //Línea de cierre
                        $pdf->Cell($w_total, 7, "Total", 1, 0, 'C');
                        $pdf->Cell($w[4], 7, "$" . $totalRubros, 1, 0, 'C');
                        $pdf->ln(10);
                        //fin rubros
                    }                             //fin de if num rubros
                }//fin acciones
            }//fin met
        }//fin obj
        $pdf->ln(10);
        $pdf->Cell(200, 5, "Resumen del Proyecto Integral", 1, 0, 'C');
        $pdf->ln();
        $pdf->Cell(200, 5, "Monto total del Proyecto Integral ProFEN : $" . $TotalProyecto, 1, 0, 'C');
        $pdf->ln(30);
        ///////////Objetivos fin
        $pdf->ln(30);
        $pdf->SetFont('times', 'S', 9);
        $pdf->SetY(-40);

        $pdf->Cell(90, 7, "Responsable de la Entidad", "T", 0, 'C');

        $pdf->Cell(10, 7, "", "", 0, 'C');


        //footer
        // Position at 1.5 cm from bottom
        //$pdf->SetY(-15);
        // Set font
        $pdf->SetFont('times', 'I', 8);
        // Page number
        $pdf->Cell(0, 10, 'Página ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, 0, 'C');
        //fin footer

        if ($esPROFEN == 1) {
            $titulo_pdf = "tercertrimestre_PEFEN_2011-2012_" . $proyecto['escuela'] . ".pdf";
        } else {
            $titulo_pdf = "tercertrimestre_SCAPI_PEFEN_2011-2012_" . $proyecto['entidad'] . ".pdf";
        }

        $pdf->Output('pdf/' . $titulo_pdf, 'F');

        if ($esPROFEN == 1) {
            echo "<a href='/tercertrimestre2012/pdf/" . $titulo_pdf . "'>Bajar Reporte SCAPI</a>";
        } else {
            echo "<a href='/tercertrimestre2012/pdf/" . $titulo_pdf . "'>Bajar Reporte SCAPI</a>";
        }
    }

    function reporte_profen($proyecto) {
        $scapi = 'SCAPI PEFEN 2011-2012';
        $this->load->library('session');
        $this->load->library('pdf');
        $pdf = new Pdf('P', 'mm', 'LETTER', true, 'UTF-8', false);

        $pdf->AddPage("P", "LETTER");
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 25);

        $id_entidad = $this->session->userdata('id_entidad');
        $entidad = $this->session->userdata('entidad');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('DGESPE');
        $pdf->SetTitle($scapi);




        $fecha = $this->fecha();
        $entidad = $this->session->userdata('entidad');
        //$escuela = $this->session->userdata('escuela');
        $escuela = $proyecto['escuela'];
        $nombre_proyecto = $proyecto['nombre'];
        $id_proyecto = $proyecto['id_proyecto'];

        $responsable = "Datos del responsable: \nNombre: " . $proyecto['nombre_responsable'] . "\nCargo:  " . $proyecto['cargo'] . "\nGrado Académico:  " . $proyecto['grado_academico'] . "\nTeléfono: " . $proyecto['telefono'] . "\nDirección de correo: " . $proyecto['email'];
        $objetivo_proyecto = "Objetivo general del proyecto: " . $proyecto['objetivo'];
        $justificacion_proyecto = "Justificación del proyecto: " . $proyecto['justificacion'];



        //header
        // Logo
        //   $pdf->Image("/var/www/scapi20112012/images/dgespe.png", 170, 20, 20);
        $pdf->Image("http://localhost/segundotrimestre2011/images/dgespe.png", 170, 20, 20);



        $pdf->SetFont('times', 'S', 9);
        $pdf->Cell(20, 20, $fecha, 0, 1);


        $pdf->SetFont('times', 'B', 13);
        $pdf->MultiCell(190, 5, "88Proyecto Integral para el Programa de Fortalecimiento de la Escuela Normal. ", "TLR", 'C', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, "PEFEN 2011-2012", "LR", 'C', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $entidad, "LR", 'C', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $escuela, "BLR", 'C', 0, 1, '', '', true);



        $pdf->SetFont('times', '', 10);

        $pdf->ln("10");
        $pdf->MultiCell(190, 5, "Proyecto " . $nombre_proyecto, "", 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $objetivo_proyecto, '', 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $justificacion_proyecto, 'B', 'L', 0, 1, '', '', true);
        $pdf->MultiCell(190, 5, $responsable, "B", 'L', 0, 1, '', '', true);

        /**
         *
         * */
        $TotalProyecto = 0;
        //objetivos
        $query1 = $this->db->query("select objetivos.id_objetivos, objetivos.descripcion as descripcion_objetivo,  objetivos.id_subtipo_de_objetivo, objetivos.justificacion as justificacion_objetivo, tipo_de_objetivo.tipo_de_objetivo, subtipo_de_objetivo.subtipo_de_objetivo from objetivos, tipo_de_objetivo, subtipo_de_objetivo where tipo_de_objetivo.id_tipo_de_objetivo=subtipo_de_objetivo.id_tipo_de_objetivo and subtipo_de_objetivo.id_subtipo_de_objetivo=objetivos.id_subtipo_de_objetivo and id_proyecto=$id_proyecto order by id_objetivos");

        foreach ($query1->result_array() as $objetivo) {//ini obj
            $id_objetivo = $objetivo['id_objetivos'];
            $pdf->MultiCell(200, 5, "Objetivo. " . $objetivo['descripcion_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "Tipo: " . $objetivo['tipo_de_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "SubTipo: " . $objetivo['subtipo_de_objetivo'], 0, '', 0);
            $pdf->MultiCell(200, 5, "Justificación: " . $objetivo['justificacion_objetivo'], 0, '', 0);
            $pdf->ln();
            //metas

            /* select metas.id_metas, metas.descripcion as descripcion_meta, metas.id_unidad_de_medida, unidad_de_medida.unidad_de_medida, tipo_de_beneficiario, beneficiarios_hombres, beneficiarios_mujeres from metas, unidad_de_medida, tipo_de_beneficiario where metas.id_unidad_de_medida=unidad_de_medida.id_unidad_de_medida and tipo_de_beneficiario.id_tipo_de_beneficiario=metas.id_tipo_de_beneficiario and id_objetivos=$id_objetivo */
            //	echo "Metas".br();
            $query2 = $this->db->query("select metas.id_metas, metas.descripcion as descripcion_meta, metas.unidad_de_medida,  tipo_de_beneficiario, beneficiarios_hombres, beneficiarios_mujeres from metas,  tipo_de_beneficiario where  tipo_de_beneficiario.id_tipo_de_beneficiario=metas.id_tipo_de_beneficiario and id_objetivos=$id_objetivo  order by 1 asc");
            foreach ($query2->result_array() as $meta) {//ini met
                $id_meta = $meta['id_metas'];
                //echo "---id_meta ".$id_meta.br();
                $pdf->MultiCell(200, 5, "Meta " . $meta['descripcion_meta'], 0, '', 0);
                $pdf->MultiCell(200, 5, "Unidad de Medida: " . $meta['unidad_de_medida'], 0, '', 0);
                $pdf->MultiCell(200, 5, "Tipo de Beneficiario: " . $meta['tipo_de_beneficiario'], 0, '', 0);

                $beneficiarios_hombres = $meta['beneficiarios_hombres'];
                $beneficiarios_mujeres = $meta['beneficiarios_mujeres'];
                $num_beneficiarios = $beneficiarios_mujeres + $beneficiarios_hombres;
                $pdf->MultiCell(200, 5, "No. Beneficiarios: " . $num_beneficiarios, 0, '', 0);
                $pdf->MultiCell(200, 5, "Hombres: " . $beneficiarios_hombres, 0, '', 0);
                $pdf->MultiCell(200, 5, "Mujeres: " . $beneficiarios_mujeres, 0, '', 0);
                $pdf->ln();
                //acciones
                //		echo "Acciones".br();
                $query3 = $this->db->query("select accion.id_accion, accion.accion, accion.descripcion1 as
descripcion_accion1,  accion.descripcion2 as
descripcion_accion2 from accion where id_metas=$id_meta order by 1 asc");
                foreach ($query3->result_array() as $accion) {//ini acc
                    $id_accion = $accion['id_accion'];
                    $pdf->MultiCell(200, 5, "Acción. " . $accion['accion'], 0, '', 0);
                    $pdf->MultiCell(200, 5, "Justificación 2011: " . $accion['descripcion_accion1'], 0, '', 0);
                    $pdf->MultiCell(200, 5, "Justificación 2012: " . $accion['descripcion_accion2'], 0, '', 0);
                    $pdf->ln();
                    //rubrosora
                    //primer año anyo=1
                    $numRubros = 0;
                    $query4 = $this->db->query("select rubro.id_rubro, rubro.id_clasificador, clasificador.clasificador, clasificador.id_concepto_de_gasto, concepto_de_gasto.concepto_de_gasto, precio_unitario, cantidad , idperiodonomenclatura  from rubro, clasificador, concepto_de_gasto, monto, periodo where rubro.id_clasificador=clasificador.id_clasificador and concepto_de_gasto.id_concepto_de_gasto=clasificador.id_concepto_de_gasto and rubro.id_rubro=monto.id_rubro and  id_accion=$id_accion and anyo=1 and monto.id_periodo=1 and periodo.id_periodo=rubro.id_periodo_aplicacion  order by 1 asc"); //precio_unitario, cantidad , (precio_unitario*cantidad) as total_rubro, rubro.id_concepto_de_gasto
                    $numRubros = $query4->num_rows();
                    if ($numRubros >= 1) {
                        //tablas de rubros

                        $w = array(32, 66, 10, 25, 25, 25);
                        $w_total = 158;
                        $header = array("Concepto de gasto ", "Rubro de gasto", "P", "Cantidad", "Precio unitario", "Total");
                        $pdf->Cell($w_total + 25, 7, "RECURSOS 2011", 1, 0, 'C');
                        $pdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($header); $i++)
                            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        $pdf->Ln();
                        $precio_unitario = 0;
                        $cantidad = 0;
                        $total_rubro = 0;
                        $totalRubros = 0;
                        foreach ($query4->result_array() as $rubro) {
                            $pdf->SetFont('times', '', 8);
                            $id_rubro = $rubro['id_rubro'];
                            $pdf->Cell($w[0], 6, $rubro['concepto_de_gasto'], 'LR');
                            $pdf->Cell($w[1], 6, $rubro['clasificador'], 'LR');
                            $pdf->Cell($w[2], 6, $rubro['idperiodonomenclatura'], 'LR');
                            $pdf->Cell($w[3], 6, $rubro['cantidad'], 'LR');
                            $pdf->Cell($w[4], 6, $rubro['precio_unitario'], 'LR');
                            $precio_unitario = $rubro['precio_unitario'];
                            $cantidad = $rubro['cantidad'];
                            $total_rubro = $precio_unitario * $cantidad;
                            $totalRubros = $total_rubro + $totalRubros;
                            $pdf->Cell($w[4], 6, $total_rubro, 'LR');
                            $pdf->ln();
                            $pdf->SetFont('times', '', 10);
                        }
                        //echo $totalRubros."<br>";
                        $TotalProyecto = $TotalProyecto + $totalRubros;

                        //Línea de cierre
                        $pdf->Cell($w_total, 7, "Total", 1, 0, 'C');
                        $pdf->Cell($w[4], 7, "$" . $totalRubros, 1, 0, 'C');
                        $pdf->ln(10);
                        //fin rubros
                    }    //fin de if num rubros
                    //segundo año anyo=2
                    $numRubros = 0;
                    $query4 = $this->db->query("select rubro.id_rubro, rubro.id_clasificador, clasificador.clasificador, clasificador.id_concepto_de_gasto, concepto_de_gasto.concepto_de_gasto, precio_unitario, cantidad , idperiodonomenclatura  from rubro, clasificador, concepto_de_gasto, monto, periodo where rubro.id_clasificador=clasificador.id_clasificador and concepto_de_gasto.id_concepto_de_gasto=clasificador.id_concepto_de_gasto and rubro.id_rubro=monto.id_rubro and  id_accion=$id_accion and anyo=2 and monto.id_periodo=1 and periodo.id_periodo=rubro.id_periodo_aplicacion  order by 1 asc"); //precio_unitario, cantidad , (precio_unitario*cantidad) as total_rubro, rubro.id_concepto_de_gasto
                    $numRubros = $query4->num_rows();
                    if ($numRubros >= 1) {
                        //tablas de rubros

                        $w = array(32, 66, 10, 25, 25, 25);
                        $w_total = 158;
                        $header = array("Concepto de gasto ", "Rubro de gasto", "P", "Cantidad", "Precio unitario", "Total");
                        $pdf->Cell($w_total + 25, 7, "RECURSOS 2012", 1, 0, 'C');
                        $pdf->Ln();
                        //Cabeceras
                        for ($i = 0; $i < count($header); $i++)
                            $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
                        $pdf->Ln();
                        $precio_unitario = 0;
                        $cantidad = 0;
                        $total_rubro = 0;
                        $totalRubros = 0;
                        foreach ($query4->result_array() as $rubro) {
                            $pdf->SetFont('times', '', 8);
                            $id_rubro = $rubro['id_rubro'];
                            $pdf->Cell($w[0], 6, $rubro['concepto_de_gasto'], 'LR');
                            $pdf->Cell($w[1], 6, $rubro['clasificador'], 'LR');
                            $pdf->Cell($w[2], 6, $rubro['idperiodonomenclatura'], 'LR');
                            $pdf->Cell($w[3], 6, $rubro['cantidad'], 'LR');
                            $pdf->Cell($w[4], 6, $rubro['precio_unitario'], 'LR');
                            $precio_unitario = $rubro['precio_unitario'];
                            $cantidad = $rubro['cantidad'];
                            $total_rubro = $precio_unitario * $cantidad;
                            $totalRubros = $total_rubro + $totalRubros;
                            $pdf->Cell($w[4], 6, $total_rubro, 'LR');
                            $pdf->ln();
                            $pdf->SetFont('times', '', 10);
                        }
                        //echo $totalRubros."<br>";
                        $TotalProyecto = $TotalProyecto + $totalRubros;

                        //Línea de cierre
                        $pdf->Cell($w_total, 7, "Total", 1, 0, 'C');
                        $pdf->Cell($w[4], 7, "$" . $totalRubros, 1, 0, 'C');
                        $pdf->ln(10);
                        //fin rubros
                    }    //fin de if num rubros
                }//fin acciones
            }//fin met
        }//fin obj
        $pdf->ln(10);
        $pdf->Cell(200, 5, "Resumen del Proyecto Integral", 1, 0, 'C');
        $pdf->ln();
        $pdf->Cell(200, 5, "Monto total del Proyecto Integral ProFEN : $" . $TotalProyecto, 1, 0, 'C');
        $pdf->ln(15);

        /**
         *
         */
        $pdf->SetFont('times', 'S', 9);
        //$pdf->SetY(-40);
        $pdf->Cell(90, 7, "Responsable de la Entidad", "T", 0, 'C');
        $pdf->Cell(10, 7, "", "", 0, 'C');
        $pdf->Cell(90, 7, "Responsable de la Escuela", "T", 0, 'C');
        //$pdf->ln(10);
        //footer
        // Position at 1.5 cm from bottom
        $pdf->SetY(-35);
        // Set font

        $pdf->SetFont('times', 'I', 8);
        // Page number
        $pdf->Cell(0, 10, 'Página ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, 0, 'C');
        //fin footer
//	  	$pdf->Output('/var/www/scapi20092010/pdf/scapiProfen2011.pdf', 'F');
        $pdf->Output('pdf/primertrimestre2011.pdf', 'F');

        echo "<a href='/primertrimestre2011/pdf/primertrimestre2011.pdf'>Reporte SCAPI</a>";
        //return $dir;
    }

}

?>
